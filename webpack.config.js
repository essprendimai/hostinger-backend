const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractFroalaCSS = new ExtractTextPlugin('froala.css');

// Page styles
var pageDefaultModuleConfig = {
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: {
                    loader: "vue-loader"
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: '[name].[ext]',
                        outputPath: '../../../images/',
                        publicPath: '/images/'
                    }
                }
            },
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: '[name].[ext]',
                        outputPath: '../../../fonts/',
                        publicPath: '/fonts/'
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'default.css',
                            outputPath: '../../../css/basic-view/page/'
                        }
                    },
                    {
                        loader: 'extract-loader',
                    },
                    {
                        loader: 'css-loader?-minimize',
                    },
                    {
                        loader: 'postcss-loader',
                    },
                    {
                        loader: 'resolve-url-loader',
                        options: {
                            root : ''
                        }
                    },
                    {
                        loader: 'sass-loader?outputStyle=compressed',
                    }
                ]
            }
        ]
    },
    watch: true,
};
var pageDefaultConfig = Object.assign({}, pageDefaultModuleConfig, {
    entry: [
        './Modules/Basic/Assets/js/page/default.js',
    ],
    output: {
        path: path.resolve(__dirname, 'public/js/basic-view/page'),
        filename: 'default.js'
    },
});

var basicModuleConfig = {
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: {
                    loader: "vue-loader"
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: '[name].[ext]',
                        outputPath: '../../images/',
                        publicPath: '/images/'
                    }
                }
            },
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: '[name].[ext]',
                        outputPath: '../../fonts/',
                        publicPath: '/fonts/'
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'basic.css',
                            outputPath: '../../css/basic-view/'
                        }
                    },
                    {
                        loader: 'extract-loader',
                    },
                    {
                        loader: 'css-loader?-minimize',
                    },
                    {
                        loader: 'resolve-url-loader',
                        options: {
                            root : ''
                        }
                    },
                    {
                        loader: 'postcss-loader',
                    },
                    {
                        loader: 'sass-loader?outputStyle=compressed',
                    }
                ]
            }
        ]
    },
    watch: true,
};
var basicConfig = Object.assign({}, basicModuleConfig, {
    entry: [
        './Modules/Basic/Assets/js/app.js',
        './resources/Assets/scss/admin-main.scss',
    ],
    output: {
        path: path.resolve(__dirname, 'public/js/basic-view'),
        filename: 'basic.js'
    },
});

var froalaCSSModulecConfig = {
    module: {
        rules: [
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: '[name].[ext]',
                        outputPath: '../../images/',
                        publicPath: '/images/'
                    }
                }
            },
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: '[name].[ext]',
                        outputPath: '../../fonts/',
                        publicPath: '/fonts/'
                    }
                }
            },
            {
                test: /\.css$/i,
                use: extractFroalaCSS.extract({
                    use: [
                        {
                            loader: 'css-loader?-minimize',
                        },
                        {
                            loader: 'resolve-url-loader',
                            options: {
                                root : ''
                            }
                        },
                        {
                            loader: 'sass-loader?outputStyle=compressed',
                        }
                    ]
                })
            },
        ]
    },
    plugins: [
        extractFroalaCSS,
    ]
};
var froalaCSSConfig = Object.assign({}, froalaCSSModulecConfig, {
    entry: [
        path.resolve(__dirname, 'node_modules/font-awesome/css/font-awesome.css'),
        path.resolve(__dirname, 'node_modules/@fortawesome/fontawesome-free/css/all.css'),
        path.resolve(__dirname, 'node_modules/froala-editor/css/froala_editor.pkgd.css')
    ],
    devtool: '',
    output: {
        path: path.resolve(__dirname, 'public/css/basic-view'),
        filename: 'froala.css'
    },
});

var froalaJSModulecConfig = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
        ]
    }
};
var froalaJSConfig = Object.assign({}, froalaJSModulecConfig, {
    entry: [
        path.resolve(__dirname, 'Modules/Basic/Assets/js/froala.js'),
    ],
    output: {
        path: path.resolve(__dirname, 'public/js/basic-view'),
        filename: 'froala.js'
    },
});

var welcomeModuleConfig = {
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'welcome.css',
                            outputPath: '../../css/basic-view/'
                        }
                    },
                    {
                        loader: 'extract-loader',
                    },
                    {
                        loader: 'css-loader?-minimize',
                    },
                    {
                        loader: 'resolve-url-loader',
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            root : ''
                        }
                    },
                    {
                        loader: 'sass-loader?outputStyle=compressed',
                    }
                ]
            }
        ]
    },
    watch: true,
};
var welcomeConfig = Object.assign({}, welcomeModuleConfig, {
    entry: [
        './Modules/Basic/Assets/sass/welcome/welcome.scss',
    ],
    output: {
        path: path.resolve(__dirname, 'public/js/basic-view'),
        filename: 'welcome.js'
    },
});

var loginPageModuleConfig = {
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'login-page.css',
                            outputPath: '../../css/basic-view/'
                        }
                    },
                    {
                        loader: 'extract-loader',
                    },
                    {
                        loader: 'css-loader?-minimize',
                    },
                    {
                        loader: 'resolve-url-loader',
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            root : ''
                        }
                    },
                    {
                        loader: 'sass-loader?outputStyle=compressed',
                    }
                ]
            }
        ]
    },
    watch: true,
};
var loginPageConfig = Object.assign({}, loginPageModuleConfig, {
    entry: [
        './Modules/Basic/Assets/sass/login-page.scss',
    ],
    output: {
        path: path.resolve(__dirname, 'public/js/basic-view'),
        filename: 'login-page.js'
    },
});

module.exports = [
    basicConfig, froalaCSSConfig, froalaJSConfig, welcomeConfig, pageDefaultConfig, loginPageConfig
];
