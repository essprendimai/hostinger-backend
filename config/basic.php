<?php

declare(strict_types = 1);

return [
    'name' => 'Basic',
    'active_languages' => explode(',', env('ACTIVE_LANGUAGES'))
];
