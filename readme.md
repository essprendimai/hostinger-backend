# HOSTINGER BACKED #

## Introduction
    * Admin panel link: admin-panel-login
    * Login: info@hostinger.com secret
    
## API
    * Client id: 1
    * Secret: pks7dyJN8JVn2XXjX1vDoHhwwYGDYyHHaXwHCRcX
    * Type: password
    * Username: api@hostinger.com
    * Password: secret
    
## Requirements
    * PHP: `>= 7.2`

## Installation
    * Download project
    * `php artisan key:generate`
    * `php artisan queue:table`
    * `php artisan queue:failed-table`
    * `php artisan migrate`
    * `php artisan module:migrate`
    * `php artisan db:seed`
    * `php artisan module:seed`
    * `php artisan passport:install`
    * `npm install`
    * Optional - For more donwload size its important up post_max_size in php.ini file if cant upload files

# Basic Module

Basic functions and classes or services. For example Settings and Enums. Admin front side, page, group creat or language switch

## Configuration
* Run `php artisan migrate` command.
* Seed the admin user `php artisan module:seed Basic`. Created admin user credentials: `info@admin.com:secret`
* Publish lang run `php artisan vendor:publish --provider="Essprendimai\Basic\Providers\BasicServiceProvider" --tag=lang`
* Publish assets run `php artisan vendor:publish --provider="Essprendimai\Basic\Providers\BasicServiceProvider" --tag=public`
* Publish menu template: `php artisan vendor:publish --provider="Essprendimai\Basic\Providers\BasicServiceProvider" --tag=menu`
* Publish auth template: `php artisan vendor:publish --provider="Essprendimai\Basic\Providers\BasicServiceProvider" --tag=auth`
* Publish app-gate template: `php artisan vendor:publish --provider="Essprendimai\Basic\Providers\BasicServiceProvider" --tag=app-gate`
* Publish role template: `php artisan vendor:publish --provider="Essprendimai\Basic\Providers\BasicServiceProvider" --tag=role`
* Publish user template: `php artisan vendor:publish --provider="Essprendimai\Basic\Providers\BasicServiceProvider" --tag=user`
* Publish templates: `php artisan vendor:publish --provider="Essprendimai\Basic\Providers\BasicServiceProvider" --tag=views`
* Publish log templates: `php artisan vendor:publish --provider="Essprendimai\Basic\Providers\BasicServiceProvider" --tag=log`
* Publish translation templates: `php artisan vendor:publish --provider="Essprendimai\Basic\Providers\BasicServiceProvider" --tag=translations`

## Languages
* Set `env` => `ACTIVE_LANGUAGES=lt,en` for activate languages
* Usage `__('basic::language.lithuanian')`

## Froala
* Add js `dist/js/froala.js`
* Add css `dist/css/froala.css`
* Use
```javascript
<script>
    $(function() {
        $('textarea').froalaEditor()
    });
</script>
```

## Data table
* on top include: `
    @extends('basic-view::layouts.app-gate')
    @include('basic-view::includes.datatable')
`
* Create table live: `<table class="table table-hover table-bordered" id="sampleTable">`
* Load data table 
```
@push('scripts')
    <script type="text/javascript">
        $('#sampleTable').DataTable({
            "language": datatableLanguages["{{ App::getLocale() }}"],
        });
    </script>
@endpush
```

## Middleware
* LimitsBackendRouteAccess - to limit access to admin pages

## Build assets
* npm install
* npm run build - production

# ApiAuth Module

Manages api authentication users

* In laravel project `AppServiceProvider` to register method add `Passport::ignoreMigrations();`
* Run `php artisan passport:install` to generate secret keys with which encoding tokens
* Next, you should call the `Passport::routes` method within the `boot` method of your  `AuthServiceProvider`. This method will register the routes necessary to issue access tokens and revoke access tokens, clients, and personal access tokens
* Also `Passport::tokensExpireIn(now()->addMinutes(env('API_TOKEN_EXPIRES')));` and `Passport::refreshTokensExpireIn(now()->addMinutes(env('API_TOKEN_REFRESH_EXPIRES')));` to `AuthServiceProvider@boot`
* Edit env: `env('ADMIN_USER_EMAIL'), env('API_EMAIL'), env('API_PASSWORD')` for AuthSeeder

## Configuration
* Run `php artisan migrate` command.
* Seed the admin user `php artisan module:seed ApiAuth`. Api email: info@Essprendimai.com, public_token: O5h99fd4GPNIgjlkknkb552sd498
* Publish lang run `php artisan vendor:publish --provider="Essprendimai\ApiAuth\Providers\ApiAuthServiceProvider" --tag=views`

## API
    * login: use `oauth/token` endpoint (json response(200): token_type, expires_in, access_token, refresh_token):
        'username' => 'info@oauth2.com',
        'password' => 'secret',
        'grant_type' => 'password',
        'client_id' => '1',
        'client_secret' => secret from password client dashboard,
        'scope' => '',
    * register user: use `api/oauth/register-user` endpoint (json response(200): status, json response(422): messages, json response(401): message)
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . 'token',
        ],
        'form_params' => [
            'username' => 'info@oauth2k.com',
            'password' => 'secret',
            'grant_type' => 'password',
        ]
    * refresh token: use `oauth/token` endpoint (json response(200): token_type, expires_in, access_token, refresh_token json response(401): error, message)
        'form_params' => [
            'grant_type' => 'refresh_token',
            'refresh_token' => 'refresh token',
            'client_id' => '4',
            'client_secret' => 'client secret',
            'scope' => '',
        ]