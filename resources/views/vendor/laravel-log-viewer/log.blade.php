@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-bug"></i> {{ __('basic::logs.page_log.title') }}
    </h1>
    <p>{{ __('basic::logs.page_log.title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-home"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.logs') }}">{{ __('basic::logs.menu') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="col sidebar mb-3">
                    <div class="list-group div-scroll">
                        @foreach($folders as $folder)
                            <div class="list-group-item">
                                <a href="?f={{ \Illuminate\Support\Facades\Crypt::encrypt($folder) }}">
                                    <span class="fa fa-folder"></span> {{$folder}}
                                </a>
                                @if ($current_folder == $folder)
                                    <div class="list-group folder">
                                        @foreach($folder_files as $file)
                                            <a href="?l={{ \Illuminate\Support\Facades\Crypt::encrypt($file) }}&f={{ \Illuminate\Support\Facades\Crypt::encrypt($folder) }}"
                                               class="list-group-item @if ($current_file == $file) llv-active @endif">
                                                {{$file}}
                                            </a>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        @endforeach
                        @foreach($files as $file)
                            <a href="?l={{ \Illuminate\Support\Facades\Crypt::encrypt($file) }}"
                               class="list-group-item @if ($current_file == $file) llv-active @endif">
                                {{$file}}
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 table-container">
                    @if ($logs === null)
                        <div>
                            {{ __('basic::logs.page_log.title_description') }}
                        </div>
                    @else
                        <table id="table-log" class="table table-striped" data-ordering-index="{{ $standardFormat ? 2 : 0 }}">
                            <thead>
                            <tr>
                                @if ($standardFormat)
                                    <th>{{ __('basic::logs.page_log.table_title_level') }}</th>
                                    <th>{{ __('basic::logs.page_log.table_title_context') }}</th>
                                    <th>{{ __('basic::logs.page_log.table_title_date') }}</th>
                                @else
                                    <th>{{ __('basic::logs.page_log.table_title_line_number') }}</th>
                                @endif
                                <th>{{ __('basic::logs.page_log.table_title_content') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($logs as $key => $log)
                                <tr data-display="stack{{{$key}}}">
                                    @if ($standardFormat)
                                        <td class="nowrap text-{{{$log['level_class']}}}">
                                            <span class="fa fa-{{{$log['level_img']}}}" aria-hidden="true"></span>&nbsp;&nbsp;{{$log['level']}}
                                        </td>
                                        <td class="text">{{$log['context']}}</td>
                                    @endif
                                    <td class="date">{{{$log['date']}}}</td>
                                    <td class="text">
                                        @if ($log['stack'])
                                            <button type="button"
                                                    class="float-right expand btn btn-outline-dark btn-sm mb-2 ml-2"
                                                    data-display="stack{{{$key}}}">
                                                <span class="fa fa-search"></span>
                                            </button>
                                        @endif
                                        {{{$log['text']}}}
                                        @if (isset($log['in_file']))
                                            <br/>{{{$log['in_file']}}}
                                        @endif
                                        @if ($log['stack'])
                                            <div class="stack" id="stack{{{$key}}}"
                                                 style="display: none; white-space: pre-wrap;">{{{ trim($log['stack']) }}}
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    @endif
                    <div class="p-3">
                        @if($current_file)
                            <a href="?dl={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                                <span class="fa fa-download"></span> {{ __('basic::logs.page_log.button_download_file_label') }}
                            </a>
                            -
                            <a id="clean-log" href="?clean={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                                <span class="fa fa-sync"></span> {{ __('basic::logs.page_log.button_clean_file_label') }}
                            </a>
                            -
                            <a id="delete-log" href="?del={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                                <span class="fa fa-trash"></span> {{ __('basic::logs.page_log.button_delete_file_label') }}
                            </a>
                            @if(count($files) > 1)
                                -
                                <a id="delete-all-log" href="?delall=true{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                                    <span class="fa fa-trash-alt"></span> {{ __('basic::logs.page_log.button_delete_all_files_label') }}
                                </a>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Datatables -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function () {
            var datatableLanguages = [];
            datatableLanguages['lt'] = {
                "sEmptyTable": "Nėra duomenų",
                "sProcessing": "Apdorojama...",
                "sLengthMenu": "Rodyti _MENU_ įrašus",
                "sZeroRecords": "Įrašų nerasta",
                "sInfo": "Rodomi įrašai nuo _START_ iki _END_ iš _TOTAL_ įrašų",
                "sInfoEmpty": "Rodomi įrašai nuo 0 iki 0 iš 0",
                "sInfoFiltered": "(atrinkta iš _MAX_ įrašų)",
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sSearch": "Ieškoti:",
                "sUrl": "",
                "sLoadingRecords": "Kraunama...",
                "oPaginate": {
                    "sFirst": "Pirmas",
                    "sPrevious": "Ankstesnis",
                    "sNext": "Tolimesnis",
                    "sLast": "Paskutinis"
                }
            };
            datatableLanguages['en'] = {
                "sEmptyTable": "No data available in table",
                "sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
                "sInfoEmpty": "Showing 0 to 0 of 0 entries",
                "sInfoFiltered": "(filtered from _MAX_ total entries)",
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sLengthMenu": "Show _MENU_ entries",
                "sLoadingRecords": "Loading...",
                "sProcessing": "Processing...",
                "sSearch": "Search:",
                "sZeroRecords": "No matching records found",
                "oPaginate": {
                    "sFirst": "First",
                    "sLast": "Last",
                    "sNext": "Next",
                    "sPrevious": "Previous"
                },
                "oAria": {
                    "sSortAscending": ": activate to sort column ascending",
                    "sSortDescending": ": activate to sort column descending"
                }
            };

            $('.table-container tr').on('click', function () {
                $('#' + $(this).data('display')).toggle();
            });
            $('#table-log').DataTable({
                "language": datatableLanguages["{{ App::getLocale() }}"],
                "order": [$('#table-log').data('orderingIndex'), 'desc'],
                "stateSave": true,
                "stateSaveCallback": function (settings, data) {
                    window.localStorage.setItem("datatable", JSON.stringify(data));
                },
                "stateLoadCallback": function (settings) {
                    var data = JSON.parse(window.localStorage.getItem("datatable"));
                    if (data) data.start = 0;
                    return data;
                }
            });
            $('#delete-log, #clean-log, #delete-all-log').click(function () {
                return confirm("{{ __('basic::logs.page_log.alert_delete') }}");
            });
        });
    </script>
@endsection