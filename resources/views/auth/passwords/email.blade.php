@extends('companies::layouts.header')

@section('title', __('companies::titles.auth.forgot_password'))
@section('content')
        <div class="background-color justify-content-center m-auto">
            <div class="row justify-content-center align-items-center background-white m-auto">
                <div class="cla">
                    <div class="container">
                        <h2 class="text-center pb-5">{{ __('basic::passwords.page_title') }}</h2>
                        <p class="text-center">
                            {{ __('companies::companies.form.pass_reset_text') }}
                        </p>
                    </div>

                    <div class="col-12 login-sec float-left">
                        <div class="panel-body">

                            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <div class="col-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="email" class="form-control border-radius input-width input-custom-style" name="email" value="{{ old('email') }}" required @if( \Lang::has('companies::companies.placeholder.email') ) placeholder="{{ __('companies::companies.placeholder.email') }}" @endif>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                              {{ $errors->first('email') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row m-0 p-0 button-margin">
                                    <div class="col-12 ">
                                        <button type="submit" class="button button-dark input-width border-radius input-custom-style-button">
                                            {{ __('basic::passwords.reset_link') }}
                                        </button>
                                    </div>
                                    <div class="col-6 text-left">
                                        <a class="href-color" href="{{ route('client.login') }}">{{ __('basic::passwords.login') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
