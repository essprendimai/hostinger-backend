@extends('companies::layouts.header', ['hideHeaderMessages' => true])

@section('title', __('companies::titles.auth.forgot_password'))
@section('content')
        <div class="background-color justify-content-center m-auto">
            <div class="row justify-content-center align-items-center background-white m-auto">
                <div class="cla">
                    <h2 class="text-center pb-5">{{ __('basic::passwords.page_title') }}</h2>
                    <div class="col-12 login-sec float-left">
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                                {{ csrf_field() }}

                                <input type="hidden" name="token" value="{{ $token }}">

                                <div class="form-group">
                                    <div class="col-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="email" class="form-control border-radius input-width input-custom-style" name="email" value="@if( old('email')){{old('email')}}@else{{$email}}@endif" required autofocus @if( \Lang::has('companies::companies.placeholder.email') ) placeholder="{{ __('companies::companies.placeholder.email') }}" @endif>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                {{ $errors->first('email') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input id="password" type="password" class="form-control border-radius input-width input-custom-style" name="password" required @if( \Lang::has('companies::companies.placeholder.email') ) placeholder="{{ __('companies::companies.placeholder.password') }}" @endif>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                {{ $errors->first('password') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-12 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <input id="password-confirm" type="password" class="form-control border-radius input-width input-custom-style" name="password_confirmation" required @if( \Lang::has('companies::companies.placeholder.email') ) placeholder="{{ __('companies::companies.placeholder.password_confirm') }}" @endif>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row m-0 p-0 button-margin">
                                    <div class="col-12">
                                        <button type="submit" class="button button-dark input-width border-radius input-custom-style-button">{{ __('basic::passwords.reset_button') }}</button>
                                    </div>
                                    <div class="col-6 text-left">
                                        <a href="{{ route('client.login') }}" class="href-color">{{ __('basic::passwords.login') }}</a>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a class="href-color" href="{{ route('welcome') }}">{{  __('basic::register.client_page.buttons.home') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
