@extends('companies::layouts.header', ['hideHeaderMessages' => true])

@section('title', __('companies::titles.auth.register'))
@section('content')
        <div class="background-color justify-content-center m-auto">
            <div class="row justify-content-center align-items-center background-white m-auto">
                <div class="cla">
                    <h2 class="text-center pb-5">{{  __('basic::register.client_page.title') }}</h2>
                    <div class="col-12 login-sec float-left">
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('client.register') }}">
                                {{ csrf_field() }}

                                <div class="form-group ">
                                    <div class="col-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="email" class="form-control border-radius input-width input-custom-style" name="email" value="{{ old('email') }}" @if( \Lang::has('companies::companies.placeholder.email') ) placeholder="{{ __('companies::companies.placeholder.email') }}" @endif required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                               {{ $errors->first('email') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="col-12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input id="password" type="password" class="form-control border-radius input-width input-custom-style" name="password" required @if( \Lang::has('companies::companies.placeholder.password') ) placeholder="{{ __('companies::companies.placeholder.password') }}" @endif>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                {{ $errors->first('password') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="col-12">
                                        <input id="password-confirm" type="password" class="form-control border-radius input-width input-custom-style " name="password_confirmation" required @if( \Lang::has('companies::companies.placeholder.password_confirm') ) placeholder="{{ __('companies::companies.placeholder.password_confirm') }}" @endif>
                                    </div>
                                </div>

                                <div class="row m-1 p-0">
                                    <div class="col-12 mb-0 {{ $errors->has('agree_to_privacy_policy') ? ' has-error' : '' }}">
                                        <div class="animated-checkbox">
                                            <label class="label-color">
                                                <input type="checkbox" value="1" id="agree_to_privacy_policy" name="agree_to_privacy_policy" />
                                                <span class="label-text font-size-14 font-weight-normal">
                                                 <a href=" {{  __('companies::companies.form.register_button_link') }}">
                                                      {{  __('companies::companies.form.register_button') }}
                                                 </a>
                                            </span>
                                            </label>
                                        </div>
                                        @if ($errors->has('agree_to_privacy_policy'))
                                            <span class="help-block">
                                               {{ $errors->first('agree_to_privacy_policy') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-12 mb-0button-margin">
                                        <button type="submit" class="button button-dark input-width border-radius input-custom-style-button">
                                            {{  __('basic::register.client_page.buttons.register') }}
                                        </button>
                                    </div>
                                    <div class="col-6  text-left">
                                        <a class="href-color" href="{{ route('client.login') }}">
                                            {{  __('basic::register.client_page.buttons.login') }}
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
