@extends('basic-view::layouts.app-gate')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-md-offset-5">
                <h1>{{ config('app.name') }}</h1>
            </div>
        </div>
    </div>
@endsection
