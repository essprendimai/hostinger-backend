<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Parametras :attribute turi bųti priimtinas.',
    'active_url'           => 'Parametras :attribute nėra nuoroda (URL).',
    'after'                => 'Parametras :attribute datos privalo būti prieš :date.',
    'after_or_equal'       => 'Parametras :attribute datos privalo būti prieš arba lygi :date.',
    'alpha'                => 'Parametras :attribute turi susidaryti tik iš raidžių.',
    'alpha_dash'           => 'Parametras :attribute turi susidaryti tik iš raidžių, skaičių ir ženklų.',
    'alpha_num'            => 'Parametras :attribute turi susidaryti tik iš raidžių ir skaičių.',
    'array'                => 'Parametras :attribute privalo būti masyvas.',
    'before'               => 'Parametras :attribute datos privalo būti po :date.',
    'before_or_equal'      => 'Parametras :attribute datos privalo būti po arba lygi :date.',
    'between'              => [
        'numeric' => 'Parametras :attribute privalo būti tarp :min ir :max.',
        'file'    => 'Parametras :attribute privalo būti tarp :min ir :max kilobaitų.',
        'string'  => 'Parametras :attribute privalo būti tarp :min ir :max simbolių.',
        'array'   => 'Parametras :attribute privalo būti tarp :min ir :max duomenų.',
    ],
    'boolean'              => 'Laukelio :attribute reikšmė privalo būi teigiama arba neigiama.',
    'confirmed'            => 'Parametras :attribute nesutampa.',
    'date'                 => 'Parametras :attribute nėra data.',
    'date_format'          => 'Parametras :attribute nesutampa su formatu :format.',
    'different'            => 'Parametras :attribute ir :other privalo būti skirtingi.',
    'digits'               => 'Parametras :attribute privalo būti :digits skaitmenų.',
    'digits_between'       => 'Parametras :attribute privalo būti tarp :min ir :max skaitmenų.',
    'dimensions'           => 'Parametras :attribute turi klaidas paveiksliuko dimensijas.',
    'distinct'             => 'Parametras :attribute turi pasikartojančias reikšmes.',
    'email'                => 'Parametras :attribute turi būti elektroninis paštas.',
    'exists'               => 'Pasirinktas parametras :attribute klaidingas.',
    'file'                 => 'Parametras :attribute privalo būti failas.',
    'filled'               => 'Parametras :attribute turi turėti reikšmę.',
    'image'                => 'Parametras :attribute turi būti paveiksliukas.',
    'in'                   => 'Pasirinktas parametras :attribute neteisingas.',
    'in_array'             => 'Parametras :attribute ne egzistuoja tarp :other.',
    'integer'              => 'Parametras :attribute privalo būti skaičius.',
    'ip'                   => 'Parametras :attribute privalo būti IP adresas.',
    'ipv4'                 => 'Parametras :attribute privalo būti IPv4 adresas.',
    'ipv6'                 => 'Parametras :attribute privalo būti IPv6 adresas.',
    'json'                 => 'Parametras :attribute privalo būti JSON tekstas.',
    'max'                  => [
        'numeric' => 'Parametras :attribute negali būti didesnis už :max.',
        'file'    => 'Parametras :attribute negali būti didesnis už :max kilobaitų.',
        'string'  => 'Parametras :attribute negali būti didesnis už :max simbolių.',
        'array'   => 'Parametras :attribute negali būti didesnis už :max duomenų.',
    ],
    'mimes'                => 'Parametras :attribute privalo būti aprašytas, kaip: :values.',
    'mimetypes'            => 'Parametras :attribute privalo būti aprašytas, kaip: :values.',
    'min'                  => [
        'numeric' => 'Parametras :attribute turi būti mažiausiai :min.',
        'file'    => 'Parametras :attribute turi būti mažiausiai :min kilobaitų.',
        'string'  => 'Parametras :attribute turi būti mažiausiai :min simbolių.',
        'array'   => 'Parametras :attribute turi būti mažiausiai :min duomenų.',
    ],
    'not_in'               => 'Pasirinktas parametras :attribute ne teisingas.',
    'numeric'              => 'Parametras :attribute privalo būti skaičius.',
    'present'              => 'Parametras :attribute privalo būti pateiktas.',
    'regex'                => 'Parametro :attribute formatas neteisingas.',
    'required'             => 'Parametras :attribute yra privalomas.',
    'required_if'          => 'Parametras :attribute yra privalomas, kai :other yra :value.',
    'required_unless'      => 'Parametras :attribute yra privalomas nebent :other yra tarp :values.',
    'required_with'        => 'Parametras :attribute yra privalomas, kai :values yra pateiktas.',
    'required_with_all'    => 'Parametras :attribute yra privalomas, kai :values yra pateiktas.',
    'required_without'     => 'Parametras :attribute yra privalomas, kai :values nėra pateiktas.',
    'required_without_all' => 'Parametras :attribute yra privalomas, kai nėra nei vieno iš :values pateiktų.',
    'same'                 => 'Parametras :attribute ir :other privalo sutapti.',
    'size'                 => [
        'numeric' => 'Parametras :attribute privalo būti :size.',
        'file'    => 'Parametras :attribute privalo būti :size kilobaitų.',
        'string'  => 'Parametras :attribute privalo būti :size simbolių.',
        'array'   => 'Parametras :attribute privalo susidėti iš :size duomenų.',
    ],
    'string'               => 'Parametras :attribute privalo būti tekstas.',
    'timezone'             => 'Parametras :attribute privalo būti teisingos laiko zonos.',
    'unique'               => 'Jau ši :attribute reikšmė naudojama.',
    'uploaded'             => 'Parametrą :attribute nepavyko įkelti.',
    'url'                  => 'Parametro :attribute formatas yra klaidingas.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

    'exists_group' => 'Vartotojas :userEmail egzistuoja :group grupėje.',

    'success_create' => 'Sėkmingai sukurta',
    'success_update' => 'Sėkmingai atnaujinta',
    'success_delete' => 'Sėkmingai ištrinta',

    'failed_create' => 'Nepavyko sukurti',
    'failed_update' => 'Nepavyko atnaujint',
    'failed_delete' => 'Nepavyko ištrinti',
];
