<?php

declare(strict_types = 1);

return [
    'language' => 'Kalba',

    'lithuanian' => 'Lietuvių',
    'english' => 'Anglų',
];
