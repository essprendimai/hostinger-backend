<?php

return [
    'change_page'   => [
        'button_update'                 => 'Atnaujinti',
        'field_email'                   => 'Elektroninis paštas',
        'field_force_password_change'   => 'Priverstinai pakeisti slaptažodį',
        'field_name'                    => 'Vardas',
        'field_password'                => 'Slaptažodis',
        'field_roles'                   => 'Teisės',
        'title'                         => 'Vartotojas',
        'title_description'             => 'Vartotojo duomenys',
    ],
    'menu'          => [
        'roles' => 'Teisės',
        'users' => 'Vartotojai',
    ],
    'user'          => 'Vartotojai',
    'users_page'    => [
        'confirm_delete'        => 'Tikrai trinti?',
        'create'                => 'Sukurti',
        'current_users'         => 'Egzistuojantys vartotojai',
        'delete'                => 'ištrinti',
        'email'                 => 'Elektroninis paštas',
        'force_password_change' => 'Priverstinai pakeisti slaptažodį',
        'name'                  => 'Vardas',
        'password'              => 'Slaptažodis',
        'roles'                 => 'Teisės',
        'sub_title'             => 'Sukurti naują vartotoją',
        'title'                 => 'Vartotojai',
    ],
];
