<?php

declare(strict_types = 1);

return [
    'role' => 'Teisė',

    'roles_page' => [
        'title' => 'Teisės',
        'title_description' => 'Vartotojų teisių redagavimas',

        'title_new' => 'Sukurti naują teisę',
        'title_new_description' => 'Sukurti nauają tesę su kuriomis gali pasiekti puslapius',

        'title_edit' => 'Redaguoti :attribute',
        'title_edit_description' => 'Redaguoti suteiktas teises',

        'table_label_role' => 'Teisė',
        'table_label_full_access' => 'Pilnos teisės',
        'table_label_accessible_routes' => 'Kuriuos puslapius gali pasiekti(raktas)',
        'table_label_description' => 'Aprašymas',
        'table_label_action' => 'Veiksmai',

        'button_edit' => 'Redaguoti',
        'button_delete' => 'Trinti',
        'button_create' => 'Sukurti',
        'button_save' => 'Išsaugoti',
        'button_check_all' => 'Viską pažymėti',
        'button_uncheck_all' => 'Viską atžymėti',

        'alert_delete_title' => 'Trinti teisę',
        'alert_delete' => 'Ar tikrai trinti teisę?',

        'form_name' => 'Vardas',
        'form_description' => 'Aprašymas',

        'yes' => 'Taip',
        'no' => 'Ne',
        'new' => 'Naujas',
    ],
];
