<?php

declare(strict_types = 1);

return [
    'route_name' => 'puslapis',
    'pages' => 'Puslapiai',

    'menu' => [
        'categories' => 'Kategorijos',
        'pages' => 'Puslapiai',
        'new' => 'Naujas',
    ],

    'pages_page' => [
        'title' => 'Puslapiai',
        'title_description' => 'Visų puslapių sąrašas',

        'new_title' => 'Naujas puslapis',
        'new_title_description' => 'Sukurti naują puslapį',

        'edit_title' => 'Puslapio redagavimas',
        'edit_title_description' => 'Redaguoti visus puslapius susijusias su ":attribute" raktu',

        'category_id' => 'Puslapis priskirtas kategorijai',
        'page_title' => 'Titulinis',
        'page_url' => 'Puslapio nuoroda',
        'page_text' => 'Puslapio tekstas',
        'create' => 'Sukurti',
        'key' => 'Raktažodis',
        'delete' => 'Ištrinti',
        'text' => 'Tekstas',
        'confirm_delete' => 'Tikrai norite ištrinti?',
        'actions' => 'Parinktys',
        'empty_list' => 'Sąrašas tuščias',
        'edit' => 'Redaguoti',
    ],

    'categories_page' => [
        'title' => 'Kategorijos',
        'title_description' => 'Kategorijų sąrašas',

        'new_title' => 'Nauja kategorija',
        'new_title_description' => 'Sukurti naują kategoriją',

        'edit_title' => 'Kategorijos redagavimas',
        'edit_title_description' => 'Redaguoti visas kategorijas susijusias su ":attribute" raktu',

        'create' => 'Sukurti',
        'category_title' => 'Pavadinimas',
        'category_url' => 'Kategorijos nuoroda',
        'key' => 'Raktažodis',
        'delete' => 'Ištrinti',
        'confirm_delete' => 'Tikrai norite ištrinti?',
        'actions' => 'Parinktys',
        'empty_list' => 'Sąrašas tuščias',
        'edit' => 'Redaguoti',
    ],
];
