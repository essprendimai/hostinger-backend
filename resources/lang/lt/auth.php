<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Pateikti duomenys nesutampa su mūsų duomenimis.',
    'throttle' => 'Per daug kartų bandyta prisijungti. Bandykite dar kartą po :seconds sekundžių.',

];
