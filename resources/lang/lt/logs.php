<?php

declare(strict_types = 1);

return [
    'menu' => 'Klaidos',

    'page_log' => [
        'title' => 'Klaidos',
        'title_description' => 'Visos klaidos susijusios su šia sistema',

        'max_log_file' => 'Failus didesnius nei > 50M parsisiųskite.',

        'table_title_level' => 'Lygis',
        'table_title_context' => 'Sistemos aplinka',
        'table_title_date' => 'Data',
        'table_title_line_number' => 'Eilės numeris',
        'table_title_content' => 'Pranešimas',

        'button_download_file_label' => 'Siųstis failus',
        'button_clean_file_label' => 'Išvalyti failą',
        'button_delete_file_label' => 'Ištrinti failą',
        'button_delete_all_files_label' => 'Ištrinti visus failus',

        'alert_delete' => 'Ar tikrai?',
    ],
];
