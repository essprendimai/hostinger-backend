<?php

declare(strict_types = 1);

return [
    'route_name' => 'page',
    'pages' => 'Pages',

    'menu' => [
        'categories' => 'Categories',
        'pages' => 'Pages',
        'new' => 'New',
    ],

    'pages_page' => [
        'title' => 'Pages',
        'title_description' => 'List of all pages',

        'new_title' => 'New page',
        'new_title_description' => 'Create new category',

        'edit_title' => 'Edit page',
        'edit_title_description' => 'Edit all pages with ":attribute" key',

        'category_id' => 'Page related to category',
        'page_title' => 'Title',
        'page_url' => 'Page url',
        'page_text' => 'Page content',
        'create' => 'Create',
        'key' => 'Key',
        'text' => 'Text',
        'delete' => 'Delete',
        'confirm_delete' => 'Confirm delete?',
        'actions' => 'Actions',
        'empty_list' => 'Empty list',
        'edit' => 'Edit',
    ],

    'categories_page' => [
        'title' => 'Categories',
        'title_description' => 'List of all categories',

        'new_title' => 'New category',
        'new_title_description' => 'Create new category',

        'edit_title' => 'Edit category',
        'edit_title_description' => 'Edit all categories with ":attribute" key',

        'create' => 'Create',
        'category_title' => 'Title',
        'category_url' => 'Category url',
        'key' => 'Key',
        'delete' => 'Delete',
        'confirm_delete' => 'Confirm delete?',
        'actions' => 'Actions',
        'empty_list' => 'Empty list',
        'edit' => 'Edit',
    ],
];
