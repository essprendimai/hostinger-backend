<?php

declare(strict_types = 1);

return [
    'language' => 'Language',

    'lithuanian' => 'Lithuania',
    'english' => 'English',
];
