<?php

declare(strict_types = 1);

return [
    'role' => 'Role',

    'roles_page' => [
        'title' => 'Roles',
        'title_description' => 'Users available roles',

        'title_new' => 'Create new role',
        'title_new_description' => 'Create new roles for use to access pages',

        'title_edit' => 'Edit :attribute',
        'title_edit_description' => 'Edit page access rights',

        'table_label_role' => 'Role',
        'table_label_full_access' => 'Full access',
        'table_label_accessible_routes' => 'Accessible routes',
        'table_label_description' => 'Description',
        'table_label_action' => 'Actions',

        'button_edit' => 'Edit',
        'button_delete' => 'Delete',
        'button_create' => 'Create',
        'button_save' => 'Save',
        'button_check_all' => 'Check All',
        'button_uncheck_all' => 'Uncheck All',

        'alert_delete_title' => 'Delete role',
        'alert_delete' => 'Confirm delete?',

        'form_name' => 'Name',
        'form_description' => 'Description',

        'yes' => 'Yes',
        'no' => 'No',
        'new' => 'New',
    ],
];
