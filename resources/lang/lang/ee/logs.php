<?php

return [
    'menu'      => 'Logid',
    'page_log'  => [
        'alert_delete'                  => 'Oled sa kindel?',
        'button_clean_file_label'       => 'Puhastage fail',
        'button_delete_all_files_label' => 'Kustuta kõik failid',
        'button_delete_file_label'      => 'Kustuta fail',
        'button_download_file_label'    => 'Lae fail alla',
        'max_log_file'                  => 'Logifail> 50M, palun laadige see alla.',
        'table_title_content'           => 'Sisu',
        'table_title_context'           => 'Sisu',
        'table_title_date'              => 'Kuupäev',
        'table_title_level'             => 'Tase',
        'table_title_line_number'       => 'Liini number',
        'title'                         => 'Logid',
        'title_description'             => 'Kõik administraatori paneeli nurjunud toimingud',
    ],
];
