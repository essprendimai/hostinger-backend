<?php

return [
    'failed_to_send'            => 'Teie sõnumi saatmine ebaõnnestus',
    'send_message_to_users'     => [
        'labels'    => [
            'message'   => 'Sõnum',
        ],
        'subject'   => 'Sõnum',
    ],
    'send_reset_password'       => [
        'footer'        => 'Kui te ei taotlenud parooli lähtestamist, ei ole vaja täiendavaid toiminguid.',
        'reset_button'  => 'Lähtesta parool',
        'subject'       => 'Lähtesta parool',
        'title'         => 'Saate selle meili, sest saime teie kontole parooli lähtestamise taotluse.',
    ],
    'successfully_sent'         => 'Teie sõnum saadeti',
    'verify_draft_user_email'   => [
        'labels'    => [
            'button_text'               => 'Kinnita e-maili aadress',
            'if_you_not_create_ignore'  => 'Kui te ei loonud kontot, ei ole edasine tegevus vajalik.',
            'password'                  => 'Teie ajutine parool:',
            'please_click'              => 'Oma e-posti aadressi kinnitamiseks klõpsake alloleval nupul.',
        ],
        'subject'   => 'Kinnita e-maili aadress',
    ],
    'verify_mail'               => [
        'activate'  => 'Kinnita e-maili aadress',
        'footer'    => 'Kui te ei loonud kontot, ei ole edasine tegevus vajalik.',
        'subject'   => 'Kinnita e-maili aadress',
        'title'     => 'Oma e-posti aadressi kinnitamiseks klõpsake alloleval nupul.',
    ],
];
