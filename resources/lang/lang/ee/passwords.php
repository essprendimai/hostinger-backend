<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'email'                     => 'E-Mail Address',
    'login'                     => 'E-posti aadress',
    'page_title'                => 'Lähtesta parool',
    'password'                  => 'Paroolid peavad olema vähemalt kuus tähemärki ja vastama kinnitusele.',
    'password_confirm_field'    => 'Kinnita salasõna',
    'password_field'            => 'Parool',
    'reset'                     => 'Teie parool on nullitud!',
    'reset_button'              => 'Lähtesta parool',
    'reset_link'                => 'Saada parooli lähtestamise link',
    'sent'                      => '„Oleme oma parooli lähtestamise lingi e-posti teel saatnud!',
    'token'                     => 'See parooli lähtestamise märgis on kehtetu.',
    'user'                      => 'Me ei leia selle e-posti aadressiga kasutajat.',
];
