<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'                => 'Need volikirjad ei vasta meie andmetele.',
    'throttle'              => 'Liiga palju sisselogimiskatseid. Proovige uuesti: sekundites sekundites.',
    'verify'                => 'Kinnitage oma e-kiri',
    'verify_account_email'  => 'Kinnituslingiga meil on saadetud, kontrollige oma postkasti ja kinnitage oma konto!',
];
