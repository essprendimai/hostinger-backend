<?php

return [
    'edit_page'         => [
        'buttons'   => [
            'change'    => 'Muuda',
        ],
        'form'      => [
            'password'                  => 'Parool',
            'password_new'              => 'Uus salasõna',
            'password_new_confirmation' => 'Kinnita Uus salasõna',
        ],
        'title'     => 'Muuda profiili',
    ],
    'error_messages'    => [
        'password_do_not_match' => 'Halb parool',
    ],
];
