<?php

return [
    'role'          => 'Role',
    'roles_page'    => [
        'alert_delete'                  => 'Confirm delete?',
        'alert_delete_title'            => 'Delete role',
        'button_check_all'              => 'Check All',
        'button_create'                 => 'Create',
        'button_delete'                 => 'Delete',
        'button_edit'                   => 'Edit',
        'button_save'                   => 'Save',
        'button_uncheck_all'            => 'Uncheck All',
        'form_description'              => 'Description',
        'form_name'                     => 'Name',
        'new'                           => 'New',
        'no'                            => 'No',
        'table_label_accessible_routes' => 'Accessible routes',
        'table_label_action'            => 'Actions',
        'table_label_description'       => 'Description',
        'table_label_full_access'       => 'Full access',
        'table_label_role'              => 'Role',
        'title'                         => 'Roles',
        'title_description'             => 'Users available roles',
        'title_edit'                    => 'Edit :attribute',
        'title_edit_description'        => 'Edit page access rights',
        'title_new'                     => 'Create new role',
        'title_new_description'         => 'Create new roles for use to access pages',
        'yes'                           => 'Yes',
    ],
];
