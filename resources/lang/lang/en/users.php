<?php

return [
    'change_page'   => [
        'button_update'                 => 'Update',
        'field_email'                   => 'Email',
        'field_force_password_change'   => 'Force to change password',
        'field_name'                    => 'Name',
        'field_password'                => 'Password',
        'field_roles'                   => 'Roles',
        'title'                         => 'User',
        'title_description'             => 'User data',
    ],
    'menu'          => [
        'roles' => 'Roles',
        'users' => 'Users',
    ],
    'user'          => 'User',
    'users_page'    => [
        'confirm_delete'        => 'Confirm delete?',
        'create'                => 'Create',
        'current_users'         => 'Current Users',
        'delete'                => 'delete',
        'email'                 => 'Email',
        'force_password_change' => 'Force to change password',
        'name'                  => 'Name',
        'password'              => 'Password',
        'roles'                 => 'Roles',
        'sub_title'             => 'Create new User',
        'title'                 => 'Users99',
    ],
];
