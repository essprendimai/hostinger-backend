<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password'  => 'Slaptažodis privalo būti mažiausiai 6 simbolių ir sutapti su teisėmis.',
    'reset'     => 'Slaptažodis atnaujintas!',
    'sent'      => 'Mes nusiuntėle patvirtinimo nuorodą nurodytu paštu!',
    'token'     => 'Slaptažodžio susigrąžinimo raktas neteisingas.',
    'user'      => 'Negalime rasti vartotojo su tokiu elektroniniu paštu.',
];
