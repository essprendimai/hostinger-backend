<?php

declare(strict_types = 1);

namespace App\Repositiories;

use App\Entities\Job;
use Essprendimai\Basic\Enums\Enumerable;
use Essprendimai\Basic\Repositories\Repository;
use Illuminate\Support\Collection;
use InvalidArgumentException;
use RuntimeException;

/**
 * Class JobRepository
 * @package App\Repositiories
 */
class JobRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Job::class;
    }

    /**
     * @param Enumerable $queue
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function deleteByQueue(Enumerable $queue)
    {
        $this->deleteWhere([
            'queue' => $queue->id(),
        ]);
    }

    /**
     * @param Enumerable $queue
     * @return Collection
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function getAllByQueue(Enumerable $queue): Collection
    {
        return $this->findAllBy([
            'queue' => $queue->id(),
        ]);
    }
}
