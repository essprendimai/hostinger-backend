<?php

declare(strict_types = 1);

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Job
 * @package App\Entities
 */
class Job extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'jobs';

    /**
     * @var array
     */
    protected $fillable = [
        'queue',
        'payload',
        'attempts',
        'reserved_at',
        'available_at',
        'created_at',
    ];
}
