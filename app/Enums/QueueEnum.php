<?php

declare(strict_types = 1);

namespace App\Enums;

use Essprendimai\Basic\Enums\Enumerable;

/**
 * Class QueueEnum
 * @package App\Enums
 */
class QueueEnum extends Enumerable
{
    /**
     * @return QueueEnum
     * @throws \ReflectionException
     */
    final public static function defaultQueue(): self
    {
        return self::make('default', 'Default', 'Default queue');
    }
}
