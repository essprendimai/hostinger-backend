<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Repositories;

use Essprendimai\ApiAuth\DTO\AuthDTO;
use Essprendimai\ApiAuth\Entities\Auth;
use Essprendimai\ApiAuth\Enums\AuthType;
use Essprendimai\Basic\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use RuntimeException;

/**
 * Class AuthRepository
 * @package Essprendimai\ApiAuth\Repositories
 */
class AuthRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Auth::class;
    }

    /**
     * @return Collection
     * @throws RuntimeException
     */
    public function allToDTO(): Collection
    {
        return $this->all()
            ->map(function(Auth $auth) {
                return $this::toDTO($auth);
            });
    }

    /**
     * @param Auth $auth
     * @return AuthDTO
     * @throws \Exception
     */
    public static function toDTO(Auth $auth): AuthDTO
    {
        return (new AuthDTO())
            ->setId($auth->id)
            ->setEmail($auth->email)
            ->setPrivateToken((string)$auth->private_token)
            ->setPublicToken((string)$auth->public_token)
            ->setToken((string)$auth->token)
            ->setType(AuthType::from($auth->type))
            ->setName((string)$auth->name)
            ->setPassword((string)$auth->password)
            ->setClients($auth->clients ? $auth->clients : null);
    }

    /**
     * @param AuthDTO $auth
     * @return Model
     */
    public function createFromData(AuthDTO $auth): Model
    {
        return $this->create([
            'email' => $auth->getEmail(),
            'private_token' => $auth->getPrivateToken(),
            'public_token' => $auth->getPublicToken(),
            'token' => $auth->getToken(),
            'type' => $auth->getType()->id(),
        ]);
    }

    /**
     * @param string $publicToken
     * @param string $email
     * @return AuthDTO|null
     * @throws \Exception
     */
    public function getAuth(string $publicToken, string $email): ? AuthDTO
    {
        $auth = $this->makeQuery()
            ->where('email', $email)
            ->where('public_token', $publicToken)
            ->first();

        if ($auth) {
            return (new AuthDTO())
                ->setId($auth->id)
                ->setEmail($auth->email)
                ->setPublicToken((string)$auth->public_token)
                ->setPrivateToken((string)$auth->private_token)
                ->setToken((string)$auth->token)
                ->setType(AuthType::from($auth->type))
                ->setName((string)$auth->name)
                ->setPassword((string)$auth->password)
                ->setClients($auth->clients ? $auth->clients : null);
        }

        return null;
    }

    /**
     * @param string $column
     * @param string $operator
     * @param string $value
     * @return Collection
     */
    public function where(string $column, string $operator, string $value): Collection
    {
        return $this->makeQuery()->where($column, $operator, $value)->get();
    }
}
