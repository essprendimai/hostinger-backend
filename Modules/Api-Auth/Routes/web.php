<?php

declare(strict_types = 1);

use Essprendimai\Basic\Http\Middleware\LimitsBackendRouteAccess;
use Illuminate\Auth\Middleware\Authenticate;

Route::group([
    'middleware' => [
        'auth',
        LimitsBackendRouteAccess::class
    ],
], function() {
    Route::resource('apiauth', 'ApiAuthController', [
        'expext' => [
            'show',
            'edit',
        ],
    ]);
});

Route::group([
    'middleware' => ['web', Authenticate::class, LimitsBackendRouteAccess::class],
], function(){
    Route::resource('apiauthpassword', 'OAuth2BackendController');
});
