<?php

declare(strict_types = 1);

Route::post('api/oauth/register-user', 'OAuth2Controller@register');

Route::resource('api/oauth/users', 'OAuth2Controller', [
    'except' => [
        'edit', 'create',
    ],
]);