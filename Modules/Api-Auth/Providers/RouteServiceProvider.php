<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

/**
 * Class RouteServiceProvider
 * @package Essprendimai\ApiAuth\Providers
 */
class RouteServiceProvider extends ServiceProvider
{
    /** @see ServiceProvider::$namespace */
    protected $namespace = 'Essprendimai\ApiAuth\Http\Controllers';

    /**
     * Define the routes for the application.
     */
    public function map(): void
    {
        if (!app()->routesAreCached()) {
            $this->mapApiRoutes();
            $this->mapWebRoutes();
        }
    }

    /**
     * Map web routes
     *
     * @return void
     */
    private function mapApiRoutes(): void
    {
        Route::middleware(['auth:api'])
            ->namespace($this->namespace)
            ->group($this->modulePath('Routes/api.php'));
    }

    private function mapWebRoutes(): void
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->prefix('backend')
            ->as('backend.')
            ->group($this->modulePath('Routes/web.php'));
    }

    /**
     * @param string $path
     * @return string
     */
    private function modulePath(string $path): string
    {
        return __DIR__ . '/../' . $path;
    }
}
