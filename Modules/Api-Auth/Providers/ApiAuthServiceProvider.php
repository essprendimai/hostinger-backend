<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Providers;

use Essprendimai\ApiAuth\Entities\Auth;
use Essprendimai\ApiAuth\Http\Handlers\AuthDestroyPasswordHandler;
use Essprendimai\ApiAuth\Http\Handlers\AuthStorePasswordHandler;
use Essprendimai\ApiAuth\Http\Handlers\DeleteAuthHandler;
use Essprendimai\ApiAuth\Http\Handlers\GetAuthHandler;
use Essprendimai\ApiAuth\Http\Handlers\StoreAuthHandler;
use Essprendimai\ApiAuth\Repositories\AuthRepository;
use Essprendimai\ApiAuth\Repositories\OAuthClientUserRepository;
use Essprendimai\ApiAuth\Services\AuthService;
use Essprendimai\ApiAuth\Services\OAuth2UserManager;
use Illuminate\Config\Repository;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Laravel\Passport\Bridge\UserRepository;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * Class ApiAuthServiceProvider
 * @package Essprendimai\ApiAuth\Providers
 */
class ApiAuthServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     */
    public function boot(): void
    {
        $this->registerConfig();
        $this->publishTranslations();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(realpath(__DIR__ . '/../Database/Migrations'));
    }

    /** Register config */
    protected function registerConfig(): void
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('api_auth.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'api_auth'
        );
    }

    /** Register views */
    public function registerViews(): void
    {
        $viewPath = base_path('resources/views/modules/api-auth');
        $sourcePath = __DIR__ . '/../Resources/views';
        $this->publishes([$sourcePath => $viewPath], 'views');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/modules/api-auth';
        }, config('view.paths')), [$sourcePath]), 'api-auth');
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories(): void
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function register(): void
    {
        $this->registerAuthGuard();
        $this->app->bind(UserRepository::class, OAuthClientUserRepository::class);

        $this->registerRepositories();
        $this->registerHandlers();
        $this->registerServices();

        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function registerAuthGuard(): void
    {
        /** @var Repository $configs */
        $configs = $this->app->get('config');
        $configs->set('auth.guards.auth', [
            'driver' => 'passport',
            'provider' => 'auth',
        ]);
        $configs->set('auth.providers.auth', [
            'driver' => 'eloquent',
            'model' => Auth::class,
        ]);
    }

    private function publishTranslations(): void
    {
        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'api-auth');

        $this->publishes([
            __DIR__ . '/../Resources/lang' => resource_path('lang'),
        ], 'lang');
    }

    private function registerRepositories(): void
    {
        $this->app->singleton(AuthRepository::class);
    }

    private function registerHandlers(): void
    {
        $this->app->singleton(DeleteAuthHandler::class);
        $this->app->singleton(GetAuthHandler::class);
        $this->app->singleton(StoreAuthHandler::class);
        $this->app->singleton(AuthDestroyPasswordHandler::class);
        $this->app->singleton(AuthStorePasswordHandler::class);
    }

    private function registerServices(): void
    {
        $this->app->singleton(AuthService::class);
        $this->app->singleton(OAuth2UserManager::class);
    }
}
