<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Services;

use Essprendimai\ApiAuth\Entities\Auth;
use Essprendimai\ApiAuth\Enums\AuthType;
use Essprendimai\ApiAuth\Repositories\AuthRepository;
use Laravel\Passport\Client;
use Laravel\Passport\ClientRepository;

/**
 * Class OAuth2UserManager
 * @package Essprendimai\OAuth2\Services
 */
class OAuth2UserManager
{
    const DEFAULT_REDIRECT = 'http://localhost/redirect';

    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var AuthRepository
     */
    private $authRepository;

    /**
     * OAuth2UserManager constructor.
     * @param AuthRepository $authRepository
     * @param ClientRepository $clientRepository
     */
    public function __construct(AuthRepository $authRepository, ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->authRepository = $authRepository;
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @return Client
     * @throws \ReflectionException
     * @throws \RuntimeException
     */
    public function createPasswordClient(string $name, string $email, string $password): Client
    {
        /** @var Auth $user */
        $user = $this->authRepository->create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
            'type' => AuthType::Password(),
        ]);
        return $this->clientRepository->createPasswordGrantClient($user->id, $name, self::DEFAULT_REDIRECT);
    }

    /**
     * @param int $userId
     * @param string $name
     * @return Client
     * @throws \RuntimeException
     */
    public function createPasswordType(int $userId, string $name): Client
    {
        return $this->clientRepository->createPasswordGrantClient($userId, $name, self::DEFAULT_REDIRECT);
    }
}
