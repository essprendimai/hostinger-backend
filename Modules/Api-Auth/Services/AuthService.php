<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Services;

use Essprendimai\ApiAuth\DTO\AuthDTO;
use Essprendimai\ApiAuth\Repositories\AuthRepository;
use Illuminate\Support\Facades\Hash;
use InvalidArgumentException;
use RuntimeException;

/**
 * Class AuthService
 * @package Essprendimai\ApiAuth\Services
 */
class AuthService
{
    /**
     * @param null $publicToke
     * @return AuthDTO
     */
    public function generateTokens($publicToke = null): AuthDTO
    {
        $publicToken = $publicToke ? $publicToke : sha1(str_random(8));
        $privateToken = sha1(microtime());
        $token = Hash::make($publicToken . $privateToken);

        return (new AuthDTO())
            ->setPublicToken($publicToken)
            ->setPrivateToken($privateToken)
            ->setToken($token);
    }

    /**
     * @param string $publicToken
     * @param string $email
     * @return bool
     * @throws RuntimeException
     * @throws InvalidArgumentException
     */
    public function check(string $publicToken, string $email): bool
    {
        /** @var AuthRepository $repository */
        $repository = app(AuthRepository::class);
        $auth = $repository->getAuth($publicToken, $email);

        if (!$auth) {
            return false;
        }

        return Hash::check($publicToken . $auth->getPrivateToken(), $auth->getToken());
    }
}
