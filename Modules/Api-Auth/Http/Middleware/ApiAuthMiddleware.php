<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Middleware;

use Closure;
use Essprendimai\ApiAuth\Services\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use LogicException;

/**
 * Class ApiAuthMiddleware
 * @package Essprendimai\ApiAuth\Http\Middleware
 */
class ApiAuthMiddleware
{
    const UNAUTHORIZED = 'Unauthenticated.';

    /** @var AuthService */
    private $authService;

    /**
     * PlayerApiAuthMiddleware constructor.
     * @param AuthService $authService
     * @internal param PlayerService $playerService
     */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return JsonResponse|mixed
     * @throws LogicException
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            if ($this->authService->check($request->input('token'), $request->input('email'))) {
                return $next($request);
            }
        } catch (\Throwable $e) {
            return response()->json(['message' => self::UNAUTHORIZED], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return response()->json(['message' => self::UNAUTHORIZED], JsonResponse::HTTP_UNAUTHORIZED);
    }
}
