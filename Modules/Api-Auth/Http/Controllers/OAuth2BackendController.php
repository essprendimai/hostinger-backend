<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Controllers;

use Essprendimai\ApiAuth\Entities\Auth;
use Essprendimai\ApiAuth\Http\Handlers\AuthDestroyPasswordHandler;
use Essprendimai\ApiAuth\Http\Handlers\AuthStorePasswordHandler;
use Essprendimai\ApiAuth\Http\Requests\AuthPasswordDestroyRequest;
use Essprendimai\ApiAuth\Http\Requests\AuthPasswordRequest;
use Essprendimai\ApiAuth\Repositories\AuthRepository;
use Essprendimai\Basic\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Throwable;

/**
 * Class OAuth2BackendController
 * @package Essprendimai\ApiAuth\Http\Controllers
 */
class OAuth2BackendController extends Controller
{
    /**
     * @return View
     */
    public function create(): View
    {
        return view('api-auth::create-password');
    }

    /**
     * @param AuthPasswordRequest $request
     * @param AuthStorePasswordHandler $handler
     * @return RedirectResponse
     */
    public function store(AuthPasswordRequest $request, AuthStorePasswordHandler $handler): RedirectResponse
    {
        try {
            if($handler->handle($request)) {
                return redirect()->back()->with('success', 'User created successfully');
            }

            return redirect()->back()->with('danger', 'User exists with password type');
        } catch (Throwable $exception) {
            logger()->error($exception, $request->toArray());

            return redirect()->back()->with('danger', 'Failed to create user!');
        }
    }

    /**
     * @param AuthPasswordDestroyRequest $request
     * @param Auth $apiauthpassword
     * @param AuthDestroyPasswordHandler $handler
     * @return RedirectResponse
     */
    public function destroy(AuthPasswordDestroyRequest $request, Auth $apiauthpassword, AuthDestroyPasswordHandler $handler): RedirectResponse
    {
        try {
            $handler->handle($apiauthpassword, $request->getClientId());

            return redirect()->back()->with('success', 'User deleted successfully!');
        } catch (Throwable $exception) {
            logger()->error($exception, $request->toArray());

            return redirect()->back()->with('danger', 'Failed to delete user!');
        }
    }

    /**
     * @param Auth $apiauthpassword
     * @return View
     */
    public function edit(Auth $apiauthpassword): View
    {
        $user = $apiauthpassword;
        return view('api-auth::edit-password', compact('user'));
    }

    /**
     * @param AuthPasswordRequest $request
     * @param Auth $apiauthpassword
     * @param AuthRepository $userRepository
     * @return RedirectResponse
     */
    public function update(AuthPasswordRequest $request, Auth $apiauthpassword, AuthRepository $userRepository): RedirectResponse
    {
        try {
            $userRepository->update([
                'email' => $request->getEmail(),
                'password' => bcrypt($request->getPassword()),
                'name' => $request->getName(),
            ], $apiauthpassword->id);

            return redirect()->back()->with('success', 'User updated successfully');
        } catch (Throwable $exception) {
            logger()->error($exception, $request->toArray());

            return redirect()->back()->with('danger', 'Failed to update user!');
        }
    }
}
