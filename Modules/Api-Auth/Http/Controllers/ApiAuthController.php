<?php

namespace Essprendimai\ApiAuth\Http\Controllers;

use Essprendimai\ApiAuth\Entities\Auth;
use Essprendimai\ApiAuth\Http\Handlers\DeleteAuthHandler;
use Essprendimai\ApiAuth\Http\Handlers\GetAuthHandler;
use Essprendimai\ApiAuth\Http\Handlers\StoreAuthHandler;
use Essprendimai\ApiAuth\Http\Requests\AuthFormRequest;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use LogicException;
use RuntimeException;
use Throwable;

/**
 * Class ApiAuthController
 * @package Essprendimai\ApiAuth\Http\Controllers
 */
class ApiAuthController extends Controller
{
    const FAIL_CREATE_AUTH = 'Faialed to create api user';
    const FAIL_DELETE_AUTH = 'Failed to delete api user';

    const SUCCESS_CREATE_AUTH = 'Api user created';
    const SUCCESS_DELETE_AUTH = 'Api user deleted';

    /**
     * @param GetAuthHandler $handler
     * @return View
     * @throws LogicException
     * @throws RuntimeException
     */
    public function index(GetAuthHandler $handler): View
    {
        $auths = $handler->handle();

        return view('api-auth::index', compact('auths'));
    }

    /**
     * @return Factory|View
     * @throws LogicException
     */
    public function create(): View
    {
        return view('api-auth::create');
    }

    /**
     * @param AuthFormRequest $request
     * @param StoreAuthHandler $handler
     * @return RedirectResponse
     */
    public function store(AuthFormRequest $request, StoreAuthHandler $handler): RedirectResponse
    {
        try {
            $handler->handle($request->resolve());
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.apiauth.index'))
                ->with('danger', self::FAIL_CREATE_AUTH);
        }

        return redirect(route('backend.apiauth.index'))
            ->with('success', self::SUCCESS_CREATE_AUTH);
    }

    /**
     * @param Request $request
     * @param Auth $apiauthpassword
     * @param DeleteAuthHandler $handler
     * @return RedirectResponse
     */
    public function destroy(Request $request, Auth $apiauthpassword, DeleteAuthHandler $handler): RedirectResponse
    {
        try {
            $handler->handle($apiauthpassword);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.apiauth.index'))
                ->with('danger', self::FAIL_DELETE_AUTH);
        }

        return redirect(route('backend.apiauth.index'))
            ->with('success', self::SUCCESS_DELETE_AUTH);
    }
}
