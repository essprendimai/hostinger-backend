<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Controllers;

use Essprendimai\ApiAuth\Http\Requests\ApiUserStoreRequest;
use Essprendimai\ApiAuth\Http\Requests\RegisterUserPasswordRequest;
use Essprendimai\ApiAuth\Services\OAuth2UserManager;
use Essprendimai\Basic\Entities\User;
use Essprendimai\Basic\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Throwable;

/**
 * Class OAuth2Controller
 * @package Modules\OAuth2\Http\Controllers
 */
class OAuth2Controller extends Controller
{
    const MESSAGE_SOMETHING_WENT_WRONG = 'Something went wrong';
    const MESSAGE_FAIL = 'FAIL';
    const MESSAGE_OK = 'OK';

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * OAuth2Controller constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \LogicException
     */
    public function index(Request $request): JsonResponse
    {
        try {
            $users = $this->userRepository->getAllUsers();
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return response()->json([
                'message' => self::MESSAGE_SOMETHING_WENT_WRONG,
                'status' => self::MESSAGE_FAIL,
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'data' => $users,
            'status' => self::MESSAGE_OK,
        ], JsonResponse::HTTP_OK);
    }

    /**
     * @param ApiUserStoreRequest $request
     * @return JsonResponse
     * @throws \LogicException
     */
    public function store(ApiUserStoreRequest $request): JsonResponse
    {
        try {
            $this->userRepository->create([
                'email' => $request->getEmail(),
                'password' => bcrypt($request->getPassword()),
                'name' => $request->getEmail(),
                'is_admin' => 0,
            ]);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return response()->json([
                'message' => self::MESSAGE_SOMETHING_WENT_WRONG,
                'status' => self::MESSAGE_FAIL,
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'status' => self::MESSAGE_OK,
        ], JsonResponse::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws \LogicException
     */
    public function destroy(Request $request, int $id): JsonResponse
    {
        try {
            $this->userRepository->delete($id);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return response()->json([
                'message' => self::MESSAGE_SOMETHING_WENT_WRONG,
                'status' => self::MESSAGE_FAIL,
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'status' => self::MESSAGE_OK,
        ], JsonResponse::HTTP_OK);
    }


    /**
     * @param Request $request
     * @param string $key
     * @return JsonResponse
     * @throws \LogicException
     */
    public function update(Request $request, string $key): JsonResponse
    {
        try {
            // update user
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return response()->json([
                'message' => self::MESSAGE_SOMETHING_WENT_WRONG,
                'status' => self::MESSAGE_FAIL,
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'aha' => 'update user',
            'status' => self::MESSAGE_OK,
        ], JsonResponse::HTTP_OK);
    }


    /**
     * @param Request $request
     * @param string $key
     * @return JsonResponse
     * @throws \LogicException
     */
    public function show(Request $request, string $key): JsonResponse
    {
        try {
            $user = $this->userRepository->find($key);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return response()->json([
                'message' => self::MESSAGE_SOMETHING_WENT_WRONG,
                'status' => self::MESSAGE_FAIL,
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'data' => $user,
            'status' => self::MESSAGE_OK,
        ], JsonResponse::HTTP_OK);
    }

    /**
     * @param RegisterUserPasswordRequest $request
     * @param OAuth2UserManager $OAuth2UserManager
     * @return JsonResponse
     * @throws \LogicException
     */
    public function register(RegisterUserPasswordRequest $request, OAuth2UserManager $OAuth2UserManager): JsonResponse
    {
        try {
            $OAuth2UserManager->createPasswordClient($request->getUserName(), $request->getUserName(), $request->getPassword());
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return response()->json([
                'message' => self::MESSAGE_SOMETHING_WENT_WRONG,
                'status' => self::MESSAGE_FAIL,
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'status' => self::MESSAGE_OK,
        ], JsonResponse::HTTP_OK);
    }
}
