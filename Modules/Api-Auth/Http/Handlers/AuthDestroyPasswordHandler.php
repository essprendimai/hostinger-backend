<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Handlers;

use Essprendimai\ApiAuth\Entities\Auth;
use Laravel\Passport\Client;

/**
 * Class AuthDestroyPasswordHandler
 * @package Essprendimai\ApiAuth\Http\Handlers
 */
class AuthDestroyPasswordHandler
{
    /**
     * @param Auth $user
     * @param int $clientId
     * @throws \Exception
     */
    public function handle(Auth $user, int $clientId): void
    {
        $client = $user->clients->first(function(Client $client) use ($clientId): bool {
            return $client->id == $clientId;
        });

        if ($client) {
            $client->delete();
        }

        if ($user->clients->count() <= 1) {
            $user->delete();
        }
    }
}
