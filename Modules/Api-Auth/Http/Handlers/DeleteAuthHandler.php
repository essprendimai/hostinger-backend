<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Handlers;

use Essprendimai\ApiAuth\Entities\Auth;
use Essprendimai\ApiAuth\Repositories\AuthRepository;

/**
 * Class DeleteAuthHandler
 * @package Essprendimai\ApiAuth\Http\Handlers
 */
class DeleteAuthHandler
{
    /** @var AuthRepository */
    private $authRepository;

    /**
     * DeleteAdvertisingHandler constructor.
     * @param AuthRepository $authRepository
     * @internal param AdvertisingRepository $advertisingRepository
     */
    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    /**
     * @param Auth $auth
     */
    public function handle(Auth $auth): void
    {
        $this->authRepository->delete($auth->id);
    }
}
