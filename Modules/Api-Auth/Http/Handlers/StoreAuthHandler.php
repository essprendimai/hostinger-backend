<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Handlers;

use Essprendimai\ApiAuth\DTO\AuthDTO;
use Essprendimai\ApiAuth\Enums\AuthType;
use Essprendimai\ApiAuth\Repositories\AuthRepository;
use Essprendimai\ApiAuth\Services\AuthService;

/**
 * Class StoreAuthHandler
 * @package Essprendimai\ApiAuth\Http\Handlers
 */
class StoreAuthHandler
{
    /** @var AuthRepository */
    private $authRepository;

    /** @var AuthService */
    private $authService;

    /**
     * CreateAdvertisingHandler constructor.
     * @param AuthRepository $authRepository
     * @param AuthService $authService
     * @internal param AdvertisingRepository $advertisingRepository
     */
    public function __construct(AuthRepository $authRepository, AuthService $authService)
    {
        $this->authRepository = $authRepository;
        $this->authService = $authService;
    }

    /**
     * @param AuthDTO $auth
     */
    public function handle(AuthDTO $auth): void
    {
        $authData = $this->authService->generateTokens();
        $authData->setEmail($auth->getEmail());
        $authData->setType(AuthType::token());

        $this->authRepository->createFromData($authData);
    }
}
