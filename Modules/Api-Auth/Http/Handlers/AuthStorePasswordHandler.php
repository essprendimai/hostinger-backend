<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Handlers;

use Essprendimai\ApiAuth\Entities\Auth;
use Essprendimai\ApiAuth\Http\Requests\AuthPasswordRequest;
use Essprendimai\ApiAuth\Repositories\AuthRepository;
use Essprendimai\ApiAuth\Services\OAuth2UserManager;

/**
 * Class AuthStorePasswordHandler
 * @package Essprendimai\ApiAuth\Http\Handlers
 */
class AuthStorePasswordHandler
{
    /**
     * @var OAuth2UserManager
     */
    private $auth2UserManager;

    /**
     * @var AuthRepository
     */
    private $userRepository;

    /**
     * AuthStorePasswordHandler constructor.
     * @param OAuth2UserManager $auth2UserManager
     * @param AuthRepository $userRepository
     */
    public function __construct(OAuth2UserManager $auth2UserManager, AuthRepository $userRepository)
    {
        $this->auth2UserManager = $auth2UserManager;
        $this->userRepository = $userRepository;
    }

    /**
     * @param AuthPasswordRequest $request
     * @return bool
     */
    public function handle(AuthPasswordRequest $request): bool
    {
        /** @var Auth $user */
        $user = $this->userRepository->where('email', '=', $request->getEmail())->first();

        if ($user) {
            $passwordClient = $user->clients->first(function($item) {
                return $item->password_client == 1;
            });

            if ($passwordClient) {
                return false;
            } else {
                $this->auth2UserManager->createPasswordType($user->id, $request->getName());
            }
        } else {
            $this->auth2UserManager->createPasswordClient($request->getName(), $request->getEmail(), $request->getPassword());
        }

        return true;
    }
}
