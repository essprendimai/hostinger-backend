<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Handlers;

use Essprendimai\ApiAuth\Repositories\AuthRepository;
use Illuminate\Support\Collection;
use RuntimeException;

/**
 * Class GetAuthHandler
 * @package Essprendimai\ApiAuth\Http\Handlers
 */
class GetAuthHandler
{
    /** @var AuthRepository */
    private $authRepository;

    /**
     * GetAdvertisingHandler constructor.
     * @param AuthRepository $authRepository
     * @internal param AdvertisingRepository $advertisingRepository
     */
    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    /**
     * @return Collection
     * @throws RuntimeException
     */
    public function handle(): Collection
    {
        return $this->authRepository->allToDTO();
    }
}
