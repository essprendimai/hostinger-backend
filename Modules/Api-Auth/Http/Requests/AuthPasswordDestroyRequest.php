<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AuthPasswordDestroyRequest
 * @package Essprendimai\ApiAuth\Http\Requests
 */
class AuthPasswordDestroyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'client_id' => 'required|numeric',
        ];
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return (int)$this->input('client_id');
    }
}
