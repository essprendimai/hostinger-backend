<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Requests;

use Essprendimai\ApiAuth\DTO\AuthDTO;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AuthFormRequest
 * @package Essprendimai\ApiAuth\Http\Requests
 */
class AuthFormRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|string|email|unique:auths,email',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return AuthDTO
     */
    public function resolve(): AuthDTO
    {
        return (new AuthDTO())
            ->setEmail((string)$this->input('email'));
    }
}
