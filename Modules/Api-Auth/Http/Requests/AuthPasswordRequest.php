<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AuthPasswordRequest
 * @package Essprendimai\ApiAuth\Http\Requests
 */
class AuthPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:191',
            'email' => 'required|email|max:191',
            'password' => 'required|min:8',
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return (string)$this->input('name');
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return (string)$this->input('email');
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return (string)$this->input('password');
    }
}
