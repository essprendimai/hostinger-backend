<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

/**
 * Class ApiUserStoreRequest
 * @package Essprendimai\ApiAuth\Http\Requests
 */
class ApiUserStoreRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function wantsJson(): bool
    {
        return true;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|max:191|unique:users,email',
            'password' => 'required|string',
        ];
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return (string)$this->input('email');
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return (string)$this->input('password');
    }

    /**
     * @param Validator $validator
     * @throws HttpResponseException
     * @throws \LogicException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'messages' => [
                    'email' => $validator->getMessageBag()->get('email'),
                    'password' => $validator->getMessageBag()->get('password'),
                ],
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
