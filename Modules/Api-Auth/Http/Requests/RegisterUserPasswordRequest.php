<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

/**
 * Class RegisterUserPasswordRequest
 * @package Essprendimai\ApiAuth\Http\Requests
 */
class RegisterUserPasswordRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function wantsJson(): bool
    {
        return true;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'username' => 'required|email|max:191|unique:auths,email',
            'password' => 'required|string',
            'grant_type' => 'required|string',
        ];
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return (string)$this->input('username');
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return (string)$this->input('password');
    }

    /**
     * @return string
     */
    public function getGrantType(): string
    {
        return (string)$this->input('grant_type');
    }

    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'messages' => [
                    'username' => $validator->getMessageBag()->get('username'),
                    'password' => $validator->getMessageBag()->get('password'),
                    'grant_type' => $validator->getMessageBag()->get('grant_type'),
                ],
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
