<?php

return [
    'buttons'                           => [
        'confirm_delete'    => 'Confirm delete?',
        'create'            => 'Create',
        'delete'            => 'Delete',
        'edit'              => 'Edit',
    ],
    'create_api_password'               => 'Create api password',
    'create_token_api'                  => 'Create token api',
    'edit'                              => 'Edit',
    'empty'                             => 'Empty list',
    'fields'                            => [
        'email'     => 'Email',
        'name'      => 'Name',
        'password'  => 'Password',
    ],
    'menu'                              => [
        'api_auth'                  => 'Api Auth',
        'communication_security'    => 'Communication security',
    ],
    'new'                               => 'New',
    'page_api_password_create'          => [
        'title'             => 'Api password',
        'title_description' => 'Create security user which used to authenticate user when getting server data with Headers (Bearer authentication)',
    ],
    'page_api_password_edit'            => [
        'title'             => 'Api :attribute password',
        'title_description' => 'Edit security user which used to authenticate user when getting server data with Headers (Bearer authentication)',
    ],
    'page_api_toke_create'              => [
        'title'             => 'Api token',
        'title_description' => 'Create security user which used to authenticate user when getting server data',
    ],
    'page_communication_security_list'  => [
        'title'             => 'Communication security',
        'title_description' => 'Create security user/key which used to authentikate user when getting server data',
    ],
    'private_not_exists'                => 'Secret key not exists',
    'public_not_exists'                 => 'Public key not exists',
    'table'                             => [
        'actions'   => 'Actions',
        'client_id' => 'Client id',
        'email'     => 'Email',
        'public_key'=> 'Public key',
        'secret'    => 'Secret',
        'type'      => 'Type',
    ],
];
