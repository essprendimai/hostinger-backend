<?php

return [
    'buttons'                           => [
        'confirm_delete'    => 'Kinnita kustutamine?',
        'create'            => 'Loo',
        'delete'            => 'Kustuta',
        'edit'              => 'Muuda',
    ],
    'create_api_password'               => 'Loo api parool',
    'create_token_api'                  => 'Loo märgis api',
    'edit'                              => 'Muuda',
    'empty'                             => 'Tühi nimekiri',
    'fields'                            => [
        'email'     => 'E-post',
        'name'      => 'Nimi',
        'password'  => 'Parool',
    ],
    'menu'                              => [
        'api_auth'                  => 'Api Auth',
        'communication_security'    => 'Side turvalisus',
    ],
    'new'                               => 'Uus',
    'page_api_password_create'          => [
        'title'             => 'Api parool',
        'title_description' => 'Looge kasutaja, kes kasutas serveri andmete autentimiseks päiseid (kandja autentimine)',
    ],
    'page_api_password_edit'            => [
        'title'             => 'Api :attribute parool',
        'title_description' => 'Redigeerige turvaseadet, mis kasutas kasutaja autentimisel serveri andmeid päistega (kandja autentimine)',
    ],
    'page_api_toke_create'              => [
        'title'             => 'Api token',
        'title_description' => 'Looge serveriteabe hankimisel turvalisuse kasutaja, kes kasutas kasutaja autentimist',
    ],
    'page_communication_security_list'  => [
        'title'             => 'Side turvalisus',
        'title_description' => 'Looge turvalisuse kasutaja / võti, mis kasutas kasutaja autentimisel serveri andmeid',
    ],
    'private_not_exists'                => 'Salajane võti pole olemas',
    'public_not_exists'                 => 'Avalik võti pole olemas',
    'table'                             => [
        'actions'   => 'Meetmeid',
        'client_id' => 'Kliendi ID',
        'email'     => 'E-post',
        'public_key'=> 'Avalik võti',
        'secret'    => 'Saladus',
        'type'      => 'Tüüp',
    ],
];
