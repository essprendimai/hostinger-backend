<?php

return [
    'buttons'                           => [
        'confirm_delete'    => 'Tikrai trinti?',
        'create'            => 'Sukurti',
        'delete'            => 'Trinti',
        'edit'              => 'Redaguoti',
    ],
    'create_api_password'               => 'Sukurti slaptažodžio vartotoją',
    'create_token_api'                  => 'Sukurti token vartotoją',
    'edit'                              => 'Redaguoti',
    'empty'                             => 'Sąrašas tuščias',
    'fields'                            => [
        'email'     => 'El. paštas',
        'name'      => 'Vardas',
        'password'  => 'Slaptažodis',
    ],
    'menu'                              => [
        'api_auth'                  => 'Duomenų prieigos vartotojai',
        'communication_security'    => 'Apsaugos priemonės',
    ],
    'new'                               => 'Naujas',
    'page_api_password_create'          => [
        'title'             => 'Slaptažodžio vartotojas',
        'title_description' => 'Sukurti apsaugos vartotoją, kuris apsaugo duomenų gavimą per Headerius (Bearer)',
    ],
    'page_api_password_edit'            => [
        'title'             => 'Redaguoti :attribute slaptažodžio vartotoją',
        'title_description' => 'Redaguoti apsaugos vartotoją, kuris apsaugo duomenų gavimą per Headerius (Bearer)',
    ],
    'page_api_toke_create'              => [
        'title'             => 'Token vartotojas',
        'title_description' => 'Sukurti apsaugos vartotoją, kuris apsaugo duomenų gavimą',
    ],
    'page_communication_security_list'  => [
        'title'             => 'Apsaugos priemonės',
        'title_description' => 'Forma, kuri skirta sukurti apsaugos vartotojus, kurie naudojami gaunant sistemos duomenis per nuorodą. Kreipiantis į serverį būtina authentikuoti vartotoją su pateiktais duomenimis',
    ],
    'private_not_exists'                => 'Privatus raktas neegzistuoja',
    'public_not_exists'                 => 'Viešas raktas neegzistuoja',
    'table'                             => [
        'actions'   => 'Veiksmai',
        'client_id' => 'Kliento id',
        'email'     => 'El. paštas',
        'public_key'=> 'Viešas raktas',
        'secret'    => 'Privatus raktas',
        'type'      => 'Tipas',
    ],
];
