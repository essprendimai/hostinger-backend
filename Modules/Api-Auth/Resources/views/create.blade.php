@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="icon fa fa-plus"></i> {{ __('api-auth::api-auth.page_api_toke_create.title') }}&nbsp;&nbsp;&nbsp;
    </h1>
    <p>{{ __('api-auth::api-auth.page_api_toke_create.title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.apiauth.index') }}">{{ __('api-auth::api-auth.menu.communication_security') }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.apiauth.create') }}">{{ __('api-auth::api-auth.new') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <form class="form-horizontal" method="POST" action="{{ route('backend.apiauth.store') }}">
                    {{ csrf_field() }}

                    <div class="panel-body">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="title" class="control-label">{{ __('api-auth::api-auth.fields.email') }}</label>

                            <input id="title" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   required
                                   autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="panel-footer text-right">
                        <button type="submit" class="btn btn-primary">
                            {{ __('api-auth::api-auth.buttons.create') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
