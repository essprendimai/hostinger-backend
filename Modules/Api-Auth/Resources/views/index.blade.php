@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="icon fa fa-shield"></i> {{ __('api-auth::api-auth.page_communication_security_list.title') }}&nbsp;&nbsp;&nbsp;
        <a class="btn btn-primary" href="{{ route('backend.apiauth.create') }}">
            <i class="fa fa-plus"></i> {{ __('api-auth::api-auth.create_token_api') }}
        </a>
        <a class="btn btn-primary" href="{{ route('backend.apiauthpassword.create') }}">
            <i class="fa fa-plus"></i> {{ __('api-auth::api-auth.create_api_password') }}
        </a>
    </h1>
    <p>{{ __('api-auth::api-auth.page_communication_security_list.title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.apiauth.index') }}">{{ __('api-auth::api-auth.menu.communication_security') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>{{ __('api-auth::api-auth.table.email') }}</th>
                        <th>{{ __('api-auth::api-auth.table.public_key') }} / {{ __('api-auth::api-auth.table.secret') }}</th>
                        <th>{{ __('api-auth::api-auth.table.client_id') }}</th>
                        <th>{{ __('api-auth::api-auth.table.type') }}</th>
                        <th>{{ __('api-auth::api-auth.table.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($auths as $auth)
                        <tr>
                            <td>{{ $auth->getEmail() }}</td>
                            <td>
                                {!! $auth->getPublicToken() ? '<b class="text-info">Public: </b>' .  $auth->getPublicToken() : '<b class="text-danger">' . __('api-auth::api-auth.public_not_exists') . '</b>' !!}
                                <br/>
                                <hr/>
                                {!!  $auth->getType()->id() === \Essprendimai\ApiAuth\Enums\AuthType::Password()->id() ? '<b class="text-info">Secret: </b>' . $auth->getClients()->first()->secret : '<b class="text-danger">' . __('api-auth::api-auth.private_not_exists') . '</b>' !!}
                            </td>
                            <td>{{ $auth->getType()->id() === \Essprendimai\ApiAuth\Enums\AuthType::Password()->id() ? $auth->getClients()->first()->id : '' }}</td>
                            <td>{{ $auth->getType()->name() }}</td>
                            <td class="text-center">
                                @if($auth->getType()->id() === \Essprendimai\ApiAuth\Enums\AuthType::Password()->id())
                                    <a class="btn btn-sm btn-info" href="{{ route('backend.apiauthpassword.edit', $auth->getId()) }}">
                                        <i class="fas fa-edit"></i> {{ __('api-auth::api-auth.buttons.edit') }}
                                    </a>

                                    {!! BootForm::open(['route' => ['backend.apiauthpassword.destroy', $auth->getId()], 'method'=> 'DELETE']) !!}
                                    {!! BootForm::hidden('client_id', $auth->getClients()->first()->id) !!}
                                    {!! Form::button(
                                     '<i class="fa fa-trash"></i> ' . __('api-auth::api-auth.buttons.delete'),
                                     [
                                         'type' => 'submit',
                                         'class' => 'btn-danger btn btn-sm',
                                         'title' => __('basic::pages.pages_page.delete'),
                                         'onclick'=> 'return confirm("' . __('api-auth::api-auth.buttons.confirm_delete') . '")'
                                     ]) !!}
                                    {!! BootForm::close() !!}
                                @else
                                    {!! BootForm::open(['route' => ['backend.apiauth.destroy', $auth->getId()], 'method'=> 'DELETE']) !!}
                                    {!! Form::button(
                                     '<i class="fa fa-trash"></i> ' . __('api-auth::api-auth.buttons.delete'),
                                     [
                                         'type' => 'submit',
                                         'class' => 'btn-danger btn btn-sm',
                                         'title' => __('basic::pages.pages_page.delete'),
                                         'onclick'=> 'return confirm("' . __('api-auth::api-auth.buttons.confirm_delete') . '")'
                                     ]) !!}
                                    {!! BootForm::close() !!}
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">
                                <p class="text-center bg-warning">{{ __('api-auth::api-auth.empty') }}</p>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
