@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="icon fas fa-edit"></i> {{ __('api-auth::api-auth.page_api_password_edit.title') }}&nbsp;&nbsp;&nbsp;
    </h1>
    <p>{{ __('api-auth::api-auth.page_api_password_edit.title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.apiauth.index') }}">{{ __('api-auth::api-auth.menu.communication_security') }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.apiauthpassword.create') }}">{{ __('api-auth::api-auth.new') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                {!! BootForm::open(['route' => 'backend.apiauthpassword.store', 'method' => 'POST']) !!}
                    {!! BootForm::text('name', __('api-auth::api-auth.fields.name')) !!}
                    {!! BootForm::email('email', __('api-auth::api-auth.fields.email')) !!}
                    {!! BootForm::password('password', __('api-auth::api-auth.fields.password')) !!}
                    {!! BootForm::submit(__('api-auth::api-auth.buttons.create')) !!}
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@stop
