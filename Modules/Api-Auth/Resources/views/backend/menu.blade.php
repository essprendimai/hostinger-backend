@if (can_access_any([
    'backend.apiauth.index',
]))
    <li class="treeview">
        <a class="app-menu__item {{ active('backend.apiauth.*') }}" href="#" data-toggle="treeview">
            <i class="app-menu__icon fa fa-user-secret"></i>
            <span class="app-menu__label">{{ __('api-auth::api-auth.menu.api_auth') }}</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
            @if (can_access('backend.apiauth.index'))
                <li>
                    <a class="treeview-item {{ active('backend.apiauth.index') }}" href="{{ route('backend.apiauth.index') }}">
                        <i class="icon fa fa-shield"></i> {{ __('api-auth::api-auth.menu.communication_security') }}
                    </a>
                </li>
            @endif
        </ul>
    </li>
@endif