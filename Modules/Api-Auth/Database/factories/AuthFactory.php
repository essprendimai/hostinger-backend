<?php

declare(strict_types=1);

use Essprendimai\ApiAuth\Entities\Auth;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Auth::class, function (Faker\Generator $faker) {
    return [
        'id' => $faker->unique()->randomNumber(5),
        'email' => $faker->email,
        'public_token' => $faker->sha256,
        'private_token' => $faker->sha256,
        'token' => $faker->sha256,
    ];
});
