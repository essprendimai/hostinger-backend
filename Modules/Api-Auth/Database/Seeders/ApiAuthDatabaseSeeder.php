<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use InvalidArgumentException;

/**
 * Class ApiAuthDatabaseSeeder
 * @package Essprendimai\ApiAuth\Database\Seeders
 */
class ApiAuthDatabaseSeeder extends Seeder
{
    /**
     * @throws InvalidArgumentException
     */
    public function run()
    {
        Model::unguard();

        $this->call(AuthSeeder::class);
    }
}
