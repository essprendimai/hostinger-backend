<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Database\Seeders;

use Essprendimai\ApiAuth\DTO\AuthDTO;
use Essprendimai\ApiAuth\Enums\AuthType;
use Essprendimai\ApiAuth\Repositories\AuthRepository;
use Essprendimai\ApiAuth\Services\AuthService;
use Essprendimai\ApiAuth\Services\OAuth2UserManager;
use Illuminate\Database\Seeder;

/**
 * Class AuthSeeder
 * @package Essprendimai\ApiAuth\Database\Seeders
 */
class AuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /** @var AuthService $service */
        $service = app(AuthService::class);

        /** @var AuthRepository $repository */
        $repository = app(AuthRepository::class);

        $auth = $service->generateTokens('O5h99fd4GPNIgjlkknkb552sd498');

        $repository->createFromData((new AuthDTO())
            ->setEmail(env('ADMIN_USER_EMAIL'))
            ->setPublicToken('O5h99fd4GPNIgjlkknkb552sd498')
            ->setPrivateToken($auth->getPrivateToken())
            ->setToken($auth->getToken())
            ->setType(AuthType::token())
        );

        /** @var OAuth2UserManager $apiManager */
        $apiManager = app(OAuth2UserManager::class);
        $client = $apiManager->createPasswordClient(env('ADMIN_USER_EMAIL'), env('API_EMAIL'), env('API_PASSWORD'));
        $client->secret = 'pks7dyJN8JVn2XXjX1vDoHhwwYGDYyHHaXwHCRcX';
        $client->save();
    }
}
