<?php

declare(strict_types = 1);

use Essprendimai\ApiAuth\Enums\AuthType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateAuthTable
 */
class CreateAuthTable extends Migration
{
    /**
     * @throws InvalidArgumentException
     * @throws RuntimeException
     * @throws Exception
     */
    public function up(): void
    {
        Schema::create('auths', function(Blueprint $table) {
            $table->increments('id');

            $table->string('email');
            $table->string('public_token')->nullable();
            $table->string('private_token')->nullable();
            $table->string('token')->nullable();
            $table->string('password')->nullable();
            $table->string('name')->nullable();
            $table->enum('type', AuthType::enum());
        });
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function down(): void
    {
        Schema::dropIfExists('auths');
    }
}
