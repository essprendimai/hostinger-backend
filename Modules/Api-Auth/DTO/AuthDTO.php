<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\DTO;

use Essprendimai\Basic\Enums\Enumerable;

/**
 * Class AuthDTO
 * @package Essprendimai\ApiAuth\DTO
 */
class AuthDTO
{
    /** @var int */
    private $id = 0;

    /** @var string */
    private $email = '';

    /** @var string */
    private $public_token = '';

    /** @var string */
    private $private_token = '';

    /** @var string */
    private $token = '';

    /** @var Enumerable */
    private $type;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $password = '';

    private $clients;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return AuthDTO
     */
    public function setId(int $id): AuthDTO
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return AuthDTO
     */
    public function setEmail(string $email): AuthDTO
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPublicToken(): string
    {
        return $this->public_token;
    }

    /**
     * @param string $public_token
     * @return AuthDTO
     */
    public function setPublicToken(string $public_token): AuthDTO
    {
        $this->public_token = $public_token;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrivateToken(): string
    {
        return $this->private_token;
    }

    /**
     * @param string $private_token
     * @return AuthDTO
     */
    public function setPrivateToken(string $private_token): AuthDTO
    {
        $this->private_token = $private_token;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return AuthDTO
     */
    public function setToken(string $token): AuthDTO
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return Enumerable
     */
    public function getType(): Enumerable
    {
        return $this->type;
    }

    /**
     * @param Enumerable $type
     * @return AuthDTO
     */
    public function setType(Enumerable $type): AuthDTO
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return AuthDTO
     */
    public function setName(string $name): AuthDTO
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return AuthDTO
     */
    public function setPassword(string $password): AuthDTO
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * @param $clients
     * @return AuthDTO
     */
    public function setClients($clients): AuthDTO
    {
        $this->clients = $clients;

        return $this;
    }
}
