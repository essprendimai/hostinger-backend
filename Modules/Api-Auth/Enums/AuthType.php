<?php
declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Enums;

use Essprendimai\Basic\Enums\Enumerable;

/**
 * Class AuthType
 * @package Essprendimai\ApiAuth\Enums
 */
class AuthType extends Enumerable
{
    /**
     * @return AuthType
     */
    final public static function token(): AuthType
    {
        return self::make('token', 'Token');
    }

    /**
     * @return AuthType
     */
    final public static function Password(): AuthType
    {
        return self::make('password', 'Password');
    }
}
