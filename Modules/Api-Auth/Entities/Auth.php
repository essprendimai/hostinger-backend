<?php

declare(strict_types = 1);

namespace Essprendimai\ApiAuth\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * Class Auth
 * @package Essprendimai\ApiAuth\Entities
 */
class Auth extends Model
{
    use HasApiTokens, Notifiable, Authenticatable, Authorizable;

    /** @var bool */
    public $timestamps = false;

    /** @var array */
    protected $fillable = [
        'name',
        'email',
        'public_token',
        'private_token',
        'token',
        'type',
        'password',
    ];
}
