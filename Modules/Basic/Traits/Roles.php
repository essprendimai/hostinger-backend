<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Traits;

use Essprendimai\Basic\Entities\Role;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Trait Roles
 * @package Essprendimai\Basic\Traits
 *
 * @method BelongsToMany belongsToMany($related, $table = null, $foreignKey = null, $relatedKey = null, $relation = null)
 */
trait Roles
{
    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }
}
