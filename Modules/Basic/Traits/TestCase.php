<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Traits;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

/**
 * Class TestCase
 * @package Essprendimai\Basic\Traits
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
