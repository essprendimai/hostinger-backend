<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Traits;

use Illuminate\Contracts\Console\Kernel;

/**
 * Trait CreatesApplication
 * @package Essprendimai\Basic\Traits
 */
trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../../../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }
}
