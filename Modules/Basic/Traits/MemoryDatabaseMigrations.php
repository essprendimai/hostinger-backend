<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Console\Kernel;

/**
 * Trait MemoryDatabaseMigrations
 * @package Essprendimai\Basic\Traits
 */
trait MemoryDatabaseMigrations
{
    /**
     * Define hooks to migrate the database before and after each test.
     *
     * @before
     * @return void
     */
    public function runDatabaseMigrations()
    {
        $this->afterApplicationCreated(function() {
            $this->artisan('migrate');

            $this->app[Kernel::class]->setArtisan(null);

            $this->beforeApplicationDestroyed(function() {
                if (DB::connection()->getDatabaseName() != ':memory:') {
                    $this->artisan('migrate:rollback');
                }
            });
        });
    }
}
