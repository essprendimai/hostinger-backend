<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Traits;

use Essprendimai\Basic\Entities\User;

/**
 * Trait AuthenticateAs
 * @package Essprendimai\Basic\Traits
 */
trait AuthenticateAs
{
    /**
     * @param User $user
     * @param bool $remember
     */
    public function authenticateAs(User $user, bool $remember = false): void
    {
        auth()->login($user, $remember);
    }
}
