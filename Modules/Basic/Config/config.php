<?php

declare(strict_types = 1);

return [
    'name' => 'Basic',
    'active_languages' => explode(',', (string)env('ACTIVE_LANGUAGES'))
];
