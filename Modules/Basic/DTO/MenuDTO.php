<?php

declare(strict_types=1);

namespace Essprendimai\Basic\DTO;

use Essprendimai\Basic\Enums\LanguageEnum;
use Essprendimai\Basic\Repositories\CategoryRepository;

/**
 * Class MenuDTO
 * @package Essprendimai\Basic\DTO
 */
class MenuDTO
{
    /**
     * @var string
     */
    private $pageUrl = '';

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $content = '';

    /**
     * @var bool
     */
    private $isExternalLink = false;

    /**
     * @var string|null
     */
    private $categoryUrl;

    /**
     * @var LanguageEnum|null
     */
    private $language;

    /**
     * @var string
     */
    private $featuredImage = '';

    /**
     * @var string
     */
    private $menuType = CategoryRepository::FOOTER;

    public function __construct()
    {
        $this->language = LanguageEnum::en();
    }

    /**
     * @return string
     */
    public function getPageUrl(): string
    {
        return $this->pageUrl;
    }

    /**
     * @param string $pageUrl
     * @return MenuDTO
     */
    public function setPageUrl(string $pageUrl): MenuDTO
    {
        $this->pageUrl = $pageUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return MenuDTO
     */
    public function setTitle(string $title): MenuDTO
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return bool
     */
    public function isExternalLink(): bool
    {
        return $this->isExternalLink;
    }

    /**
     * @param bool $isExternalLink
     * @return MenuDTO
     */
    public function setIsExternalLink(bool $isExternalLink): MenuDTO
    {
        $this->isExternalLink = $isExternalLink;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategoryUrl(): ?string
    {
        return $this->categoryUrl;
    }

    /**
     * @param string|null $categoryUrl
     * @return MenuDTO
     */
    public function setCategoryUrl(?string $categoryUrl): MenuDTO
    {
        $this->categoryUrl = $categoryUrl;

        return $this;
    }

    /**
     * @return LanguageEnum|null
     */
    public function getLanguage(): ?LanguageEnum
    {
        return $this->language;
    }

    /**
     * @param LanguageEnum|null $language
     * @return MenuDTO
     */
    public function setLanguage(?LanguageEnum $language): MenuDTO
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return string
     */
    public function getMenuType(): string
    {
        return $this->menuType;
    }

    /**
     * @param string $menuType
     * @return MenuDTO
     */
    public function setMenuType(string $menuType): MenuDTO
    {
        $this->menuType = $menuType;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return MenuDTO
     */
    public function setContent(string $content): MenuDTO
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getFeaturedImage(): string
    {
        return $this->featuredImage;
    }

    /**
     * @param string $featuredImage
     * @return MenuDTO
     */
    public function setFeaturedImage(string $featuredImage): MenuDTO
    {
        $this->featuredImage = $featuredImage;

        return $this;
    }
}
