<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Services;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Facades\DB;
use Essprendimai\Basic\Enums\GroupsRelationsTypesEnum;
use Essprendimai\Basic\Enums\LanguageEnum;
use Essprendimai\Basic\Enums\PageGroupsTypesEnum;
use Essprendimai\Basic\Exceptions\PageTransactionException;
use Essprendimai\Basic\Http\Requests\PagesFormRequest;
use Essprendimai\Basic\Repositories\GroupRelationRepository;
use Essprendimai\Basic\Repositories\PageRepository;
use Throwable;

/**
 * Class PageTransactionService
 * @package Essprendimai\Basic\Services
 */
class PageTransactionService
{
    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * @var GroupRelationRepository
     */
    private $groupRelationRepository;

    /**
     * @var PageFileService
     */
    private $pageFileService;

    /**
     * PageTransactionService constructor.
     * @param PageRepository $pageRepository
     * @param GroupRelationRepository $groupRelationRepository
     * @param PageFileService $pageFileService
     */
    public function __construct(
        PageRepository $pageRepository,
        GroupRelationRepository $groupRelationRepository,
        PageFileService $pageFileService
    ) {
        $this->pageRepository = $pageRepository;
        $this->groupRelationRepository = $groupRelationRepository;
        $this->pageFileService = $pageFileService;
    }

    /**
     * @param PagesFormRequest $request
     * @throws Throwable
     */
    public function storePages(PagesFormRequest $request): void
    {
        DB::beginTransaction();

        try {
            foreach (LanguageService::getAllActiveLanguages() as $language) {
                $groupType = $this->getGroupType($request, $language);

                $this->storePage($request, $groupType, $language->id());
            }
        } catch (Throwable $throwable) {
            logger()->error($throwable, $throwable->getTrace());

            DB::rollback();

            throw PageTransactionException::failStorePage();
        }

        DB::commit();
    }

    /**
     * @param string $key
     * @throws PageTransactionException
     */
    public function destroyPagesByKey(string $key): void
    {
        DB::beginTransaction();

        try {
            $pages = $this->destroyPageRelationshipByPageKey($key);
            $deleteFeaturedImages = $pages->pluck('featured_image')->toArray();

            $this->pageRepository->deleteWhere([
                'key' => $key,
            ]);

            foreach ($deleteFeaturedImages as $deleteFeaturedImage) {
                if ($deleteFeaturedImage) {
                    $this->pageFileService->delete($deleteFeaturedImage);
                }
            }
        } catch (Throwable $throwable) {
            logger()->error($throwable, $throwable->getTrace());

            DB::rollback();

            throw PageTransactionException::failDestroyPage();
        }

        DB::commit();
    }

    /**
     * @param PagesFormRequest $request
     * @param string $key
     * @throws PageTransactionException
     */
    public function updatePages(PagesFormRequest $request, string $key)
    {
        DB::beginTransaction();

        try {
            $this->destroyPageRelationshipByPageKey($key);

            foreach (LanguageService::getAllActiveLanguages() as $language) {
                $page = $this->pageRepository->findWithKey($key, $language->id());
                $groupType = $this->getGroupType($request, $language);

                if (!$page) {
                    $this->storePage($request, $groupType, $language->id());
                } else {
                    $this->updatePage($request, $page, $language, $groupType);
                }
            }
        } catch (Throwable $throwable) {
            logger()->error($throwable, $throwable->getTrace());

            DB::rollback();

            throw PageTransactionException::failStorePage();
        }

        DB::commit();
    }

    /**
     * @param PagesFormRequest $request
     * @param string $groupType
     * @param string $language
     * @throws \ReflectionException
     */
    private function storePage(PagesFormRequest $request, string $groupType, string $language)
    {
        $imageUrl = null;
        if ($featuredImage = $request->getFeaturedImageFile($language)) {
            $imageUrl = $this->pageFileService->store($featuredImage);
        }

        $page = $this->pageRepository->create([
            'key' => $request->getKey(),
            'language' => $language,
            'title' => $request->getTitle($language),
            'url' => $request->getUrl($language),
            'text' => $request->getText($language),
            'category_id' => $request->getCategory($language),
            'active' => $request->getActive($language),
            'external_url' => $request->getExternalUrl($language),
            'type' => $groupType,
            'featured_image' => $imageUrl,
            'is_template' => $request->isTemplate($language),
        ]);

        if ($groupType === PageGroupsTypesEnum::menu()->id()) {
            $this->storePageRelationWithGroup($request->getGroupIds($language), $page->id);
        }
    }

    /**
     * @param array $groupIds
     * @param int $pageId
     * @throws \ReflectionException
     */
    private function storePageRelationWithGroup(array $groupIds, int $pageId)
    {
        $this->groupRelationRepository->createRelations($groupIds, $pageId, GroupsRelationsTypesEnum::page()->id());
    }

    /**
     * @param PagesFormRequest $request
     * @param LanguageEnum $language
     * @return string
     */
    private function getGroupType(PagesFormRequest $request, LanguageEnum $language): string
    {
        return $request->getGroupIds($language->id()) ? PageGroupsTypesEnum::menu()->id() : PageGroupsTypesEnum::none()->id();
    }

    /**
     * @param string $key
     * @return EloquentCollection
     * @throws \ReflectionException
     */
    private function destroyPageRelationshipByPageKey(string $key): EloquentCollection
    {
        $pages = $this->pageRepository->findAllBy(['key' => $key]);
        $keys = $pages->pluck('id')->toArray();

        $this->groupRelationRepository->deleteAllRelations($keys, GroupsRelationsTypesEnum::page()->id());

        return $pages;
    }

    /**
     * @param PagesFormRequest $request
     * @param \Essprendimai\Basic\Entities\Page|null $page
     * @param LanguageEnum $language
     * @param string $groupType
     * @throws \ReflectionException
     */
    private function updatePage(
        PagesFormRequest $request,
        ?\Essprendimai\Basic\Entities\Page $page,
        LanguageEnum $language,
        string $groupType
    ): void {
        $deleteFeaturedImage = $page->featured_image;

        $imageUrl = null;
        if ($featuredImage = $request->getFeaturedImageFile($language->id())) {
            $imageUrl = $this->pageFileService->store($featuredImage);
        }

        $this->pageRepository->update([
            'key' => $request->getKey(),
            'language' => $language->id(),
            'title' => $request->getTitle($language->id()),
            'url' => $request->getUrl($language->id()),
            'text' => $request->getText($language->id()),
            'category_id' => $request->getCategory($language->id()),
            'active' => $request->getActive($language->id()),
            'external_url' => $request->getExternalUrl($language->id()),
            'type' => $groupType,
            'featured_image' => $imageUrl ? $imageUrl : $deleteFeaturedImage,
            'is_template' => $request->isTemplate($language->id()),
        ], $page->id);

        if ($groupType === PageGroupsTypesEnum::menu()->id()) {
            $this->storePageRelationWithGroup($request->getGroupIds($language->id()), $page->id);
        }

        if ($imageUrl && $deleteFeaturedImage) {
            $this->pageFileService->delete($deleteFeaturedImage);
        }
    }
}
