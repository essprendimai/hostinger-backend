<?php

declare(strict_types = 1);

namespace Modules\Basic\Services;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Support\Facades\Mail;
use Essprendimai\Basic\Emails\SendMessageToUsersMail;
use Essprendimai\Basic\Emails\VerifyDraftUserEmail;
use Essprendimai\Basic\Entities\User;
use Essprendimai\Basic\Notifications\ResetPasswordQueued;
use Essprendimai\Basic\Notifications\VerifyEmailQueued;
use Essprendimai\Basic\Services\LanguageService;

/**
 * Class UserMailSenderService
 * @package Modules\Basic\Services
 */
class UserMailSenderService
{
    /**
     * @param array $emails
     * @param string $message
     */
    public function sendMessageToUsers(array $emails, string $message): void
    {
        Mail::to($emails)
            ->locale(LanguageService::getActiveLanguage()->id())
            ->send(new SendMessageToUsersMail($message));
    }

    /**
     * @param User $user
     */
    public function sendVerifyDraftUser(User $user): void
    {
        Mail::to($user->email)
            ->locale(LanguageService::getActiveLanguage()->id())
            ->send(new VerifyDraftUserEmail($user->id, $this->setUserRandomPassword($user)));
    }

    /**
     * @return VerifyEmail
     */
    public function getVerifyEmailNotify(): VerifyEmail
    {
        return (new VerifyEmailQueued())->locale(LanguageService::getActiveLanguage()->id());
    }

    /**
     * @param string $token
     * @return ResetPasswordQueued
     */
    public function getResetPasswordNotify(string $token)
    {
        return (new ResetPasswordQueued($token))->locale(LanguageService::getActiveLanguage()->id());
    }

    /**
     * @param User $user
     * @return string
     */
    protected function setUserRandomPassword(User $user): string
    {
        $password = str_random(10);
        $user->update([
            'password' => bcrypt($password),
        ]);

        return $password;
    }
}
