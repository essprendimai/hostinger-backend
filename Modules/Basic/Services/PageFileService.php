<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Services;

use Illuminate\Http\UploadedFile;
use Storage;

/**
 * Class PageFileService
 * @package Essprendimai\Basic\Services
 */
class PageFileService
{
    const DISK_PAGE_IMAGES = 'page-images';

    /**
     * @param string $image
     */
    public function delete(string $image): void
    {
        Storage::disk(self::DISK_PAGE_IMAGES)->delete(basename($image));
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function store(UploadedFile $file): string
    {
        $file->store(self::getPublicPath());

        return Storage::disk(self::DISK_PAGE_IMAGES)->url($file->hashName());
    }

    /**
     * @return string
     */
    public static function getRootPath(): string
    {
        return storage_path('app/' . self::getPublicPath());
    }

    /**
     * @return string
     */
    public static function getPublicPath(): string
    {
        return 'public/' . self::DISK_PAGE_IMAGES;
    }

    /**
     * @return string
     */
    public static function getUrlPath(): string
    {
        return env('APP_URL') . '/storage/' . self::DISK_PAGE_IMAGES;
    }
}
