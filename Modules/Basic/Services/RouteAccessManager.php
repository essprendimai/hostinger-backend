<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Services;

use Essprendimai\Basic\Entities\Role;
use Essprendimai\Basic\Http\Middleware\LimitsBackendRouteAccess;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Routing\Route as RoutingRoute;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

/**
 * Class RouteAccessManager
 * @package Essprendimai\Basic\Services
 */
class RouteAccessManager
{
    const BACKEND_ROUTE_NAME_PREFIX = 'backend.';
    const CACHE_FOR_MINUTES = 1440; // 24 hours
    const ROUTES_CACHE_TAG = 'routes-cache';
    const ROUTES_CACHE_KEY = 'backend-routes';
    const ROUTE_ACCESS_TAG_PREFIX = 'access-to-';
    const ROLE_USER_TAG_PREFIX = 'role-user-';
    const FULL_ACCESS_FIELD = 'full_access';

    /**
     * @return array
     */
    public function getBackendRoutes(): array
    {
        return Cache::remember(self::ROUTES_CACHE_KEY, self::CACHE_FOR_MINUTES, function() {
            /** @var Collection|RoutingRoute[] $routes */
            $routes = collect(Route::getRoutes()->getRoutes());

            return $routes->filter(function(RoutingRoute $route) {
                if ($route->getName()) {
                    return $route;
                }
            })->map(function(RoutingRoute $route) {
                return $route->getName();
            })->toArray();
        });
    }

    /**
     * @param Authenticatable|Role $user
     * @param string $route
     * @return bool
     */
    public function accessAllowed(Authenticatable $user, string $route): bool
    {
        return Cache::tags([
            $this->buildUserTag($user),
            self::ROUTES_CACHE_TAG,
        ])->remember($this->buildRouteAccessTag($route),
            self::CACHE_FOR_MINUTES, function() use ($user, $route) {
                /** @var Collection|Role[] $roles */
                $roles = $user->roles()->get();

                if ($roles->contains(self::FULL_ACCESS_FIELD, true)) {
                    return true;
                }

                return $roles->flatMap(function(Role $role) {
                    return $role->accessible_routes;
                })->contains($route);
            });
    }

    /**
     * @param Authenticatable|Role $user
     * @param int[] $rolesIds
     */
    public function syncRoles(Authenticatable $user, array $rolesIds): void
    {
        $availableRoles = Role::pluck('id');

        $user->roles()->sync(array_filter($rolesIds, function(string $role) use ($availableRoles) {
            return $availableRoles->contains($role);
        }));

        $this->flushUserCache($user);
    }

    /**
     * @param Authenticatable $user
     */
    public function flushUserCache(Authenticatable $user): void
    {
        Cache::tags($this->buildUserTag($user))->flush();
    }

    /**
     * Flush all route access cache
     */
    public function flushCache(): void
    {
        Cache::forget(self::ROUTES_CACHE_KEY);
        Cache::tags(self::ROUTES_CACHE_TAG)->flush();
    }

    /**
     * @param string $route
     * @return string
     */
    private function buildRouteAccessTag(string $route): string
    {
        return self::ROUTE_ACCESS_TAG_PREFIX . $route;
    }

    /**
     * @param Authenticatable $user
     * @return string
     */
    private function buildUserTag(Authenticatable $user): string
    {
        return self::ROLE_USER_TAG_PREFIX . $user->id;
    }
}
