<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Services;

use Blade;
use Essprendimai\Basic\Entities\Page;
use Essprendimai\Basic\Repositories\PageRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;
use InvalidArgumentException;
use RuntimeException;

/**
 * Class PageService
 * @package Essprendimai\Basic\Services
 */
class PageService
{
    const PAGE_KEY = 'page_key';

    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * @var string
     */
    private $layout = '';

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var string|null
     */
    private $content;

    /**
     * @var Page|null
     */
    private $page;

    /**
     * PageService constructor.
     * @param PageRepository $pageRepository
     */
    public function __construct(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param string|null $url
     * @param string|null $categoryUrl
     * @return View
     * @throws \Exception
     */
    public function buildPage(string $url = null, string $categoryUrl = null): View
    {
        if (!$this->title && !$this->content) {
            $this->buildPageFromUrl($url, $categoryUrl);
        }

        $view = view($this->layout, [
            'content' => $this->content,
            'title' => $this->title,
            'page' => $this->page,
        ]);
        $view->getFactory()->inject('title', $this->title);

        return $view;
    }

    /**
     * @param Request|null $request
     * @return RedirectResponse
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public static function switchPageContentByPageUrlAndLanguage(?Request $request): RedirectResponse
    {
        $language = App::getLocale();

        /** @var PageRepository $pageRepository */
        $pageRepository = app(PageRepository::class);
        if ($page = $pageRepository->findWithKey(self::getSessionPageKey(), $language)) {
            if ($page->exists && $page->active) {
                return redirect(get_page_link($page));
            }
        }

        return redirect()->back();
    }

    /**
     * @param string $layout
     * @return PageService
     */
    public function setLayout(string $layout): self
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * @param string $title
     * @return PageService
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param string $content
     * @return PageService
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    private static function getSessionPageKey(): string
    {
        return (string)session(self::PAGE_KEY);
    }

    /**
     * @return PageRepository
     */
    public function getPageRepository(): PageRepository
    {
        return $this->pageRepository;
    }

    /**
     * @param PageRepository $pageRepository
     */
    public function setPageRepository(PageRepository $pageRepository): void
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @return Page
     */
    public function getPage(): ?Page
    {
        return $this->page;
    }

    /**
     * @param Page $page
     */
    public function setPage(?Page $page): void
    {
        $this->page = $page;
    }

    /**
     * @param $value
     * @param array $args
     * @return false|string
     * @throws \Exception
     */
    public static function bladeCompile($value, array $args = array())
    {
        $generated = Blade::compileString($value);

        $args['__env'] =  app(\Illuminate\View\Factory::class);
        ob_start() and extract($args, EXTR_SKIP);

        // We'll include the view contents for parsing within a catcher
        // so we can avoid any WSOD errors. If an exception occurs we
        // will throw it out to the exception handler.
        try
        {
            eval('?>' . html_entity_decode(urldecode(htmlspecialchars_decode($generated, ENT_QUOTES))));
        }

            // If we caught an exception, we'll silently flush the output
            // buffer so that no partially rendered views get thrown out
            // to the client and confuse the user with junk.
        catch (\Exception $e)
        {
            ob_get_clean(); throw $e;
        }

        $content = ob_get_clean();

        return $content;
    }

    /**
     * @param string $url
     * @param string|null $categoryUrl
     * @throws \Exception
     */
    private function buildPageFromUrl(string $url, string $categoryUrl = null): void
    {
        $page = null;
        if ($url && !$categoryUrl) {
            $page = $this->pageRepository->findOneByUrl($url);
        } elseif ($url && $categoryUrl) {
            $page = $this->pageRepository->findOneByPageUrlAndCategoryUrl($url, $categoryUrl);
        }

        if ($page && $page->exists && $page->active) {
            session([
                self::PAGE_KEY => $page->key,
            ]);

            $this->changeLanguageIfPageChangedFromUrl($page);

            $this->setTitle($page->title ?? '');

            try {
                $this->setContent(self::bladeCompile($page->text ?? ''));
            } catch (\Throwable $throwable) {
                $this->setContent($page->text ?? '');
            }

            $this->setPage($page ?? null);
        } else {
            session([
                self::PAGE_KEY => null,
            ]);

            abort(404);
        }
    }

    /**
     * @param Page $page
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    private function changeLanguageIfPageChangedFromUrl(Page $page): void
    {
        if ($page->language !== App::getLocale()) {
            LanguageService::change(null, $page->language);
        }
    }
}
