<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Services;

use Essprendimai\Basic\Enums\Enumerable;
use Essprendimai\Basic\Enums\LanguageEnum;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

/**
 * Class LanguageService
 * @package Essprendimai\Basic\Services
 */
class LanguageService
{
    const SESSION_LANGUAGE = 'language';

    /**
     * @return array
     */
    public static function getActiveLanguagesFromConfig(): array
    {
        $activeLanguages = config('basic.active_languages');
        if (sizeof($activeLanguages) === 0) {
            $languages = explode(',', env('ACTIVE_LANGUAGES'));
        } else {
            $languages = $activeLanguages;
        }

        return $languages;
    }

    /**
     * @return array|LanguageEnum[]
     * @throws \Exception
     */
    public static function getAllActiveLanguages(): array
    {
        $languages = [];
        foreach (self::getActiveLanguagesFromConfig() as $activeLanguage) {
            try {
                $languages[] = LanguageEnum::from($activeLanguage);
            } catch (\Exception $exception) {
                // Language not fount from config
            }
        }

        return $languages;
    }

    /**
     * @return LanguageEnum|null
     */
    public static function getActiveLanguage(): ?Enumerable
    {
        try {
            return LanguageEnum::from(App::getLocale());
        } catch (\Exception $exception) {
            // Language not fount from config
        }

        return null;
    }

    /**
     * @param Request $request|null
     * @param string $language
     * @return RedirectResponse
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public static function change(Request $request = null, string $language): RedirectResponse
    {
        session([
            self::SESSION_LANGUAGE => $language,
        ]);

        self::load();

        if ($request && $request->input('change_page')) {
            return PageService::switchPageContentByPageUrlAndLanguage($request);
        }

        return \redirect()->back();
    }

    /**
     * @param Request $request
     * @param string $language
     */
    public static function changeBackend(Request $request, string $language)
    {
        session([
            self::SESSION_LANGUAGE => $language,
        ]);
    }

    public static function load()
    {
        App::setLocale(session(self::SESSION_LANGUAGE, config('app.locale')));
    }
}
