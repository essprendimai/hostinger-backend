<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Providers;

use Collective\Html\FormFacade;
use Collective\Html\HtmlServiceProvider;
use Illuminate\Support\Facades\Gate;
use Essprendimai\Basic\Entities\User;
use Essprendimai\Basic\Http\Middleware\ForceChangePassword;
use Essprendimai\Basic\Http\Middleware\IsAdmin;
use Essprendimai\Basic\Http\Middleware\IsClient;
use Essprendimai\Basic\Http\Middleware\LanguageAccess;
use Essprendimai\Basic\Http\Middleware\RedirectIfAuthenticated;
use Essprendimai\Basic\Repositories\CategoryRepository;
use Essprendimai\Basic\Repositories\GroupRelationRepository;
use Essprendimai\Basic\Repositories\GroupRepository;
use Essprendimai\Basic\Repositories\PageRepository;
use Essprendimai\Basic\Repositories\UserRepository;
use Essprendimai\Basic\Services\LanguageService;
use Essprendimai\Basic\Services\PageFileService;
use Essprendimai\Basic\Services\PageTransactionService;
use Essprendimai\Basic\Services\RouteAccessManager;
use Illuminate\Container\EntryNotFoundException;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Nwidart\Modules\Facades\Module;
use Nwidart\Modules\LaravelModulesServiceProvider;
use Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider;
use Watson\BootstrapForm\BootstrapFormServiceProvider;
use Watson\BootstrapForm\Facades\BootstrapForm;
use Illuminate\Database\Eloquent\Factory;

/**
 * Class BasicServiceProvider
 * @package Essprendimai\Basic\Providers
 */
class BasicServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * @throws EntryNotFoundException
     * @throws \LogicException
     */
    public function boot(): void
    {
        \Schema::defaultStringLength(191);

        $this->registerConfig();
        $this->registerMiddleware();
        $this->publishTranslations();
        $this->registerViews();
        $this->loadMigrationsFrom(realpath(__DIR__ . '/../Database/Migrations'));
        $this->registerAssets();
        $this->loadHelpers();
        $this->bootTranslationGate();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(BootstrapFormServiceProvider::class);
        $this->app->register(LaravelModulesServiceProvider::class);
        $this->app->register(LaravelLogViewerServiceProvider::class);
        $this->app->register(HtmlServiceProvider::class);

        AliasLoader::getInstance()->alias('Form', FormFacade::class);
        AliasLoader::getInstance()->alias('BootForm', BootstrapForm::class);
        AliasLoader::getInstance()->alias('Module', Module::class);

        $this->registerFactories();

        $this->registerServiceProviders();
        $this->registerRepositories();
        $this->registerServices();
    }

    protected function registerConfig(): void
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('basic.php'),
        ], 'config');

        $this->mergeConfigFrom(__DIR__ . '/../Config/config.php', 'basic');
        // Set login guard for clients
        config([
            'auth.guards.client' => [
                'driver' => 'session',
                'provider' => 'clients',
            ],
            'auth.providers.clients' => [
                'driver' => 'eloquent',
                'model' => User::class,
            ],
            'auth.passwords.clients' => [
                'provider' => 'clients',
                'table' => 'password_resets',
                'expire' => 60,
            ],
            'filesystems.disks.' . PageFileService::DISK_PAGE_IMAGES => [
                'driver' => 'local',
                'root' => PageFileService::getRootPath(),
                'url' => PageFileService::getUrlPath(),
                'visibility' => 'public',
            ],
        ]);
    }

    private function registerViews(): void
    {
        $viewPath = base_path('resources/views/modules/basic');
        $menuPath = base_path('resources/views/modules/basic/menu');
        $rolePath = base_path('resources/views/modules/basic/role');
        $userPath = base_path('resources/views/modules/basic/user');
        $clientPath = base_path('resources/views/modules/basic/client');
        $includesPath = base_path('resources/views/modules/basic/includes');
        $authPath = base_path('resources/views/auth');
        $notificationsPath = base_path('resources/views/notifications');
        $mailPath = base_path('resources/views/mail');
        $groups = base_path('resources/views/groups');
        $appGatePath = base_path('resources/views/modules/basic/layouts/app-gate.blade.php');
        $logsPath = base_path('resources/views/vendor/laravel-log-viewer/log.blade.php');

        $sourcePath = __DIR__ . '/../Resources/views';
        $authSourcePath = __DIR__ . '/../Resources/views/auth';
        $notificationsSourcePath = __DIR__ . '/../Resources/views/notifications';
        $mailSourcePath = __DIR__ . '/../Resources/views/mail';
        $groupsSourcePath = __DIR__ . '/../Resources/views/groups';
        $menuSourcePath = __DIR__ . '/../Resources/views/menu';
        $roleSourcePath = __DIR__ . '/../Resources/views/role';
        $userSourcePath = __DIR__ . '/../Resources/views/user';
        $clientSourcePath = __DIR__ . '/../Resources/views/client';
        $includesSourcePath = __DIR__ . '/../Resources/views/includes';
        $gateSourcePath = __DIR__ . '/../Resources/views/layouts/app-gate.blade.php';
        $logsSourcePath = __DIR__ . '/../Resources/views/log.blade.php';

        $this->publishes([$sourcePath => $viewPath], 'views');
        $this->publishes([$authSourcePath => $authPath], 'auth');
        $this->publishes([$mailSourcePath => $mailPath], 'mail');
        $this->publishes([$notificationsSourcePath => $notificationsPath], 'notifications');
        $this->publishes([$menuSourcePath => $menuPath], 'menu');
        $this->publishes([$includesSourcePath => $includesPath], 'includes');
        $this->publishes([$gateSourcePath => $appGatePath], 'app-gate');
        $this->publishes([$roleSourcePath => $rolePath], 'role');
        $this->publishes([$userSourcePath => $userPath], 'user');
        $this->publishes([$clientSourcePath => $clientPath], 'client');
        $this->publishes([$logsSourcePath => $logsPath], 'log');
        $this->publishes([$groupsSourcePath => $groups], 'groups');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/modules/basic';
        }, config('view.paths')), [$sourcePath]), 'basic-view');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/modules/basic/auth';
        }, config('view.paths')), [$authSourcePath]), 'basic-view');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/modules/basic/mail';
        }, config('view.paths')), [$authSourcePath]), 'basic-view');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/vendor/notifications';
        }, config('view.paths')), [$notificationsSourcePath]), 'basic-view');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/modules/basic/groups';
        }, config('view.paths')), [$groupsSourcePath]), 'basic-view');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/modules/basic/menu';
        }, config('view.paths')), [$menuSourcePath]), 'basic-view');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/modules/basic/app-gate';
        }, config('view.paths')), [$gateSourcePath]), 'basic-view');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/modules/basic/role';
        }, config('view.paths')), [$roleSourcePath]), 'basic-view');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/modules/basic/user';
        }, config('view.paths')), [$userSourcePath]), 'basic-view');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/vendor/laravel-log-viewer';
        }, config('view.paths')), [$logsSourcePath]), 'basic-view');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/modules/basic/includes';
        }, config('view.paths')), [$includesSourcePath]), 'basic-view');

        $this->loadViewsFrom(array_merge(array_map(function($path) {
            return $path . '/modules/basic/client';
        }, config('view.paths')), [$clientSourcePath]), 'basic-view');
    }

    private function registerServiceProviders(): void
    {
        $this->app->register(RouteServiceProvider::class);
    }

    private function registerAssets(): void
    {
        $this->publishes([
            __DIR__ . '/../Assets/dist/css' => public_path('css/basic-view'),
            __DIR__ . '/../Assets/dist/fonts' => public_path('fonts'),
            __DIR__ . '/../Assets/dist/js' => public_path('js/basic-view'),
            __DIR__ . '/../Assets/dist/images' => public_path('images'),
        ], 'public');
    }

    private function publishTranslations(): void
    {
        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'basic');

        $this->publishes([
            __DIR__ . '/../Resources/lang' => resource_path('lang'),
        ], 'lang');
    }

    private function registerMiddleware()
    {
        /** @var Router $router */
        $router = $this->app['router'];
        $router->pushMiddlewareToGroup('web', LanguageAccess::class);
        $router->pushMiddlewareToGroup('web', ForceChangePassword::class);
        $router->aliasMiddleware('custom-guest', RedirectIfAuthenticated::class);
        $router->aliasMiddleware('auth.client', IsClient::class);
        $router->aliasMiddleware('auth.admin', IsAdmin::class);
    }
    
    private function registerRepositories(): void
    {
        $this->app->singleton(UserRepository::class);
        $this->app->singleton(CategoryRepository::class);
        $this->app->singleton(PageRepository::class);
        $this->app->singleton(GroupRepository::class);
        $this->app->singleton(GroupRelationRepository::class);
    }

    private function registerServices(): void
    {
        $this->app->singleton(RouteAccessManager::class);
        $this->app->singleton(LanguageService::class);
        $this->app->singleton(PageTransactionService::class);
        $this->app->singleton(PageFileService::class);
    }

    private function loadHelpers(): void
    {
        include_once __DIR__ . '/../Helpers/route_access.php';
        include_once __DIR__ . '/../Helpers/page.php';
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories(): void
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/Factories');
        }
    }

    private function bootTranslationGate(): void
    {
        Gate::define('ltm-admin-translations', function ($user) {
            /* @var $user \App\User */
            // modify the code below to return true for users that can administer translations
            //return $user && $user->is_admin;
            return true;
        });

        Gate::define('ltm-bypass-lottery', function ($user) {
            /* @var $user \App\User */
            // modify the code below to return true for users that can administer translations
            // or edit translation so that missing keys will be logged for all sessions of
            // these users instead of one out of N sessions as given by random lottery result
            return true;
        });

        Gate::define('ltm-list-editors', function ($user, $connection_name, &$user_list) {
            /* @var $connection_name string */
            /* @var $user \App\User */
            /* @var $query  \Illuminate\Database\Query\Builder */
            $query = $user->on($connection_name)->getQuery();

            // modify the query to return only users that can edit translations and can be
            // managed by $user if you have a an editor scope defined on your user model
            // you can use it to filter only translation editors
            $user_list = $query->orderby('id')->get(['id', 'email']);

            // if the returned list is empty then no per locale admin will be shown for
            // the current user.
            return $user_list;
        });
    }
}
