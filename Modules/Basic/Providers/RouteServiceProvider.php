<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

/**
 * Class RouteServiceProvider
 * @package Essprendimai\Basic\Providers
 */
class RouteServiceProvider extends ServiceProvider
{
    /**
     * The root namespace to assume when generating URLs to actions.
     *
     * @var string
     */
    protected $namespace = 'Essprendimai\Basic\Http\Controllers';

    /**
     * Define the routes for the application.
     */
    public function map(): void
    {
        if (!app()->routesAreCached()) {
            $this->mapWebRoutes();
        }
    }

    /**
     * Map web routes
     *
     * @return void
     */
    private function mapWebRoutes(): void
    {
        \Route::middleware(['web'])
            ->namespace($this->namespace)
            ->group($this->modulePath('routes/web.php'));
    }

    /**
     * @param string $path
     * @return string
     */
    private function modulePath(string $path): string
    {
        return __DIR__ . '/../' . $path;
    }
}
