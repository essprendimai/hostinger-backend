<?php

declare(strict_types = 1);

use Essprendimai\Basic\Enums\LanguageEnum;
use Essprendimai\Basic\Enums\LanguageRouteNameEnum;
use Essprendimai\Basic\Http\Controllers\Auth\LoginController;
use Essprendimai\Basic\Http\Controllers\Auth\RegisterController;
use Essprendimai\Basic\Http\Middleware\EnsureEmailIsVerified;
use Essprendimai\Basic\Http\Middleware\IsClient;
use Essprendimai\Basic\Http\Middleware\LimitsBackendRouteAccess;
use Illuminate\Auth\Middleware\Authenticate;

// ---------- Everyone ----------
Route::get('/', 'WelcomeController@index')->name('welcome');
Route::post('admin-panel-logout', 'Auth\LoginController@logout')->name('admin-panel.logout');
Route::post(LoginController::CLIENT_LOGOUT_ROUTE_NAME, 'Auth\LoginController@logout')->name('client.logout');

// ---------- Verify email----------
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

// ---------- Reset password ----------
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// ---------- Guest ----------
Route::group([
    'middleware' => ['web', 'custom-guest'],
], function(){
    Route::get(LoginController::ADMIN_LOGIN_ROUTE_NAME, 'Auth\LoginController@showLoginForm')->name('admin-panel.login-form');
    Route::get(LoginController::ADMIN_LOGIN_ROUTE_NAME, 'Auth\LoginController@showLoginForm')->name('login');
    Route::post(LoginController::ADMIN_LOGIN_ROUTE_NAME, 'Auth\LoginController@login')->name('admin-panel.login');

    Route::get(LoginController::CLIENT_LOGIN_ROUTE_NAME, 'Auth\LoginController@showClientLoginForm')->name('client.login-form');
    Route::post(LoginController::CLIENT_LOGIN_ROUTE_NAME, 'Auth\LoginController@login')->name('client.login');

    Route::get(RegisterController::CLIENT_REGISTER_ROUTE_NAME, 'Auth\RegisterController@showRegistrationForm')->name('client.register-form');
    Route::post(RegisterController::CLIENT_REGISTER_ROUTE_NAME, 'Auth\RegisterController@register')->name('client.register');
});

// ---------- Client ----------
Route::group([
    'middleware' => ['web', IsClient::class, EnsureEmailIsVerified::class],
    'prefix' => 'client',
    'as' => 'client.',
], function(){
    Route::get('home', 'ClientHomeController@index')->name('home');

    Route::get('profile/edit', 'ClientProfileController@edit')->name('profile.edit');
    Route::post('profile/store', 'ClientProfileController@store')->name('profile.store');
});

// ---------- Language ----------
Route::group([
    'middleware' => ['web'],
], function(){
    Route::get('change-language/{language}', 'LanguageController@change')->name('language.change');
    Route::get('change-language-backend/{language}', 'LanguageController@changeBackend')->name('language.change-backend');

    foreach (LanguageEnum::enum() as $language) {
        Route::get(LanguageRouteNameEnum::from($language->id())->name() . '/{pageUrl}', 'PagesController@showPageByPageUrl')
            ->name('page.' . $language->id() . '.page-by-url');

        Route::get(LanguageRouteNameEnum::from($language->id())->name() . '/{categoryUrl}/{pageUrl}', 'PagesController@showPageByCategoryAndPageUrl')
            ->name('page.' . $language->id() . '.page-by-category-url');
    }
});

// ---------- Admin ----------
Route::group([
    'middleware' => ['web', Authenticate::class, LimitsBackendRouteAccess::class],
    'prefix' => 'backend',
], function(){
    Route::get('home', 'HomeController@index')->name('backend.home');

    Route::get('change-password', 'ChangePasswordController@changePassword')->name('backend.change-password.form');
    Route::post('change-password', 'ChangePasswordController@postChangePassword')->name('backend.change-password.post');

    Route::resource('users', 'UserController', [
        'except' => [
            'edit',
        ],
        'names' => [
            'store' => 'backend.users.store',
            'show' => 'backend.users.show',
            'index' => 'backend.users.index',
            'destroy' => 'backend.users.destroy',
            'update' => 'backend.users.update',
            'create' => 'backend.users.create',
        ],
    ]);
    Route::post('users/send-message', 'UserController@sendMessageToUsers')->name('backend.users.send-message-to-users');
    Route::post('users/send-message-from-group', 'UserController@sendMessageToUsersFromGroup')->name('backend.users.send-message-to-users-from-group');
    Route::post('users/add-users-to-group', 'UserController@addUsersToGroup')->name('backend.users.add-users-to-group');
    Route::get('users/delete-user-from-group/{userId}/{groupId}','UserController@removeFromGroup')->name('backend.users.remove-user-from-group');

    Route::resource('role', 'RolesController', ['as' => 'backend']);
    Route::resource('categories', 'CategoriesController', ['as' => 'backend']);
    Route::resource('groups', 'GroupsController', [
        'except' => [
            'show',
        ],
        'as' => 'backend',
    ]);

    Route::resource('pages', 'PagesController', [
        'except' => [
            'show',
        ],
        'as' => 'backend',
    ]);
    Route::get('pages/change-status/{page}/{status}', 'PagesController@changeStatus')->name('backend.pages.change_status');
    Route::get('pages/group', 'PagesController@groupIndex')->name('backend.pages.group.index');

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('backend.logs');
    Route::get('send-verification/{user}', 'Auth\AdminVerificationController@sendVerification')->name('backend.send-verification');
    Route::get('send-draft-user-verification/{user}', 'Auth\AdminVerificationController@sendDraftUserVerification')->name('backend.send-draft-user-verification');
});
