<?php

return [
    'add_user_to_group_modal'   => [
        'group'             => 'Grupė',
        'selected_users'    => 'Pasirinkti vartotojai',
    ],
    'buttons'                   => [
        'add'                   => 'Pridėti',
        'add_to_group'          => 'Pridėti į grupę',
        'edit'                  => 'Redaguoti',
        'send'                  => 'Siųsti',
        'send_message'          => 'Siųsti žinutę',
        'send_message_group'    => 'Siųsti žinutę per grupę',
        'verify'                => 'Siųsti patvirtinimą',
        'verify_draft_user'     => 'Siųsti patvirtinimą DRAFT vartotojui',
    ],
    'change_page'               => [
        'button_update'                 => 'Atnaujinti',
        'field_email'                   => 'Elektroninis paštas',
        'field_force_password_change'   => 'Priverstinai pakeisti slaptažodį',
        'field_name'                    => 'Vardas',
        'field_password'                => 'Slaptažodis',
        'field_roles'                   => 'Teisės',
        'is_admin'                      => 'Adminas',
        'title'                         => 'Vartotojas',
        'title_description'             => 'Vartotojo duomenys',
    ],
    'change_password'           => [
        'new_password'              => 'Naujas slaptažodis',
        'new_password_confirmation' => 'Patvirtinkite naują slaptažodį',
        'old_password'              => 'Senas slaptažodis',
        'submit'                    => 'Pakeisti',
        'title'                     => 'Pasikeiskite slaptažodį',
        'title_description'         => 'Adminas privertė Jus pasikeisti slaptažodį. Kol nepasikeisite tol negalėsite pasiekti kitų puslapių',
    ],
    'draft'                     => 'DRAFT',
    'email_verified'            => 'Jūsų el. paštas buvo patvirtintas',
    'error_close_button'        => 'Uždaryti',
    'error_select_user'         => 'Nei vienas vartotojas nepasirinktas',
    'error_title'               => 'Pranešimas!',
    'failed_verification_sent'  => 'Elektroninis paštas jau aktyvuotas',
    'is_admin_types'            => [
        'admin' => 'ADMINAS',
        'client'=> 'KLIENTAS',
    ],
    'menu'                      => [
        'create_user'   => 'Sukurti vartotoją',
        'roles'         => 'Teisės',
        'users'         => 'Vartotojai',
    ],
    'search'                    => 'Paieška',
    'send_message_group_modal'  => [
        'group'     => 'Grupė',
        'message'   => 'Žinutė',
    ],
    'send_message_modal'        => [
        'message'           => 'Žinutė',
        'selected_users'    => 'Pasirinkti vartotojai',
    ],
    'snx_ee'                    => 'SNX EE',
    'user'                      => 'Vartotojai',
    'users_page'                => [
        'actions'               => 'Veiksmai',
        'badges'                => 'Požymiai',
        'confirm_delete'        => 'Tikrai trinti?',
        'create'                => 'Sukurti',
        'current_users'         => 'Egzistuojantys vartotojai',
        'delete'                => 'Ištrinti',
        'email'                 => 'Elektroninis paštas',
        'force_password_change' => 'Priverstinai pakeisti slaptažodį',
        'group'                 => 'Grupė',
        'groups'                => 'Grupės',
        'id'                    => 'ID',
        'is_admin'              => 'Ar adminas',
        'name'                  => 'Vardas',
        'password'              => 'Slaptažodis',
        'roles'                 => 'Teisės',
        'sub_title'             => 'Vartotojai',
        'title'                 => 'Vartotojai',
    ],
    'verification_sent'         => 'Patvirtinimo laiškas išsiųstas vartotojui',
    'verified'                  => 'PATVIRTINTAS',
];
