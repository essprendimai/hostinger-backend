<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'email'                     => 'Elektroninis paštas',
    'login'                     => 'Prisijungimo langas',
    'page_title'                => 'Atkurti slaptažodį',
    'password'                  => 'Slaptažodis privalo būti mažiausiai 6 simbolių ir sutapti su teisėmis.',
    'password_confirm_field'    => 'Patvirtinti slaptažodį',
    'password_field'            => 'Slaptažodis',
    'reset'                     => 'Slaptažodis atnaujintas!',
    'reset_button'              => 'Atkurti slaptažodį',
    'reset_link'                => 'Siųsti slaptažodžio atstatymo nuorodą',
    'sent'                      => 'Mes nusiuntėle patvirtinimo nuorodą nurodytu paštu!',
    'token'                     => 'Slaptažodžio susigrąžinimo raktas neteisingas.',
    'user'                      => 'Negalime rasti vartotojo su tokiu elektroniniu paštu.',
];
