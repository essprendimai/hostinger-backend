<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'                => 'Pateikti duomenys nesutampa su mūsų duomenimis.',
    'throttle'              => 'Per daug kartų bandyta prisijungti. Bandykite dar kartą po :seconds sekundžių.',
    'verify'                => 'Aktyvuokite paskyrą per nuorodą, kuri buvo išsiųsta elektroniniu paštu',
    'verify_account_email'  => 'Jums yra išsiųstas el. Laiškas su patvirtinimo nuoroda, patikrinkite pašto dėžutę ir patvirtinkite paskyrą!',
];
