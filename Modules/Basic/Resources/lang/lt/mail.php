<?php

return [
    'failed_to_send'            => 'Nepavyko nusiųsti laiško',
    'send_message_to_users'     => [
        'labels'    => [
            'message'   => 'Žinutė',
        ],
        'subject'   => 'Žinutė',
    ],
    'send_reset_password'       => [
        'footer'        => 'Jei Jūs niekur nepateikėte prašyma prašome nesikeisti arba pranešti mums.',
        'reset_button'  => 'Atstatyti slaptažodį',
        'subject'       => 'Atstatyti slaptažodį',
        'title'         => 'Gavote šį laišką, nes pateikėte prašymą atstatyti slaptažodį.',
    ],
    'successfully_sent'         => 'Laiškas nusiųstas',
    'verify_draft_user_email'   => [
        'labels'    => [
            'button_text'               => 'Patvirtinti elektroninio pašto adresą',
            'if_you_not_create_ignore'  => 'Jei nekūrėte paskyros, jokių papildomų veiksmų jums nereikia atlikti.',
            'password'                  => 'Jūsų laikinas slaptažodis:',
            'please_click'              => 'Jei norite patvirtinti el. pašto adresą, paspauskite žemiau esantį mygtuką.',
        ],
        'subject'   => 'Elektroninio pašto adreso patvirtinimas',
    ],
    'verify_mail'               => [
        'activate'  => 'Aktyvuoti',
        'footer'    => 'Jeu nesiregistravote, jokių veiksmų nedarykite',
        'subject'   => 'Patvirtinti elektroninį paštą',
        'title'     => 'Paspauskite žemiau esantį mygtuką, kad aktyvuotumėte savo paskyrą.',
    ],
];
