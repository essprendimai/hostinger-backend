<?php

return [
    'english'               => 'Anglų',
    'estonia'               => 'Estų',
    'language'              => 'Kalba',
    'lithuanian'            => 'Lietuvių',
    'menu'                  => [
        'translate' => 'Vertimas',
    ],
    'translation_manager'   => [
        'description'   => 'Vertimai nėra matomi tol, kol juos paskelbia administratorius',
        'title'         => 'Vertimu valdymas',
    ],
];
