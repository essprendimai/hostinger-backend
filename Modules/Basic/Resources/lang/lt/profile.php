<?php

return [
    'edit_page'         => [
        'buttons'   => [
            'change'    => 'Redaguoti',
        ],
        'form'      => [
            'password'                  => 'Slaptažodis',
            'password_new'              => 'Naujas slaptažodis',
            'password_new_confirmation' => 'Pakartoti naują slaptažodį',
        ],
        'title'     => 'Profilio redagavimas',
    ],
    'error_messages'    => [
        'password_do_not_match' => 'Klaidingas slaptažodis',
    ],
];
