<?php

return [
    'client_page'   => [
        'buttons'           => [
            'home'      => 'Pagrindinis',
            'login'     => 'Prisijungimas',
            'register'  => 'Sukurti paskyrą',
        ],
        'confirm_password'  => 'Pakartoti slaptažodį',
        'email'             => 'Elektroninis paštas',
        'password'          => 'Slaptažodis',
        'success'           => 'Registracija sėkminga. Patvirtinkite el. paštą.',
        'title'             => 'Registracija',
    ],
];
