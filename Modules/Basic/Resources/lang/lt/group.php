<?php

return [
    'buttons'       => [
        'activity'          => 'Aktyvuoti',
        'change'            => 'Pakeisti',
        'confirm_delete'    => 'Tikrai trinti?',
        'create'            => 'Sukurti',
        'delete'            => 'Trinti',
        'disable'           => 'Išjungti',
        'edit'              => 'Redaguoti',
        'enable'            => 'Įjungti',
        'preview'           => 'Peržiūrėti',
        'recreate'          => 'Sukurti iš naujo',
        'save'              => 'Išsaugoti',
        'send_email'        => 'Siųsti elektroninį laišką',
        'update'            => 'Atnaujinti',
        'upload'            => 'Įkelti',
    ],
    'category'      => 'Kategorija',
    'errors'        => [
        'already_exists'    => 'Jau tokia kategorija egzistuoja',
    ],
    'group'         => 'Group',
    'group_page'    => [
        'create'                    => 'Sukurti',
        'create_title_description'  => 'Forma skirta sukurti grupes',
        'edit_title'                => 'Grupės redagavimas',
        'edit_title_description'    => 'Grupės redagavimo forma',
        'form'                      => [
            'actions'   => 'Veiksmai',
            'key'       => 'Raktas',
            'title'     => 'Pavadinimas',
            'type'      => 'Tipas',
        ],
        'title'                     => 'Grupės',
        'title_description'         => 'Sukurti grupes email laiškams grupuoti, menių ir t.t.',
    ],
    'menu'          => 'Menu',
    'menu_group'    => 'Menu (iš kategorijų)',
    'menu_title'    => 'Grupės',
    'page_category' => 'Puslapių kategorijos',
    'search'        => 'Paieška',
];
