<?php

return [
    'client_page'   => [
        'email'             => 'Elektroninis paštas',
        'forgot_password'   => 'Pamiršau slaptažodį',
        'password'          => 'Slaptažodis',
        'register'          => 'Registruotis',
        'remember_me'       => 'Prisimint',
        'sign_up'           => 'Prisijungti',
        'title'             => 'Prisijungti prie paskyros',
    ],
];
