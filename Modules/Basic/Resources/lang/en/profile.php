<?php

return [
    'edit_page'         => [
        'buttons'   => [
            'change'    => 'Change password',
            'change2'   => 'Change password2',
        ],
        'form'      => [
            'password'                  => 'Current password',
            'password_new'              => 'New password',
            'password_new_confirmation' => 'Repeat new password',
        ],
        'title'     => 'Change password',
    ],
    'error_messages'    => [
        'password_do_not_match' => 'Passwords do not match',
    ],
];
