<?php

return [
    'failed_to_send'            => 'Failed to sent your message',
    'send_message_to_users'     => [
        'labels'    => [
            'message'   => 'Message',
        ],
        'subject'   => 'Message',
    ],
    'send_reset_password'       => [
        'footer'        => 'If you did not request a password reset, no further action is required.',
        'reset_button'  => 'Reset Password',
        'subject'       => 'Reset password',
        'title'         => 'You are receiving this email because we received a password reset request for your account.',
    ],
    'successfully_sent'         => 'Your message was sent',
    'verify_draft_user_email'   => [
        'labels'    => [
            'button_text'               => 'Verify your account',
            'if_you_not_create_ignore'  => 'If you did not create an account, no further action is required.',
            'password'                  => 'Your temporary password:',
            'please_click'              => 'Please click the button below to verify your email address.',
        ],
        'subject'   => 'Verify your account',
    ],
    'verify_mail'               => [
        'activate'  => 'Verify your account',
        'footer'    => 'If you did not create an account, no further action is required.',
        'subject'   => 'Verify Email Address',
        'title'     => 'Please click the button below to verify your email address.',
    ],
];
