<?php

return [
    'client_page'   => [
        'buttons'           => [
            'home'      => '< back',
            'login'     => 'Login',
            'register'  => 'Create my account',
        ],
        'confirm_password'  => 'Confirm Password',
        'email'             => 'E-mail address',
        'password'          => 'Password',
        'success'           => 'To finish the registration please check your email and press the verification button',
        'title'             => 'Registration',
    ],
];
