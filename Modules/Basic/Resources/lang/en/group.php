<?php

return [
    'buttons'       => [
        'activity'          => 'Activate',
        'change'            => 'Change',
        'confirm_delete'    => 'Confirm delete?',
        'create'            => 'Create',
        'delete'            => 'Delete',
        'disable'           => 'Disable',
        'edit'              => 'Edit',
        'enable'            => 'Enable',
        'preview'           => 'Preview',
        'recreate'          => 'Recreate',
        'save'              => 'Save',
        'send_email'        => 'Send email',
        'update'            => 'Update',
        'upload'            => 'Upload',
    ],
    'category'      => 'Category',
    'errors'        => [
        'already_exists'    => 'Group already exists',
    ],
    'group'         => 'Group',
    'group_page'    => [
        'create'                    => 'Create',
        'create_title_description'  => 'Form to create group',
        'edit_title'                => 'Groups edit form',
        'edit_title_description'    => 'Edit group',
        'form'                      => [
            'actions'   => 'Actions',
            'key'       => 'Key',
            'title'     => 'Title',
            'type'      => 'Type',
        ],
        'title'                     => 'Groups',
        'title_description'         => 'Create groups for emails, menus and more..',
    ],
    'menu'          => 'Menu',
    'menu_group'    => 'Menu (from groups)',
    'menu_title'    => 'Groups',
    'page_category' => 'Page categories',
    'search'        => 'Search',
];
