<?php

return [
    'menu'      => 'Logs',
    'page_log'  => [
        'alert_delete'                  => 'Are you sure?',
        'button_clean_file_label'       => 'Clean file',
        'button_delete_all_files_label' => 'Delete all files',
        'button_delete_file_label'      => 'Delete file',
        'button_download_file_label'    => 'Download file',
        'max_log_file'                  => 'Log file >50M, please download it.',
        'table_title_content'           => 'Content',
        'table_title_context'           => 'Context',
        'table_title_date'              => 'Date',
        'table_title_level'             => 'Level',
        'table_title_line_number'       => 'Line number',
        'title'                         => 'Logs',
        'title_description'             => 'All admin panel failed actions',
    ],
];
