<?php

return [
    'english'               => 'English',
    'estonia'               => 'Estonia',
    'language'              => 'Language',
    'lithuanian'            => 'Lithuania',
    'menu'                  => [
        'translate' => 'Translate',
    ],
    'translation_manager'   => [
        'description'   => 'Translations are not visible until they are published by the administrator',
        'title'         => 'Translation Manager',
    ],
];
