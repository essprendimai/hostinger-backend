<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'                => 'Wrong email address or password. Please check.',
    'throttle'              => 'Too many login attempts. Please try again in :seconds seconds.',
    'verify'                => 'Verify your email',
    'verify_account_email'  => 'Email with a confirmation link has been sent, please check your inbox and verify your account!',
];
