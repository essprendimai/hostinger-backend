<?php

return [
    'client_page'   => [
        'email'             => 'Email',
        'forgot_password'   => 'Forgot password',
        'password'          => 'Password',
        'register'          => 'Register',
        'remember_me'       => 'Remember me',
        'sign_up'           => 'Log in',
        'title'             => 'Login to my account',
    ],
];
