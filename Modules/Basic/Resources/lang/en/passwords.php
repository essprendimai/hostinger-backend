<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'email'                     => 'E-Mail Address',
    'login'                     => 'Login page',
    'page_title'                => 'Reset password',
    'password'                  => 'Passwords must be at least six characters and match the confirmation.',
    'password_confirm_field'    => 'Confirm password',
    'password_field'            => 'Password',
    'reset'                     => 'Your password has been reset!',
    'reset_button'              => 'Reset password',
    'reset_link'                => 'Reset the password',
    'sent'                      => 'A link to reset the password has been sent. Please check your email.',
    'token'                     => 'This password reset token is invalid.',
    'user'                      => 'User with the entered email address was not found',
];
