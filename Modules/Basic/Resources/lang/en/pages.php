<?php

return [
    'activate'          => 'Activate',
    'active'            => 'ACTIVE',
    'categories_page'   => [
        'actions'                   => 'Actions',
        'category_title'            => 'Title',
        'category_url'              => 'Category url',
        'confirm_delete'            => 'Confirm delete?',
        'create'                    => 'Create',
        'delete'                    => 'Delete',
        'edit'                      => 'Edit',
        'edit_title'                => 'Edit category',
        'edit_title_description'    => 'Edit all categories with ":attribute" key',
        'empty_list'                => 'Empty list',
        'key'                       => 'Key',
        'new_title'                 => 'New category',
        'new_title_description'     => 'Create new category',
        'title'                     => 'Categories',
        'title_description'         => 'List of all categories',
    ],
    'disable'           => 'Disable',
    'inactive'          => 'INACTIVE',
    'menu'              => [
        'categories'    => 'Categories',
        'menu'          => 'Menu',
        'new'           => 'New',
        'pages'         => 'Pages',
    ],
    'pages'             => 'Pages',
    'pages_page'        => [
        'actions'                   => 'Actions',
        'active'                    => 'Active',
        'category_id'               => 'Page related to category',
        'confirm_delete'            => 'Confirm delete?',
        'create'                    => 'Create',
        'delete'                    => 'Delete',
        'edit'                      => 'Edit',
        'edit_title'                => 'Edit page',
        'edit_title_description'    => 'Edit all pages with ":attribute" key',
        'empty_list'                => 'Empty list',
        'external_url'              => 'External url',
        'featured_image'            => 'Featured image',
        'group_id'                  => 'Select group',
        'is_template'               => 'Template',
        'key'                       => 'Key',
        'menu_id'                   => 'Page related to menu',
        'new_title'                 => 'New page',
        'new_title_description'     => 'Create new category',
        'page_relation_type'        => 'Page relation type',
        'page_text'                 => 'Page content',
        'page_title'                => 'Title',
        'page_url'                  => 'Page url',
        'text'                      => 'Text',
        'title'                     => 'Pages',
        'title_description'         => 'List of all pages. Loads page list by current language. If for example you want to see LT pages change your language to LT.',
    ],
    'route_name'        => 'page',
    'template'          => 'TEMPLATE',
];
