<?php

return [
    'buttons'       => [
        'activity'          => 'Aktiveeri',
        'change'            => 'Muuda',
        'confirm_delete'    => 'Kinnita kustutamine?',
        'create'            => 'Loo',
        'delete'            => 'Kustuta',
        'disable'           => 'Keela',
        'edit'              => 'Muuda',
        'enable'            => 'Luba',
        'preview'           => 'Eelvaade',
        'recreate'          => 'Loo uuesti',
        'save'              => 'Salvesta',
        'send_email'        => 'Saada email',
        'update'            => 'Uuenda',
        'upload'            => 'Laadi üles',
    ],
    'category'      => 'Kategooria',
    'errors'        => [
        'already_exists'    => 'Grupp on juba olemas',
    ],
    'group'         => 'Grupp',
    'group_page'    => [
        'create'                    => 'Loo',
        'create_title_description'  => 'Vorm grupi loomiseks',
        'edit_title'                => 'Gruppide redigeerimisvorm',
        'edit_title_description'    => 'Muuda gruppi',
        'form'                      => [
            'actions'   => 'Meetmed',
            'key'       => 'Võti',
            'title'     => 'Pealkiri',
            'type'      => 'Tüüp',
        ],
        'title'                     => 'Grupid',
        'title_description'         => 'Loo e-kirjade, menüüde ja muu grupi ..',
    ],
    'menu'          => 'Menüü',
    'menu_group'    => 'Menüü (rühmadest)',
    'menu_title'    => 'Grupid',
    'page_category' => 'Lehekülje kategooriad',
    'search'        => 'Otsing',
];
