<?php

return [
    'client_page'   => [
        'buttons'           => [
            'home'      => 'Kodu',
            'login'     => 'Logi sisse',
            'register'  => 'Registreeri',
        ],
        'confirm_password'  => 'Kinnita salasõna',
        'email'             => 'E-posti aadress',
        'password'          => 'Parool',
        'success'           => 'To finish the registration please check your email and press the verification button',
        'title'             => 'Registreerimine',
    ],
];
