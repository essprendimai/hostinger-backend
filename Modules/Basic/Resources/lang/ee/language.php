<?php

return [
    'english'               => 'Inglise',
    'estonia'               => 'Eesti',
    'language'              => 'Keel',
    'lithuanian'            => 'Leedu',
    'menu'                  => [
        'translate' => 'Tõlgi',
    ],
    'translation_manager'   => [
        'description'   => 'Tõlked pole nähtavad enne, kui administraator on need avaldanud',
        'title'         => 'Tõlkehaldur',
    ],
];
