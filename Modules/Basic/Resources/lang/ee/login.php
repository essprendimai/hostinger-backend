<?php

return [
    'client_page'   => [
        'email'             => 'E-post',
        'forgot_password'   => 'Unustasid parooli',
        'password'          => 'Parool',
        'register'          => 'Registreeri',
        'remember_me'       => 'Mäleta mind',
        'sign_up'           => 'Registreeri',
        'title'             => 'Logi sisse',
    ],
];
