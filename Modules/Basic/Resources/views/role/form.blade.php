@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <?php /** @var \Essprendimai\Basic\Entities\Role $role */ ?>
                {!! BootForm::open(['model' => $role, 'store' => 'backend.role.store', 'update' => 'backend.role.update']) !!}
                {!! BootForm::text('name', __('basic::roles.roles_page.form_name')) !!}
                <div class="from-group">
                    <div class="toggle-box">
                        <input type="hidden" name="full_access" value="0"/>
                        <input class="tgl tgl-light" id="full_access" name="full_access"
                               value="1" type="checkbox" {{ $role->full_access ? 'checked' : '' }}/>
                        <label for="full_access">{{ __('basic::roles.roles_page.table_label_full_access') }}</label>
                    </div>
                </div>

                {!! BootForm::textarea('description', __('basic::roles.roles_page.form_description')) !!}

                <a id="check-all" href="javascript:void(0)" class="btn btn-sm btn-primary" role="button">{{ __('basic::roles.roles_page.button_check_all') }}</a>
                <a id="uncheck-all" href="javascript:void(0)" class="btn btn-sm btn-primary" role="button">{{ __('basic::roles.roles_page.button_uncheck_all') }}</a>
                <br/>
                <br/>

                <h3>{{ __('basic::roles.roles_page.table_label_accessible_routes') }}</h3>
                <div class="panel-body in">
                    @foreach($routes as $route)
                        <div class="from-group">
                            <div class="toggle-box" style="display: inline-block;">
                                <input class="tgl tgl-light route-toggle" id="route-{{ str_replace('.', '-', $route) }}"
                                       name="accessible_routes[]"
                                       value="{{ $route }}"
                                       type="checkbox" {{ in_array($route, (array)$role->accessible_routes) ? 'checked' : '' }}/>
                                <label class="tgl-btn" for="route-{{ str_replace('.', '-', $route) }}"></label>
                            </div>
                            <label for="route-{{ str_replace('.', '-', $route) }}">{{ $route }}</label>
                        </div>
                    @endforeach
                </div>
                {!! BootForm::submit(__('basic::roles.roles_page.button_save')) !!}
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        (function () {
            $('#check-all').on('click', function () {
                $('.route-toggle').attr('checked', 'checked');
            });

            $('#uncheck-all').on('click', function () {
                $('.route-toggle').removeAttr('checked');
            });
        })($);
    </script>
@endpush
