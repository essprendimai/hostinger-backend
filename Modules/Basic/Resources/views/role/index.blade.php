@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-universal-access"></i> {{ __('basic::roles.roles_page.title') }} &nbsp;&nbsp;&nbsp;  <a class="btn btn-primary" href="{{ route('backend.role.create') }}"><i class="fa fa-plus"></i> {{ __('basic::roles.roles_page.button_create') }}</a>
    </h1>
    <p>{{ __('basic::roles.roles_page.title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.role.index') }}">{{ __('basic::roles.roles_page.title') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                @if($roles->count())
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{ __('basic::roles.roles_page.table_label_role') }}</th>
                            <th>{{ __('basic::roles.roles_page.table_label_full_access') }}</th>
                            <th>{{ __('basic::roles.roles_page.table_label_accessible_routes') }}</th>
                            <th>{{ __('basic::roles.roles_page.table_label_description') }}</th>
                            <th>{{ __('basic::roles.roles_page.table_label_action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php /** @var \Essprendimai\Basic\Entities\Role $role */ ?>
                        @foreach($roles as $role)
                            <tr>
                                <td>{{ $role->name }}</td>
                                <td>
                                    <span class="label label-{{ $role->full_access ? 'success' : 'default' }}">
                                        {{ $role->full_access ? __('basic::roles.roles_page.yes') : __('basic::roles.roles_page.no') }}
                                    </span>
                                </td>
                                <td>{{ count($role->accessible_routes) }}</td>
                                <td>{{ str_limit($role->description) }}</td>
                                <td>
                                    <a href="{{ route('backend.role.edit', $role->id) }}" class="btn btn-sm btn-primary">
                                        <i class="fas fa-edit"></i>  {{ __('basic::roles.roles_page.button_edit') }}
                                    </a>

                                    {!! BootForm::inline(['route' => ['backend.role.destroy', $role->id], 'method' => 'DELETE', 'style' => 'display:inline']) !!}
                                    {!! Form::button(
                                      '<i class="fa fa-trash"></i> ' . __('basic::roles.roles_page.button_delete'),
                                     [
                                         'type' => 'submit',
                                         'class' => 'btn btn-danger btn-sm',
                                         'title' => __('basic::roles.roles_page.alert_delete_title'),
                                         'onclick'=>'return confirm("' . __('basic::roles.roles_page.alert_delete') . '")'
                                     ]) !!}
                                    {!! BootForm::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
