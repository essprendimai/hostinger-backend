@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-plus"></i> {{ __('basic::roles.roles_page.title_new') }}
    </h1>
    <p>{{ __('basic::roles.roles_page.title_new_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.role.index') }}">{{ __('basic::roles.roles_page.title') }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.role.create') }}">{{ __('basic::roles.roles_page.new') }}</a></li>
@endsection

@include('basic-view::role.form')
