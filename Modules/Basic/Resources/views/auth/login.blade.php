@extends('basic-view::layouts.app-gate', ['messages' => false])

@push('styles')
    <link href="/css/basic-view/login-page.css" rel="stylesheet">
@endpush

@section('content')
    <section class="login-block">
        <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="cla">
                    <div class="col-md-4 login-sec float-left">
                        <h2 class="text-center">Admin panel</h2>
                        <form class="form-horizontal login-form" method="POST" action="{{ route('admin-panel.login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="text-uppercase">Email</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span style="font-size: 12px" class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="text-uppercase">Password</label>
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span style="font-size: 12px" class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input">
                                    <small>Remember Me</small>
                                </label>
                                <button type="submit" class="btn btn-login float-right">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8 banner-sec float-right d-none d-lg-block d-md-block">
                        <div id="indicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#indicators" data-slide-to="0" class="active"></li>
                                <li data-target="#indicators" data-slide-to="1"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img class="d-block img-fluid" src="/images/login1.jpg" alt="Its your time">
                                    <div class="carousel-caption d-none d-md-block">
                                        <div class="banner-text">
                                            <h2>Its your time!</h2>
                                            <p>What’s the point of being alive if you don’t at least try to do something remarkable.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block img-fluid" src="/images/login2.jpg" alt="Make a small effort">
                                    <div class="carousel-caption d-none d-md-block">
                                        <div class="banner-text">
                                            <h2>Make a small effort!</h2>
                                            <p>Success is the sum of small efforts, repeated day-in and day-out.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
