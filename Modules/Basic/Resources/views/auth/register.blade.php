@extends('basic-view::layouts.app-gate')

@push('styles')
    <link href="/css/basic-view/login-page.css" rel="stylesheet">
@endpush

@section('content')
    <section class="login-block">
        <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="cla">
                    <div class="col-md-12 login-sec float-left">
                        <h2 class="text-center">{{  __('basic::register.client_page.title') }}</h2>

                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('client.register') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">{{  __('basic::register.client_page.email') }}</label>

                                    <div class="col-md-12">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">{{  __('basic::register.client_page.password') }}</label>

                                    <div class="col-md-12">
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-4 control-label">{{  __('basic::register.client_page.confirm_password') }}</label>

                                    <div class="col-md-12">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="row col-12 m-0 p-0">
                                    <div class="col-6">
                                        <a href="{{ route('client.login') }}">
                                            {{  __('basic::register.client_page.buttons.login') }}
                                        </a>

                                        &nbsp;&nbsp;|&nbsp;&nbsp;

                                        <a href="{{ route('welcome') }}">
                                            {{  __('basic::register.client_page.buttons.home') }}
                                        </a>
                                    </div>

                                    <div class="col-6">
                                        <button type="submit" class="btn btn-login float-right">
                                            {{  __('basic::register.client_page.buttons.register') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
