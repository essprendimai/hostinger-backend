@extends('basic-view::layouts.page.default')

@section('content')
    <section class="login-block">
        <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="cla">
                    <div class="col-md-12 login-sec float-left">
                        <h2 class="text-center">{{  __('basic::login.client_page.title') }}</h2>
                        <form class="form-horizontal login-form" method="POST" action="{{ route('client.login') }}">
                            <input type="hidden" name="auth_type" value="{{ \Essprendimai\Basic\Http\Controllers\Auth\LoginController::AUTH_CLIENT_TYPE }}"/>
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="text-uppercase">{{  __('basic::login.client_page.email') }}</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span style="font-size: 12px" class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="text-uppercase">{{  __('basic::login.client_page.password') }}</label>
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span style="font-size: 12px" class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input">
                                    <small>{{  __('basic::login.client_page.remember_me') }}</small>
                                </label>
                                <button type="submit" class="btn btn-primary float-right">{{  __('basic::login.client_page.sign_up') }}</button>
                                <a href="{{ route('password.request') }}" class="float-right m-2">{{  __('basic::login.client_page.forgot_password') }}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
