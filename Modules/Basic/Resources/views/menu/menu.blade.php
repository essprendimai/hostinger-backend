@auth
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
        <ul class="app-menu">
            @foreach(\Nwidart\Modules\Facades\Module::all() as $module)
                @if ($module->enabled())
                    @if (\View::exists('modules.' . str_replace(' ', '_', $module->getAlias()) . '.backend.menu'))
                        @include('modules.' . str_replace(' ', '_', $module->getAlias()) . '.backend.menu')
                    @elseif (\View::exists($module->getAlias() . '::backend.menu'))
                        @include($module->getAlias() . '::backend.menu')
                    @endif
                @endif
            @endforeach


            @if (can_access_any([
                'backend.users.index',
                'backend.role.index',
            ]))
                <li class="treeview">
                    <a class="app-menu__item {{ active('backend.users.*') }} {{ active('backend.role.*') }}" href="#" data-toggle="treeview">
                        <i class="app-menu__icon fa fa-users"></i>
                        <span class="app-menu__label">{{ __('basic::users.user') }}</span>
                        <i class="treeview-indicator fa fa-angle-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if (can_access('backend.users.create'))
                            <li>
                                <a class="treeview-item {{ active('backend.users.create') }}" href="{{ route('backend.users.create') }}">
                                    <i class="icon fa fa-user"></i> {{ __('basic::users.menu.create_user') }}
                                </a>
                            </li>
                        @endif
                        @if (can_access('backend.users.index'))
                            <li>
                                <a class="treeview-item {{ active('backend.users.index') }}" href="{{ route('backend.users.index') }}">
                                    <i class="icon fa fa-users"></i> {{ __('basic::users.menu.users') }}
                                </a>
                            </li>
                        @endif
                        @if (can_access('backend.role.index'))
                            <li>
                                <a class="treeview-item {{ active('backend.role.index') }}" href="{{ route('backend.role.index') }}">
                                    <i class="icon fa fa-universal-access"></i> {{ __('basic::users.menu.roles') }}
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

            @if (can_access('backend.logs'))
                <li>
                    <a class="app-menu__item {{ active('backend.logs') }}" href="{{ route('backend.logs') }}">
                        <i class="app-menu__icon fa fa-bug"></i><span class="app-menu__label">{{ __('basic::logs.menu') }}</span>
                    </a>
                </li>
            @endif

            @include('basic-view::layouts.language-mobile', ['backend' => true, 'changePage' => false])
        </ul>
    </aside>
@endauth
