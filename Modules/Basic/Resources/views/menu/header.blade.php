@auth
    <!-- Navbar-->
    <header class="app-header">

        <a class="app-header__logo" href="{{ route('admin-panel.login') }}">
           <span style="display: inline;">{{ config('app.name') }}</span>
        </a>
        <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>

        <!-- Navbar Right Menu-->
        <ul class="app-nav">
            <!-- User Menu-->
            <li class="dropdown">
                <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">{{ Auth::user()->name }}&nbsp;&nbsp;&nbsp;<i class="fa fa-user fa-lg"></i></a>
                <ul class="dropdown-menu settings-menu dropdown-menu-right">
                    <li>
                        <a class="dropdown-item" href="{{ route('admin-panel.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out-alt fa-lg"></i> Logout
                        </a>

                        <form id="logout-form" action="{{ route('admin-panel.logout') }}" method="POST" style="display: none;" >
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </header>
@endauth
