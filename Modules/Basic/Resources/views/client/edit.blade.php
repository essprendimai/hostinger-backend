@extends('basic-view::layouts.page.default')

@section('content')
    <h1 class="p-1"> {{ __('basic::profile.edit_page.title') }} </h1>
    <div class="row">
        <div class="col-6">
            <form method="POST" action="{{ route('client.profile.store') }}">
                {{ csrf_field() }}

                <div class="form-group row {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="col-sm-2 col-form-label" for="password">{{ __('basic::profile.edit_page.form.password') }}</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('password_new') ? ' has-error' : '' }}">
                    <label class="col-sm-2 col-form-label" for="password">{{ __('basic::profile.edit_page.form.password_new') }}</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password_new" name="password_new" required>
                        @if ($errors->has('password_new'))
                            <span class="help-block"><strong>{{ $errors->first('password_new') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('password_new_confirmation') ? ' has-error' : '' }}">
                    <label class="col-sm-2 col-form-label" for="password">{{ __('basic::profile.edit_page.form.password_new_confirmation') }}</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password_new_confirmation" name="password_new_confirmation" required>
                        @if ($errors->has('password_new_confirmation'))
                            <span class="help-block"><strong>{{ $errors->first('password_new_confirmation') }}</strong></span>
                        @endif
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">{{ __('basic::profile.edit_page.buttons.change') }}</button>
            </form>
        </div>
    </div>
@endsection
