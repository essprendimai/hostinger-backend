@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-edit"></i> {{ __('basic::pages.categories_page.edit_title') }}
    </h1>
    <p>{{ __('basic::pages.categories_page.edit_title_description', ['attribute' => $key]) }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.categories.index') }}">{{ __('basic::pages.menu.categories') }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.categories.edit', $key)  }}">{{ $key }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                {!! BootForm::open(['route' => ['backend.categories.update', $key]]) !!}
                {{ method_field('PUT') }}
                {!! BootForm::text('key', __('basic::pages.categories_page.key'), $key, ['disabled']) !!}
                {!! BootForm::hidden('key', $key) !!}

                @foreach ($filteredCategories as $language => $category)
                    <div>
                        <img class="app-menu__icon" style="height: 20px;margin-left: -5px;" src="/images/languages/{{ $language }}.png"/> &nbsp;&nbsp;
                        {{ __(\Essprendimai\Basic\Enums\LanguageEnum::from($language)->name()) }}
                        <br/>
                        <br/>
                    </div>

                    {{ BootForm::text(
                        'title[' . $language . ']',
                        __('basic::pages.categories_page.category_title'),
                        $category ? $category->title : null,
                        ['required']
                    ) }}

                    {!! BootForm::hidden('old_url[' . $language  . ']', $category ? $category->url : null) !!}
                    {{ BootForm::text(
                        'url[' . $language  . ']',
                        __('basic::pages.categories_page.category_url'),
                        $category ? $category->url : null,
                        ['required']
                    ) }}

                    <hr/>
                @endforeach

                {!! BootForm::submit(__('basic::pages.categories_page.edit')) !!}
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection
