@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-plus"></i> {{ __('basic::pages.categories_page.new_title') }}
    </h1>
    <p>{{ __('basic::pages.categories_page.new_title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.categories.index') }}">{{ __('basic::pages.menu.categories') }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.categories.create') }}">{{ __('basic::pages.menu.new') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                {!! BootForm::open(['route' => 'backend.categories.store']) !!}
                {!! BootForm::text('key', __('basic::pages.categories_page.key'), null, ['required']) !!}

                @foreach (\Essprendimai\Basic\Services\LanguageService::getAllActiveLanguages() as $language)
                    <div>
                        <img class="app-menu__icon" style="height: 20px;margin-left: -5px;" src="/images/languages/{{ $language }}.png"/> &nbsp;&nbsp;
                        {{ __($language->name()) }}
                        <br/>
                        <br/>
                    </div>

                    {!! BootForm::text('title[' . $language->id() . ']', __('basic::pages.categories_page.category_title'), null, ['required']) !!}
                    {!! BootForm::text('url[' . $language->id() . ']', __('basic::pages.categories_page.category_url'), null, ['required']) !!}

                    <hr/>
                @endforeach

                {!! BootForm::submit(__('basic::pages.categories_page.create')) !!}
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection
