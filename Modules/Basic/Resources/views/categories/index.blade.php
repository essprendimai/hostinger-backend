@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-folder"></i> {{ __('basic::pages.categories_page.title') }} &nbsp;&nbsp;&nbsp;  <a class="btn btn-primary" href="{{ route('backend.categories.create') }}"><i class="fa fa-plus"></i> {{ __('basic::pages.categories_page.create') }}</a>
    </h1>
    <p>{{ __('basic::pages.categories_page.title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.categories.index') }}">{{ __('basic::pages.menu.categories') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                @if (isset($categories))
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>{{ __('basic::pages.categories_page.key') }}</th>
                                <th>{{ __('basic::pages.categories_page.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($categories as $category)
                                <tr>
                                    <td width="75%">{{ $category->key }}</td>
                                    <td width="25%">
                                        <a href="{{ route('backend.categories.edit', $category->key) }}" class="btn btn-sm btn-info">
                                            <i class="fas fa-edit"></i> {{ __('basic::pages.categories_page.edit') }}
                                        </a>

                                        {!! BootForm::inline(['route' => ['backend.categories.destroy', $category->key], 'method' => 'DELETE', 'style' => 'display:inline']) !!}
                                            {!! Form::button(
                                              '<i class="fa fa-trash"></i> ' . __('basic::pages.categories_page.delete'),
                                             [
                                                 'type' => 'submit',
                                                 'class' => 'btn btn-danger btn-sm',
                                                 'title' => __('basic::pages.categories_page.delete'),
                                                 'onclick'=> 'return confirm("' . __('basic::pages.categories_page.confirm_delete') . '")'
                                             ]) !!}
                                        {!! BootForm::close() !!}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">{{ __('basic::pages.categories_page.empty_list') }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        {{ $categories->render() }}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
