@push('pre.scripts')
    <script src="/js/basic-view/plugins/jquery.dataTables.min.js"></script>
    <script src="/js/basic-view/plugins/dataTables.bootstrap.min.js"></script>

    <script>
        var datatableLanguages = [];
        datatableLanguages['lt'] = {
            "sEmptyTable": "Nėra duomenų",
            "sProcessing": "Apdorojama...",
            "sLengthMenu": "Rodyti _MENU_ įrašus",
            "sZeroRecords": "Įrašų nerasta",
            "sInfo": "Rodomi įrašai nuo _START_ iki _END_ iš _TOTAL_ įrašų",
            "sInfoEmpty": "Rodomi įrašai nuo 0 iki 0 iš 0",
            "sInfoFiltered": "(atrinkta iš _MAX_ įrašų)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sSearch": '<span class="searchIcon"></span>',
            "sSearchPlaceholder": '{{ __("companies::companies.backend.search") }}',
            "sUrl": "",
            "sLoadingRecords": "Kraunama...",
            "oPaginate": {
                "sFirst": "<<",
                "sPrevious": "<",
                "sNext": ">",
                "sLast": ">>"
            }
        };
        datatableLanguages['en'] = {
            "sEmptyTable": "No data available in table",
            "sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
            "sInfoEmpty": "Showing 0 to 0 of 0 entries",
            "sInfoFiltered": "(filtered from _MAX_ total entries)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Show _MENU_ entries",
            "sLoadingRecords": "Loading...",
            "sProcessing": "Processing...",
            "sSearch": '<span class="searchIcon"></span>',
            "sSearchPlaceholder": '{{ __("companies::companies.backend.search") }}' ,
            "sZeroRecords": "No matching records found",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<"
            },
            "oAria": {
                "sSortAscending": ": activate to sort column ascending",
                "sSortDescending": ": activate to sort column descending"
            }
        };
    </script>
@endpush