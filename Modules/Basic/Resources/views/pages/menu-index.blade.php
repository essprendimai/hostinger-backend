@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-file"></i> {{ __('basic::pages.pages_page.title') }} &nbsp;&nbsp;&nbsp;  <a class="btn btn-primary" href="{{ route('backend.pages.create') }}"><i class="fa fa-plus"></i> {{ __('basic::pages.pages_page.create') }}</a>
    </h1>
    <p>{{ __('basic::pages.pages_page.title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.pages.index') }}">{{ __('basic::pages.menu.pages') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                @if (isset($pages))
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{ __('basic::pages.pages_page.key') }}</th>
                            <th>{{ __('basic::pages.pages_page.actions') }}</th>
                            <th>{{ __('basic::pages.pages_page.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($pages as $page)
                            <tr>
                                <td width="25%">{{ $page->key }} {!! $page->active ? '<span class="badge badge-primary">' . __("basic::pages.active") . '</span>' : '<span class="badge badge-warning">' . __("basic::pages.inactive") . '</span>' !!}</td>
                                <td width="50%">
                                    <div class="input-group mb-3">
                                        <input
                                            name="read_link"
                                            type="text"
                                            class="form-control clipboard-data"
                                            aria-label="Read link"
                                            aria-describedby="read-link-addon"
                                            disabled=""
                                            value="{{ get_page_link($page) }}"
                                        >
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="read-link-addon">
                                                <i class="clipboard fa fa-clipboard" style="cursor:pointer;"></i>
                                            </span>
                                        </div>
                                    </div>
                                </td>
                                <td width="25%">
                                    <a href="{{ route('backend.pages.edit', $page->key) }}" class="btn btn-sm btn-info">
                                        <i class="fas fa-edit"></i> {{ __('basic::pages.pages_page.edit') }}
                                    </a>

                                    @if ($page->active)
                                        <a class="btn btn-sm btn-warning" href="{{ route('backend.pages.change_status', [$page->id, 0]) }}">
                                            <i class="fa fa-toggle-off"></i> {{ __('basic::pages.disable') }}
                                        </a>
                                    @else
                                        <a class="btn btn-sm btn-primary" href="{{ route('backend.pages.change_status', [$page->id, 1]) }}">
                                            <i class="fa fa-toggle-on"></i> {{ __('basic::pages.activate') }}
                                        </a>
                                    @endif

                                    {!! BootForm::inline(['route' => ['backend.pages.destroy', $page->key], 'method' => 'DELETE', 'style' => 'display:inline']) !!}
                                    {!! Form::button(
                                     '<i class="fa fa-trash"></i> ' . __('basic::pages.pages_page.delete'),
                                     [
                                         'type' => 'submit',
                                         'class' => 'btn-danger btn btn-sm',
                                         'title' => __('basic::pages.pages_page.delete'),
                                         'onclick'=> 'return confirm("' . __('basic::pages.pages_page.confirm_delete') . '")'
                                     ]) !!}
                                    {!! BootForm::close() !!}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2">{{ __('basic::pages.pages_page.empty_list') }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    {{ $pages->render() }}
                @endif
            </div>
        </div>
    </div>
@endsection

@push('pre.scripts')
    <script src="/js/basic-view/clipboard.js"></script>
@endpush

@push('scripts')
    <script>
        $(function() {
            $('.clipboard').on('click', function(event){
                var copyText = event.target || event.srcElement;
                copyText = $(copyText).closest('.input-group').find('input')[0];

                copyText.disabled = false;
                copyText.select();
                copyText.disabled = true;
                document.execCommand("Copy");
            });
        });
    </script>
@endpush
