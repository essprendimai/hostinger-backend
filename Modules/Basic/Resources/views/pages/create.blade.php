@extends('basic-view::layouts.app-gate')

@push('styles')
    <link href="/css/basic-view/froala.css" rel="stylesheet">
@endpush

@section('content-title')
    <h1>
        <i class="fa fa-plus"></i> {{ __('basic::pages.pages_page.new_title') }}
    </h1>
    <p>{{ __('basic::pages.pages_page.new_title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.pages.index') }}">{{ __('basic::pages.menu.pages') }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.pages.create') }}">{{ __('basic::pages.menu.new') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                {!! BootForm::open(['route' => 'backend.pages.store', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
                {!! BootForm::text('key', __('basic::pages.pages_page.key'), null, ['required']) !!}

                <hr/>

                @foreach (\Essprendimai\Basic\Services\LanguageService::getAllActiveLanguages() as $language)
                    <div>
                        <img class="app-menu__icon" style="height: 20px;margin-left: -5px;" src="/images/languages/{{ $language }}.png"/> &nbsp;&nbsp;
                        {{ __($language->name()) }}
                        <br/>
                        <br/>
                    </div>

                    {!! BootForm::text('title[' . $language->id() . ']', __('basic::pages.pages_page.page_title'), null) !!}
                    {!! BootForm::text('url[' . $language->id() . ']', __('basic::pages.pages_page.page_url'), null) !!}

                    <div class="animated-checkbox">
                        <label>
                            <input type="checkbox" name="external_url[{{ $language->id() }}]">
                            <span class="label-text">{{ __('basic::pages.pages_page.external_url') }}</span>
                        </label>
                    </div>

                    <div class="animated-checkbox">
                        <label>
                            <input type="checkbox" name="is_template[{{ $language->id() }}]">
                            <span class="label-text">{{ __('basic::pages.pages_page.is_template') }}</span>
                        </label>
                    </div>

                    {!! BootForm::file('featured_image[' . $language->id() . ']', __('basic::pages.pages_page.featured_image'), ['multiple' => true, 'class' => 'form-control']) !!}

                    <label>{{ __('basic::pages.pages_page.menu_id') }}</label>
                    @foreach ($menuGroups as $menuGroup)
                        <div class="animated-checkbox">
                            <label>
                                <input type="checkbox" name="menu[{{ $language->id() }}][]" value="{{ $menuGroup->id }}">
                                <span class="label-text">{{ $menuGroup->title }}</span>
                            </label>
                        </div>
                    @endforeach

                    {!! BootForm::select('category[' . $language->id() . ']', __('basic::pages.pages_page.category_id'), $categoryByLanguages[$language->id()] ?? [0 => '-']) !!}

                    <br/>

                    {!! BootForm::textarea('text[' . $language->id() . ']', __('basic::pages.pages_page.page_text'), null) !!}

                    <div class="toggle">
                        <label>
                            <input type="checkbox" name="active[{{ $language->id() }}]" value="0"><span class="button-indecator">{{  __('basic::pages.pages_page.active') }}</span>
                        </label>
                    </div>

                    <hr/>
                @endforeach

                {!! BootForm::submit(__('basic::pages.pages_page.create')) !!}
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('pre.scripts')
    <script src="/js/basic-view/froala.js"></script>
@endpush

@push('scripts')
    <script>
        $(function() {
            $('textarea').froalaEditor()
        });
    </script>
@endpush
