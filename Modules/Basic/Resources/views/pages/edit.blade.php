@extends('basic-view::layouts.app-gate')

@push('styles')
    <link href="/css/basic-view/froala.css" rel="stylesheet">
@endpush

@section('content-title')
    <h1>
        <i class="fa fa-edit"></i> {{ __('basic::pages.pages_page.edit_title') }}
    </h1>
    <p>{{ __('basic::pages.pages_page.edit_title', ['attribute' => $key]) }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.pages.index') }}">{{ __('basic::pages.menu.pages') }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.pages.edit', $key)  }}">{{ $key }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                {!! BootForm::open(['route' => ['backend.pages.update', $key], 'enctype' => 'multipart/form-data', 'files' => true]) !!}
                {{ method_field('PUT') }}
                {!! BootForm::text('key', __('basic::pages.pages_page.key'), $key, ['disabled']) !!}
                {!! BootForm::hidden('key', $key) !!}

                <hr/>

                @foreach ($filteredPages as $language => $page)
                    <div>
                        <img class="app-menu__icon" style="height: 20px;margin-left: -5px;" src="/images/languages/{{ $language }}.png"/> &nbsp;&nbsp;
                        {{ __(\Essprendimai\Basic\Enums\LanguageEnum::from($language)->name()) }}
                        <br/>
                        <br/>
                    </div>

                    {{ BootForm::text(
                        'title[' . $language . ']',
                        __('basic::pages.pages_page.page_title'),
                        $page ? $page->title : null
                    ) }}

                    {!! BootForm::hidden('old_url[' . $language  . ']', $page ? $page->url : null) !!}
                    {{ BootForm::text(
                        'url[' . $language  . ']',
                        __('basic::pages.pages_page.page_url'),
                        $page ? $page->url : null
                    ) }}

                    <div class="animated-checkbox">
                        <label>
                            <input type="checkbox" name="external_url[{{ $language }}]" {{ $page && $page->external_url ? 'checked' : '' }}>
                            <span class="label-text">{{ __('basic::pages.pages_page.external_url') }}</span>
                        </label>
                    </div>

                    <div class="animated-checkbox">
                        <label>
                            <input type="checkbox" name="is_template[{{ $language }}]" {{ $page && $page->is_template ? 'checked' : '' }}>
                            <span class="label-text">{{ __('basic::pages.pages_page.is_template') }}</span>
                        </label>
                    </div>

                    @if ($page && $page->featured_image)
                        <div class="row">
                            <div class="col-1">
                                <img width="100%" src="{{ $page->featured_image }}"/>
                            </div>
                            <div class="col-11">
                                {!! BootForm::file('featured_image[' . $language . ']', __('basic::pages.pages_page.featured_image'), ['multiple' => true, 'class' => 'form-control']) !!}
                            </div>
                        </div>
                    @else
                        {!! BootForm::file('featured_image[' . $language . ']', __('basic::pages.pages_page.featured_image'), ['multiple' => true, 'class' => 'form-control']) !!}
                    @endif

                    <label>{{ __('basic::pages.pages_page.menu_id') }}</label>
                    @foreach ($menuGroups as $menuGroup)
                        <div class="animated-checkbox">
                            <label>
                                <input type="checkbox" name="menu[{{ $language }}][]" value="{{ $menuGroup->id }}" {{ $page && $page->menuGroups->where('key', $menuGroup->key)->isNotEmpty() ? 'checked' : ''}}>
                                <span class="label-text">{{ $menuGroup->title }}</span>
                            </label>
                        </div>
                    @endforeach

                    {!! BootForm::select(
                        'category[' . $language . ']',
                         __('basic::pages.pages_page.category_id'),
                         $categoryByLanguages[$language] ?? [0 => '-'],
                         $page && $page->category_id ? $page->category_id : 0
                    ) !!}

                    {!! BootForm::textarea(
                        'text[' . $language . ']',
                        __('basic::pages.pages_page.page_text'),
                         $page->text ?? ''
                     ) !!}

                    <div class="toggle">
                        <label>
                            <input type="checkbox" name="active[{{ $language }}]" {{ $page && $page->active ? 'checked' : ''}}><span class="button-indecator">{{  __('basic::pages.pages_page.active') }}</span>
                        </label>
                    </div>

                    <div class="input-group mb-3">
                        <input
                            name="read_link"
                            type="text"
                            class="form-control clipboard-data"
                            aria-label="Read link"
                            aria-describedby="read-link-addon"
                            disabled=""
                            value="{{ get_page_link($page) }}"
                        >
                        <div class="input-group-append">
                        <span class="input-group-text" id="read-link-addon">
                            <i class="clipboard fa fa-clipboard" style="cursor:pointer;"></i>
                        </span>
                        </div>
                    </div>

                    <hr/>
                @endforeach

                {!! BootForm::submit(__('basic::pages.pages_page.edit')) !!}
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('pre.scripts')
    <script src="/js/basic-view/froala.js"></script>
    <script src="/js/basic-view/clipboard.js"></script>
@endpush

@push('scripts')
    <script>
        $(function() {
            $('textarea').froalaEditor();

            $('.clipboard').on('click', function(event){
                var copyText = event.target || event.srcElement;
                copyText = $(copyText).closest('.input-group').find('input')[0];

                copyText.disabled = false;
                copyText.select();
                copyText.disabled = true;
                document.execCommand("Copy");
            });
        });
    </script>
@endpush
