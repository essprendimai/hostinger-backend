@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-user"></i> {{ __('basic::users.change_page.title') }}: {{ $user->name }}
    </h1>
    <p>{{ __('basic::users.change_page.title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.users.index') }}">{{ __('basic::users.menu.users') }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.users.show', ['id' => $user->id]) }}">{{ $user->name }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                {!! BootForm::open(['model' => $user, 'update' => 'backend.users.update']) !!}
                {!! method_field('PUT') !!}
                {!! BootForm::text('name', __('basic::users.change_page.field_name'), $user->name) !!}
                {!! BootForm::email('email', __('basic::users.change_page.field_email'), $user->email) !!}
                {!! BootForm::password('password', __('basic::users.change_page.field_password')) !!}
                {!! BootForm::checkbox('change_password', __('basic::users.change_page.field_force_password_change')) !!}
                {!! BootForm::checkbox('is_admin', __('basic::users.change_page.is_admin')) !!}
                <label for="">{{ __('basic::users.change_page.field_roles') }}</label>
                <div class="panel-body in">
                    @foreach($roles as $role)
                        <div class="from-group">
                            <div class="toggle-box d-inline">
                                <input class="tgl tgl-light" id="role-{{ $role->id }}" name="roles[{{ $role->id }}]"
                                       value="{{ $role->id }}"
                                       type="checkbox" {{ $user->roles->contains($role) ? 'checked' : '' }}/>
                                <label class="tgl-btn" for="role-{{ $role->id }}"></label>
                            </div>
                            <label for="role-{{ $role->id }}">{{ $role->name }}</label>
                            @if(trim($role->description))
                                <small>{{ $role->description }}</small>
                            @endif
                        </div>
                    @endforeach
                </div>
                {!! BootForm::submit(__('basic::users.change_page.button_update')) !!}
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@stop
