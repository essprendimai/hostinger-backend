@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-user"></i> {{ __('basic::users.create_page.title') }}
    </h1>
    <p>{{ __('basic::users.create_page.title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.users.index') }}">{{ __('basic::users.menu.create_user') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                {!! BootForm::open(['route' => 'backend.users.store']) !!}
                    {!! BootForm::text('name', __('basic::users.users_page.name')) !!}
                    {!! BootForm::email('email', __('basic::users.users_page.email')) !!}
                    {!! BootForm::password('password', __('basic::users.users_page.password')) !!}
                    {!! BootForm::checkbox('change_password', __('basic::users.users_page.force_password_change')) !!}
                    {!! BootForm::checkbox('is_admin', __('basic::users.change_page.is_admin')) !!}
                    <label for="">{{ __('basic::users.users_page.roles') }}</label>
                    <div class="panel-body in">
                        @foreach($roles as $role)
                            <div class="from-group">
                                <div class="toggle-box d-inline">
                                    <input class="tgl tgl-light" id="role-{{ $role->id }}" name="roles[{{ $role->id }}]"
                                           value="{{ $role->id }}" type="checkbox"/>
                                    <label class="tgl-btn" for="role-{{ $role->id }}"></label>
                                    <label for="role-{{ $role->id }}">{{ $role->name }}</label>
                                    @if(trim($role->description))
                                        <small>{{ $role->description }}</small>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {!! BootForm::submit(__('basic::users.users_page.create')) !!}
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@stop
