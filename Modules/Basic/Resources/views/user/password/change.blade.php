@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-lock"></i> {{ __('basic::users.change_password.title') }}
    </h1>
    <p>{{ __('basic::users.change_password.title_description') }}</p>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                {!! BootForm::open(['update' => 'backend.change-password.post']) !!}

                {!! BootForm::password('old_password') !!}
                {!! BootForm::password('new_password') !!}
                {!! BootForm::password('new_password_confirmation') !!}

                {!! BootForm::submit('Change Password') !!}

                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@stop
