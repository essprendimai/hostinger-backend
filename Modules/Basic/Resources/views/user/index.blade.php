@extends('basic-view::layouts.app-gate')
@include('basic-view::includes.datatable')

@section('content-title')
    <h1>
        <i class="fa fa-users"></i> {{ __('basic::users.users_page.title') }}
    </h1>
    <p>{{ __('basic::users.users_page.sub_title') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.users.index') }}">{{ __('basic::users.menu.users') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                @if (isset($users))
                    <div class="table-responsive">
                        <table class="table table-bordered" id="users-data-table">
                            <thead id="users-data-table-thead">
                                <th>
                                    <div class="animated-checkbox">
                                        <label>
                                            <input id="check-all" type="checkbox"/>
                                            <span class="label-text"></span>
                                        </label>
                                    </div>
                                </th>
                                <th>{{ __('basic::users.users_page.id') }}</th>
                                <th>{{ __('basic::users.users_page.name') }}</th>
                                <th>{{ __('basic::users.users_page.groups') }}</th>
                                <th>{{ __('basic::users.users_page.roles') }}</th>
                                <th>{{ __('basic::users.users_page.badges') }}</th>
                                <th>{{ __('basic::users.users_page.actions') }}</th>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td width="1%">
                                        <div class="animated-checkbox">
                                            <label>
                                                <input type="checkbox" name="selected_user_groups[]" value="{{ $user->id }}" class="user-groups-row-checkbox"/>
                                                <span class="label-text"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="user-id">
                                        {{ $user->id }}
                                    </td>
                                    <td class="email">
                                        {{ $user->email }}
                                    </td>
                                    <td>
                                        @foreach ($user->groupGroups as $group)
                                            <span class="badge badge-warning">
                                                {{ __("basic::users.users_page.group") }}: {{ $group->title }}
                                                <a href="{{ route('backend.users.remove-user-from-group', [$user->id, $group->id]) }}" class="text-danger">
                                                    <i class="fa fa-remove"></i>
                                                </a>
                                            </span>
                                        @endforeach
                                    </td>
                                    <td>
                                        {{ $user->roles->pluck('name')->implode(', ') }}
                                    </td>
                                    <td>
                                        {!! $user->is_admin ? '<span class="badge badge-danger">' . __("basic::users.is_admin_types.admin") . '</span>' : '<span class="badge badge-primary">' . __("basic::users.is_admin_types.client") . '</span>' !!}
                                        {!! $user->hasVerifiedEmail() ? '<span class="badge badge-success">' . __("basic::users.verified") . '</span>' : '' !!}
                                        {!! $user->is_draft ? '<span class="badge badge-secondary">' . __("basic::users.draft") . '</span>' : '' !!}
                                        {!! $user->created_as_draft ? '<span class="badge badge-info">' . __("basic::users.snx_ee") . '</span>' : '' !!}
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-info" href="{{ route('backend.users.show', ['id' => $user->id]) }}">
                                            <i class="fa fa-edit"></i> {{ __('basic::users.buttons.edit') }}
                                        </a>
                                        <a class="btn-danger btn btn-sm" href="{{ route('backend.users.show', ['id' => $user->id]) }}" onclick="return confirmBeforeSubmit('delete-form-{{ $user->id }}', '{{ __('basic::users.users_page.confirm_delete') }}');">
                                            <i class="fa fa-trash"></i> {{ __('basic::users.users_page.delete') }}
                                        </a>
                                        @if (!$user->hasVerifiedEmail())
                                            @if ($user->is_draft)
                                                <a class="btn btn-sm btn-warning" href="{{ route('backend.send-draft-user-verification', ['id' => $user->id]) }}">
                                                    <i class="fa fa-mail-bulk"></i> {{ __('basic::users.buttons.verify_draft_user') }}
                                                </a>
                                            @else
                                                <a class="btn btn-sm btn-warning" href="{{ route('backend.send-verification', ['id' => $user->id]) }}">
                                                    <i class="fa fa-mail-bulk"></i> {{ __('basic::users.buttons.verify') }}
                                                </a>
                                            @endif
                                        @endif

                                        {!! BootForm::open(['route' => ['backend.users.destroy', $user->id], 'method'=> 'DELETE', 'id' => 'delete-form-' . $user->id]) !!}
                                        {!! BootForm::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th></th>
                                <th data-type="email">{{ __('basic::users.users_page.id') }}</th>
                                <th data-type="email">{{ __('basic::users.users_page.name') }}</th>
                                <th data-type="email">{{ __('basic::users.users_page.groups') }}</th>
                                <th>{{ __('basic::users.users_page.roles') }}</th>
                                <th data-type="is_admin">{{ __('basic::users.users_page.is_admin') }}</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>

                        <hr/>

                        <button id="send-message" class="btn btn-sm btn-warning"><i class="fa fa-mail-bulk"></i> {{ __('basic::users.buttons.send_message') }}</button>
                        <button id="send-message-group" class="btn btn-sm btn-warning"><i class="fa fa-mail-bulk"></i> {{ __('basic::users.buttons.send_message_group') }}</button>
                        <button id="add-to-group" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> {{ __('basic::users.buttons.add_to_group') }}</button>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Send Message Modal -->
    <div class="modal fade" id="send-message-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('basic::users.buttons.send_message') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span>{{ __('basic::users.send_message_modal.selected_users') }}</span>
                    <div id="send-message-modal-users-info"></div>

                    <hr/>

                    {!! BootForm::open(['route' => ['backend.users.send-message-to-users'], 'id' => 'message-modal-users-form']) !!}
                        {!! BootForm::textarea('message', __('basic::users.send_message_modal.message'), null) !!}
                    {!! BootForm::close() !!}
                </div>
                <div class="modal-footer">
                    <button id="message-modal-users-form-submit" type="button" class="btn btn-primary">{{ __('basic::users.buttons.send') }}</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Send Message Group Modal -->
    <div class="modal fade" id="send-message-group-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('basic::users.buttons.send_message_group') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! BootForm::open(['route' => ['backend.users.send-message-to-users-from-group'], 'id' => 'message-modal-group-form']) !!}
                        {!! BootForm::textarea('message', __('basic::users.send_message_group_modal.message'), null) !!}

                        <label>{{ __('basic::users.send_message_group_modal.group') }}</label>
                        <select class="form-control" name="group">
                            @foreach ($menuGroups as $menuGroup)
                                @if ($menuGroup->key !== \Essprendimai\Basic\Entities\Group::ALL_USERS)
                                    @continue;
                                @endif
                                <option value="{{ $menuGroup->id }}">{{ $menuGroup->title }}</option>
                            @endforeach
                        </select>
                    {!! BootForm::close() !!}
                </div>
                <div class="modal-footer">
                    <button id="message-group-modal-form-submit" type="button" class="btn btn-primary">{{ __('basic::users.buttons.send') }}</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Add To Group Modal -->
    <div class="modal fade" id="add-to-group-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('basic::users.buttons.add_to_group') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span>{{ __('basic::users.add_user_to_group_modal.selected_users') }}</span>
                    <div id="add-to-group-modal-users-info"></div>

                    <hr/>

                    {!! BootForm::open(['route' => ['backend.users.add-users-to-group'], 'id' => 'add-to-group-modal-form']) !!}
                        <label>{{ __('basic::pages.pages_page.group_id') }}</label>
                        @foreach ($menuGroups as $menuGroup)
                            @if ($menuGroup->key !== \Essprendimai\Basic\Entities\Group::ALL_USERS)
                                @continue;
                            @endif
                            <div class="animated-checkbox">
                                <label>
                                    <input type="checkbox" name="group_ids[]" value="{{ $menuGroup->id }}">
                                    <span class="label-text">{{ $menuGroup->title }}</span>
                                </label>
                            </div>
                        @endforeach
                    {!! BootForm::close() !!}
                </div>
                <div class="modal-footer">
                    <button id="add-to-group-modal-submit" type="button" class="btn btn-primary">{{ __('basic::users.buttons.add') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script type="text/javascript">
        initUserIndexDatatable({
            'admin_type': '{{ __("basic::users.is_admin_types.admin") }}',
            'client_type': '{{ __("basic::users.is_admin_types.client") }}',
            'search': '{{ __("basic::users.search") }}',
            'verified': '{{ __("basic::users.verified") }}',
            'draft': '{{ __("basic::users.draft") }}',
            'snx_ee': '{{ __("basic::users.snx_ee") }}',
        });

        var userList = UserList();
        userList.init({
            'translations': {
                'error_title': '{{ __("basic::users.error_title") }}',
                'error_close_button': '{{ __("basic::users.error_close_button") }}',
                'error_select_user': '{{ __("basic::users.error_select_user") }}',
            },
        });
    </script>
@endpush
