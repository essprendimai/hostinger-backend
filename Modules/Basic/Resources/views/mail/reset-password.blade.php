@component('mail::message')
<h1>{{ $title }}</h1>

@component('mail::button', ['url' => $url])
{{ $reset_button }}
@endcomponent

<small><i>{{ $footer }}</i></small>
@endcomponent
