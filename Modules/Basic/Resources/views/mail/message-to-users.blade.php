@component('mail::message')
<p>{{ __('basic::mail.send_message_to_users.labels.message') }}: {{ $message }}</p>
@endcomponent
