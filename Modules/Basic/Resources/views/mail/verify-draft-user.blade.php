@component('mail::message')
<p>{{ __('basic::mail.verify_draft_user_email.labels.please_click') }}</p>
<p>{{ __('basic::mail.verify_draft_user_email.labels.password') }} {{ $password }}</p>
<p style="text-align: center;">
    <a href="{{ $button_url }}"
       style="color:#FFF; display:inline-block; text-decoration:none; background-color:#3097D1; padding: 10px 18px;"
       target="_blank"
       rel="noopener noreferrer">{{ __('basic::mail.verify_draft_user_email.labels.button_text') }}</a>
</p>
<p>{{ __('basic::mail.verify_draft_user_email.labels.if_you_not_create_ignore') }}</p>
@endcomponent
