@component('mail::message')
<h2>{{ $title }}</h2>

@component('mail::button', ['url' => $url])
{{ $activate }}
@endcomponent

<small><i>{{ $footer }}</i></small>
@endcomponent
