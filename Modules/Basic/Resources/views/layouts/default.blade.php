<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>@yield('title')</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicon-16x16.png">
    <link rel="shortcut icon" href="/images/icons/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="/js/basic-view/page/default.js"></script>

    <link href="/css/basic-view/page/default.css" rel="stylesheet">

@stack('styles')

<!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <style>
        .app-content {
            margin-left: 0px;
        }

        .app-header__logo {
            flex: 0 0 auto;
            display: block;
            width: 300px;
            background-color: #007d71;
        }
    </style>
</head>
<body id="body" class="app sidebar-mini rtl">
<div id="app" class="bg-light">
    <header class="app-header">
        <a class="app-header__logo" href="{{ route('admin-panel.login') }}">
            <img src="{!! '/images/deh_ic_launcher.png' !!}" height="25" style="display: inline;"/> <span style="display: inline;">{!! config('app.name') !!}</span>
            &nbsp;&nbsp;and&nbsp;&nbsp;
            <img src="{!! '/images/hfbc_ic_launcher.png' !!}" height="25" style="display: inline;"/> <span style="display: inline;">DEH</span>
        </a>

        @include('basic-view::layouts.language-mobile-page', ['backend' => false, 'changePage' => false])
    </header>

    <main class="app-content">
        @yield('content')
    </main>
</div>

@include('basic-view::layouts.language-right', ['backend' => false, 'changePage' => false])

@yield('pre.scripts')
@yield('scripts')
@yield('post.scripts')
</body>
</html>
