@foreach (\Essprendimai\Basic\Services\LanguageService::getAllActiveLanguages() as $activeLanguage)
    <a style="padding-right: 20px;padding-top:10px; float: left;" class="d-inline d-lg-none treeview-item {{ $activeLanguage->id() === App::getLocale() ? 'active' : '' }}" href="{{ route( $backend ? 'language.change-backend' : 'language.change', ['language' => $activeLanguage->id()]) }}">
        <img style="height: 30px;margin-left: -5px;" src="/images/languages/{{ $activeLanguage->id() }}.png"/> &nbsp;&nbsp;{{ __($activeLanguage->name()) }}
    </a>
@endforeach
