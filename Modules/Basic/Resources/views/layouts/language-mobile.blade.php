<li class="treeview">
    <a class="app-menu__item" href="#" data-toggle="treeview">
        <img class="app-menu__icon" style="height: 20px;margin-left: -5px;" src="/images/languages/{{ $language = \Essprendimai\Basic\Services\LanguageService::getActiveLanguage()->id() }}.png"/>
        <span class="app-menu__label">&nbsp;&nbsp;{{ __('basic::language.language') }}</span>
        <i class="treeview-indicator fa fa-angle-right"></i>
    </a>
    <ul class="treeview-menu">
        @foreach (\Essprendimai\Basic\Services\LanguageService::getAllActiveLanguages() as $activeLanguage)
            <li>
                <a class="treeview-item {{ $activeLanguage->id() === App::getLocale() ? 'active' : '' }}" href="{{ route( $backend ? 'language.change-backend' : 'language.change', ['language' => $activeLanguage->id()]) }}">
                    <img style="height: 20px;margin-left: -5px;" src="/images/languages/{{ $activeLanguage->id() }}.png"/> &nbsp;&nbsp;{{ __($activeLanguage->name()) }}
                </a>
            </li>
        @endforeach
    </ul>
</li>
