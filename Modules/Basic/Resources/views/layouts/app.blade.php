<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="bg-light">
<head>
    <title>{{ config('app.name') }}</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/icons/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="/js/basic-view/basic.js"></script>

    <link href="/css/basic-view/basic.css" rel="stylesheet">

    @stack('styles')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body id="body" class="app sidebar-mini rtl">
    <div id="app" class="bg-light">
        @include('basic-view::menu.header')
        @include('basic-view::menu.menu')

        @auth
            <main class="app-content">
                <div class="app-title">
                    <div style="width: 100%;">
                        @yield('content-title')
                    </div>

                    <br/>

                    <ul class="app-breadcrumb breadcrumb">
                        @yield('content-location')
                    </ul>
                </div>

                @if ((isset($messages) && $messages) || !isset($messages))
                    @include('basic-view::messages')
                @endif

                @yield('content')
            </main>
        @endauth

        @guest
            @yield('content')
        @endguest
    </div>

    @stack('pre.scripts')
    @stack('scripts')
    @stack('post.scripts')

    <script>
        window.onload = function () {
            window.Layout && window.Layout.load();
        };
    </script>
</body>
</html>
