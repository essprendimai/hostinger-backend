<div class="d-none d-lg-block language-right-fixed">
    <ul class="nav navbar-nav navbar-right">
        <li class="nav-item dropdown">
            <a href="#" class="btn btn-light nav-link dropdown-toggle" id="language-desktop-nav-bar" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 7px;">
                <img height="25" style="display: inline;" src="/images/languages/{{ $language = \Essprendimai\Basic\Services\LanguageService::getActiveLanguage()->id() }}.png"/>
            </a>
            <div class="dropdown-menu fixed-language-select" aria-labelledby="language-desktop-nav-bar">
                @foreach (\Essprendimai\Basic\Services\LanguageService::getAllActiveLanguages() as $activeLanguage)
                    <a class="dropdown-item" href="{{ route( $backend ? 'language.change-backend' : 'language.change', ['language' => $activeLanguage->id()]) }}">
                        <img height="25" style="display: inline;" src="/images/languages/{{ $activeLanguage->id() }}.png"/>
                        <span>&nbsp;&nbsp;{{ __($activeLanguage->name()) }}</span>
                    </a>
                @endforeach
            </div>
        </li>
    </ul>
</div>
