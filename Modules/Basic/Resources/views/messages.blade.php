@if (
        (isset($errors) && count($errors)) ||
        isset($success) ||
        session('success') ||
        isset($danger) ||
        session('danger') ||
        isset($info) ||
        session('info') ||
        isset($warning) ||
        session('warning')
)
<div class="@if(isset($headerDark) && !$headerDark) alert-header-absolute @endif">
    <div class="row">
        <div class="col-md-12 mb-5">
            @if (isset($errors) && count($errors))
                <div class="alert alert-danger">
                    <ul class="mb-0">
                        @foreach ($errors->all() as $error)
                            <li class="list-unstyled mb-0">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <button type="button" class="vertical-center height-25" data-dismiss="alert">
                    <i class="fas fa-times-circle"></i>
                </button>
            @endif

            @if(isset($success))
                <div class="alert alert-success" role="alert">
                    {{ $success }}
                    <button type="button" class="vertical-center height-25" data-dismiss="alert">
                        <i class="fas fa-times-circle"></i>
                    </button>
                </div>
            @endif

            @if(session('success'))
                <div class="alert alert-success" role="alert">{{ session('success') }}
                    <button type="button" class="vertical-center height-25" data-dismiss="alert">
                        <i class="fas fa-times-circle"></i>
                    </button>
                </div>
            @endif

            @if(isset($danger))
                <div class="alert alert-danger" role="alert">{{ $danger }}
                    <button type="button" class="vertical-center height-25" data-dismiss="alert">
                        <i class="fas fa-times-circle"></i>
                    </button>
                </div>
            @endif

            @if(session('danger'))
                <div class="alert alert-danger" role="alert">{{ session('danger') }}
                    <button type="button" class="vertical-center height-25" data-dismiss="alert">
                        <i class="fas fa-times-circle"></i>
                    </button>
                </div>
            @endif

            @if(isset($info))
                <div class="alert alert-info" role="alert">{{ $info }}
                    <button type="button" class="vertical-center height-25" data-dismiss="alert">
                        <i class="fas fa-times-circle"></i>
                    </button>
                </div>
            @endif

            @if(session('info'))
                <div class="alert alert-info" role="alert">{{ session('info') }}
                    <button type="button" class="vertical-center height-25" data-dismiss="alert">
                        <i class="fas fa-times-circle"></i>
                    </button>
                </div>
            @endif

            @if(isset($warning))
                <div class="alert alert-warning" role="alert">{{ $warning }}
                    <button type="button" class="vertical-center height-25" data-dismiss="alert">
                        <i class="fas fa-times-circle"></i>
                    </button>
                </div>
            @endif

            @if(session('warning'))
                <div class="alert alert-warning" role="alert">{{ session('warning') }}
                    <button type="button" class="vertical-center height-25" data-dismiss="alert">
                        <i class="fas fa-times-circle"></i>
                    </button>
                </div>
            @endif
        </div>
    </div>
</div>
@endif

