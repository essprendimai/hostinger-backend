@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-edit"></i> {{ __('basic::group.group_page.edit_title') }}
    </h1>
    <p>{{ __('basic::pages.categories_page.edit_title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.groups.index') }}">{{ __('basic::group.menu_title') }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.groups.edit', $group->id)  }}">{{ $group->title }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                {!! BootForm::open(['route' => ['backend.groups.update', $group->id]]) !!}
                    {{ method_field('PUT') }}
                    {!! BootForm::hidden('id', $group->id) !!}
                    {!! BootForm::text('title', __('basic::group.group_page.form.title'), $group->title) !!}

                    <label>{{ __('basic::group.group_page.form.type') }}</label>
                    {!! \Essprendimai\Basic\Enums\GroupsTypesEnum::toHtmlSelectList('type', $group->type, null, 'form-control', false, null, true)  !!}

                    <br/>

                    {!! BootForm::submit(__('basic::group.buttons.edit')) !!}
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection
