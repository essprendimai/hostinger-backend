@extends('basic-view::layouts.app-gate')

@section('content-title')
    <h1>
        <i class="fa fa-plus"></i> {{ __('basic::group.group_page.create') }}
    </h1>
    <p>{{ __('basic::group.group_page.create_title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.groups.index') }}">{{ __('basic::group.menu_title') }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.groups.create') }}">{{ __('basic::group.group_page.create') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                {!! BootForm::open(['route' => 'backend.groups.store']) !!}
                    {!! BootForm::text('title', __('basic::group.group_page.form.title'), null) !!}

                    <label>{{ __('basic::group.group_page.form.type') }}</label>
                    {!! \Essprendimai\Basic\Enums\GroupsTypesEnum::toHtmlSelectList('type', null, null, 'form-control', false, null, true)  !!}

                    <br/>

                    {!! BootForm::submit(__('basic::group.group_page.create')) !!}
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection
