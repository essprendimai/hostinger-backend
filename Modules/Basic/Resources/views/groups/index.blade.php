@extends('basic-view::layouts.app-gate')
@include('basic-view::includes.datatable')

@section('content-title')
    <h1>
        <i class="fa fa-folder"></i> {{ __('basic::group.group_page.title') }} &nbsp;&nbsp;&nbsp;  <a class="btn btn-primary" href="{{ route('backend.groups.create') }}"><i class="fa fa-plus"></i> {{ __('basic::group.group_page.create') }}</a>
    </h1>
    <p>{{ __('basic::group.group_page.title_description') }}</p>
@endsection

@section('content-location')
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="{{ route('backend.groups.index') }}">{{ __('basic::group.menu_title') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                @if (isset($groups))
                    <div class="table-responsive">
                        <table class="table table-bordered" id="groups-data-table">
                            <thead id="groups-data-table-thead">
                                <th>{{ __('basic::group.group_page.form.key') }}</th>
                                <th>{{ __('basic::group.group_page.form.title') }}</th>
                                <th>{{ __('basic::group.group_page.form.type') }}</th>
                                <th>{{ __('basic::group.group_page.form.actions') }}</th>
                            </thead>
                            <tbody>
                            @foreach ($groups as $group)
                                <tr>
                                    <td>
                                        {{ $group->key }}
                                    </td>
                                    <td>
                                        {{ $group->title }}
                                    </td>
                                    <td>
                                        {{ __(\Essprendimai\Basic\Enums\GroupsTypesEnum::from($group->type)->name()) }}
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-info" href="{{ route('backend.groups.edit', ['id' => $group->id]) }}">
                                            <i class="fa fa-edit"></i> {{ __('basic::group.buttons.edit') }}
                                        </a>
                                        <a class="btn-danger btn btn-sm" href="{{ route('backend.groups.destroy', ['id' => $group->id]) }}" onclick="return confirmBeforeSubmit('delete-form-{{ $group->id }}', '{{ __('basic::group.buttons.confirm_delete') }}');">
                                            <i class="fa fa-trash"></i> {{ __('basic::group.buttons.delete') }}
                                        </a>

                                        {!! BootForm::open(['route' => ['backend.groups.destroy', $group->id], 'method'=> 'DELETE', 'id' => 'delete-form-' . $group->id]) !!}
                                        {!! BootForm::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ __('basic::group.group_page.form.key') }}</th>
                                <th>{{ __('basic::group.group_page.form.title') }}</th>
                                <th data-type="type">{{ __('basic::group.group_page.form.type') }}</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        initGroupsIndexDatatable({
            "page_category": "{{ __('basic::group.page_category') }}",
            "group": "{{ __('basic::group.group') }}",
            "menu": "{{ __('basic::group.menu') }}",
            "search": "{{ __('basic::group.search') }}",
        });
    </script>
@endpush

