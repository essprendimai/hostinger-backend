<?php

declare(strict_types=1);

namespace Essprendimai\Basic\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection AS SupportCollection;
use Essprendimai\Basic\DTO\MenuDTO;
use Essprendimai\Basic\Entities\Group;
use Essprendimai\Basic\Enums\GroupsRelationsTypesEnum;
use Essprendimai\Basic\Enums\GroupsTypesEnum;
use Essprendimai\Basic\Enums\LanguageEnum;
use Essprendimai\Basic\Enums\PageGroupsTypesEnum;
use Essprendimai\Basic\Services\LanguageService;

/**
 * Class GroupRepository
 * @package Essprendimai\Basic\Repositories
 */
class GroupRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Group::class;
    }

    /**
     * @param string $type
     * @return Collection
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function getGroupsByType(string $type): Collection
    {
        return $this->makeQuery()
            ->where([
                'type' => $type,
            ])
            ->get();
    }

    /**
     * @param string $key
     * @param string $type
     * @return Model
     */
    public function getGroupByTypeAndKey(string $key, string $type): Model
    {
        return $this->makeQuery()
            ->where([
                'key' => $key,
                'type' => $type,
            ])
            ->first();
    }

    /**
     * @param string $type
     * @return SupportCollection
     */
    public function getMenus(string $type): SupportCollection
    {
        return $this->makeQuery()
            ->select([
                'pages.title AS pageTitle',
                'pages.url AS pageUrl',
                'pages.language AS language',
                'pages.external_url AS externalUrl',
                'categories.url AS categoryUrl',
                'pages.text AS content',
                'pages.featured_image as featured_image'
            ])
            ->where([
                'groups.type' => PageGroupsTypesEnum::menu()->id(),
                'groups.key' => $type,
                'group_relations.type' => GroupsRelationsTypesEnum::page()->id(),
                'pages.language' => LanguageService::getActiveLanguage()->id(),
                'pages.active' => true,
            ])
            ->join('group_relations', 'group_relations.group_id', '=', 'groups.id')
            ->join('pages', 'pages.id', '=', 'group_relations.relation_id')
            ->leftJoin('categories', 'categories.id', '=', 'pages.category_id')
            ->get()
            ->map(function(Group $group) use ($type): MenuDTO {
                return (new MenuDTO())
                    ->setTitle((string)$group->pageTitle)
                    ->setPageUrl((string)$group->pageUrl)
                    ->setCategoryUrl((string)$group->categoryUrl)
                    ->setIsExternalLink((bool)$group->externalUrl)
                    ->setLanguage(LanguageEnum::from($group->language))
                    ->setMenuType($type)
                    ->setContent($group->content)
                    ->setFeaturedImage((string)$group->featured_image);
            });
    }
}
