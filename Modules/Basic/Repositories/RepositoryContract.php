<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Repositories;

use Illuminate\Database\Eloquent\Builder;
use RuntimeException;

/**
 * Interface RepositoryContract
 * @package Essprendimai\Basic\Repositories
 */
interface RepositoryContract
{
    /**
     * @return string
     */
    public function model(): string;

    /**
     * @return Builder
     * @throws RuntimeException
     */
    public function makeQuery(): Builder;
}
