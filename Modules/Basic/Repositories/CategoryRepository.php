<?php

declare(strict_types=1);

namespace Essprendimai\Basic\Repositories;

use Essprendimai\Basic\Entities\Category;
use Essprendimai\Basic\Services\LanguageService;

/**
 * Class CategoryRepository
 * @package Essprendimai\Basic\Repositories
 */
class CategoryRepository extends Repository
{
    /**
     * Constants
     */
    const FOOTER = 'footer';
    const HEADER = 'header';
    const HOME = 'home';

    /**
     * @return string
     */
    public function model(): string
    {
        return Category::class;
    }

    /**
     * @return array Example. Key [lt][1] = 'title'. [language][category id] = 'page title'
     * @throws \RuntimeException
     */
    public function getAllFilteredByLanguage()
    {
        $categories = $this->all();

        $categoryByLanguages = [];
        foreach ($categories as $category) {
            foreach (LanguageService::getAllActiveLanguages() as $language) {
                if ($category->language === $language->id()) {
                    $categoryByLanguages[$language->id()][0] = '-';
                    $categoryByLanguages[$language->id()][$category->id] = $category->title;
                }
            }
        }

        return $categoryByLanguages;
    }
}
