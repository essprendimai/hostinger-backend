<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Essprendimai\Basic\Entities\Page;
use Illuminate\Support\Collection;
use InvalidArgumentException;
use RuntimeException;
use Essprendimai\Basic\Services\LanguageService;

/**
 * Class PageRepository
 * @package Essprendimai\Basic\Repositories
 */
class PageRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Page::class;
    }

    /**
     * @param string $key
     * @param string $language
     * @return Page|null
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function findWithKey(string $key, string $language): ?Page
    {
        return $this->makeQuery()
            ->where([
                'key' => $key,
                'language' => $language,
            ])
            ->with('category')
            ->get()
            ->first();
    }

    /**
     * @return Collection
     */
    public function getAllTemplatesByCurrentLanguage(): Collection
    {
        return $this->makeQuery()
            ->where([
                'is_template' => true,
                'language' => LanguageService::getActiveLanguage()->id()
            ])
            ->get();
    }

    /**
     * @param string $pageUrl
     * @param string $categoryUrl
     * @return Page|null
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function findOneByPageUrlAndCategoryUrl(string $pageUrl, string $categoryUrl): ?Page
    {
        return $this->makeQuery()
            ->with(['menuGroups'])
            ->select(['pages.*'])
            ->join('categories', 'pages.category_id', '=', 'categories.id')
            ->where([
                'pages.url' => $pageUrl,
                'categories.url' => $categoryUrl,
            ])
            ->get()
            ->first();
    }

    /**
     * @param string $pageUrl
     * @param string $key
     * @return Page|null
     */
    public function findOneByPageUrlAndKey(string $pageUrl, string $key): ?Page
    {
        return $this->makeQuery()
            ->select(['pages.*'])
            ->join('categories', 'pages.category_id', '=', 'categories.id')
            ->where([
                'pages.url' => $pageUrl,
                'categories.key' => $key,
            ])
            ->get()
            ->first();
    }

    /**
     * @param string $key
     * @return Collection
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function findAllByKey(string $key): Collection
    {
        return $this->makeQuery()
            ->with(['menuGroups'])
            ->select('*')
            ->where(['key' => $key])
            ->with('category')
            ->get();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getPagesByCurrentLanguage(): LengthAwarePaginator
    {
        return $this->makeQuery()
            ->where(['language' => LanguageService::getActiveLanguage()->id()])
            ->with('category')
            ->groupBy('key')
            ->orderBy('created_at', 'desc')
            ->paginate(self::DEFAULT_PER_PAGE, ['*']);
    }

    /**
     * @param string $url
     * @param array $columns
     * @return Builder|Model|object|null
     */
    public function findOneByUrl(string $url, array $columns = ['*'])
    {
        return $this->makeQuery()
            ->with(['menuGroups'])
            ->where([
                'url' => $url,
            ])
            ->first($columns);
    }
}
