<?php

declare(strict_types=1);

namespace Essprendimai\Basic\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Essprendimai\Basic\Entities\GroupRelation;
use Essprendimai\Companies\Enums\UserGroupsRelationEnum;

/**
 * Class GroupRelationRepository
 * @package Essprendimai\Basic\Repositories
 */
class GroupRelationRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return GroupRelation::class;
    }

    /**
     * @param int $groupId
     * @param string $type
     * @return Collection
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function getAllByGroupIdAndType(int $groupId, string $type): Collection
    {
        return $this->makeQuery()
            ->with(['user', 'group'])
            ->where([
                'type' => $type,
                'group_id' => $groupId,
            ])
            ->get();
    }

    /**
     * @param int $groupId
     * @param array $relationships
     * @return Collection
     */
    public function getAllByGroupAndRelationships(int $groupId, array $relationships): Collection
    {
        return $this->makeQuery()
            ->where([
                'group_id' => $groupId,
            ])
            ->whereIn('relation_id', $relationships)
            ->get();
    }

    /**
     * @param int $groupId
     * @param array $userIds
     * @return Collection
     */
    public function getAllByGroupAndUsersId(int $groupId, array $userIds): Collection
    {
        return $this->makeQuery()
            ->with(['user', 'group'])
            ->where([
                'group_id' => $groupId,
            ])
            ->whereIn('relation_id', $userIds)
            ->get();
    }

    /**
     * @param int $userId
     * @return Collection
     */
    public function getAllByUserId(int $userId): Collection
    {
        return $this->makeQuery()
            ->with(['user', 'group'])
            ->where([
                'relation_id' => $userId,
            ])
            ->get();
    }

    /**
     * @param array $userIds
     * @return Collection
     */
    public function getAllByUserIds(array $userIds): Collection
    {
        return $this->makeQuery()
            ->with(['user', 'group'])
            ->where([
                'type' => UserGroupsRelationEnum::user()->id(),
            ])
            ->whereIn('relation_id', $userIds)
            ->get();
    }

    /**
     * @param int $groupId
     * @param int $relationId
     */
    public function removeRelationByGroupIdAndRelationId(int $groupId, int $relationId): void
    {
        $this->makeQuery()
            ->where([
                'group_id' => $groupId,
                'relation_id' => $relationId,
                'type' => UserGroupsRelationEnum::user()->id(),
            ])
            ->delete();
    }

    /**
     * @param array $groupIds
     * @param int $relationId
     * @param string $type
     */
    public function createRelations(array $groupIds, int $relationId, string $type = '-'): void
    {
        foreach ($groupIds as $groupId) {
            $this->createRelation((int)$groupId, $relationId, $type);
        }
    }

    /**
     * @param int $groupId
     * @param int $relationId
     * @param string $type
     */
    public function createRelation(int $groupId, int $relationId, string $type = '-'): void
    {
        $this->makeQuery()
            ->create([
                'group_id' => $groupId,
                'relation_id' => $relationId,
                'type' => $type,
            ]);
    }

    /**
     * @param array $relationIds
     * @param string $type
     */
    public function deleteAllRelations(array $relationIds, string $type = '-'): void
    {
        $this->makeQuery()
            ->where([
                'type' => $type,
            ])
            ->whereIn('relation_id', $relationIds)
            ->delete();
    }
}
