<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Essprendimai\Basic\Entities\User;

/**
 * Class UserRepository
 * @package Essprendimai\Basic\Repositories
 */
class UserRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return User::class;
    }

    /**
     * @return Collection
     * @throws \RuntimeException
     */
    public function getAllUsers(): Collection
    {
        return $this->makeQuery()
            ->with(['groupGroups'])
            ->get();
    }
}
