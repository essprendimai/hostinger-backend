<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Entities;

use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\Basic\Services\UserMailSenderService;
use Essprendimai\Basic\Enums\GroupsTypesEnum;
use Essprendimai\Basic\Notifications\ResetPasswordQueued;
use Essprendimai\Basic\Notifications\VerifyEmailQueued;
use Essprendimai\Basic\Services\LanguageService;
use Essprendimai\Basic\Traits\Roles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package Essprendimai\Basic\Entities
 */
class User extends Authenticatable implements MustVerifyEmail, CanResetPassword
{
    use Notifiable, Roles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'change_password',
        'is_admin',
        'email_verified_at',
        'is_draft',
        'created_as_draft',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return MorphMany
     */
    public function groups(): MorphMany
    {
        return $this->morphMany(GroupRelation::class, 'group', 'relation_id');
    }

    public function sendEmailVerificationNotification(): void
    {
        /** @var UserMailSenderService $mailer */
        $mailer = app(UserMailSenderService::class);
        $this->notify($mailer->getVerifyEmailNotify());
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        /** @var UserMailSenderService $mailer */
        $mailer = app(UserMailSenderService::class);
        $this->notify($mailer->getResetPasswordNotify($token));
    }

    /**
     * @return BelongsToMany
     */
    public function groupGroups(): belongsToMany
    {
        return $this->belongsToMany(Group::class, 'group_relations', 'relation_id')->where([
            'group_relations.type' => Group::ALL_USERS,
            'groups.type' => GroupsTypesEnum::group()->id(),
        ]);
    }
}
