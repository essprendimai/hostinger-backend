<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Group
 * @package Essprendimai\Basic\Entities
 */
class Group extends Model
{
    const HEADER = 'header';
    const FOOTER = 'footer';
    const HOME = 'home';
    const FEATURED_CMS = 'featured-cms';
    const ALL_USERS = 'all-users';
    const NEWSLETTER_SIGN_UPS = 'newsletter-sign-ups';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = [
        'key',
        'type',
        'title',
    ];
}

