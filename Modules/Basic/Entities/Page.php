<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Essprendimai\Basic\Enums\GroupsRelationsTypesEnum;
use Essprendimai\Basic\Enums\GroupsTypesEnum;

/**
 * Class Page
 * @package Essprendimai\Basic\Entities
 */
class Page extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = [
        'key',
        'language',
        'title',
        'url',
        'text',
        'category_id',
        'active',
        'external_url',
        'type',
        'featured_image',
        'is_template',
    ];

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return BelongsToMany
     */
    public function menuGroups(): belongsToMany
    {
        return $this->belongsToMany(Group::class, 'group_relations', 'relation_id')->where([
            'group_relations.type' => GroupsRelationsTypesEnum::page()->id(),
            'groups.type' => GroupsTypesEnum::menu()->id(),
        ]);
    }

    /**
     * @param string $type
     * @return bool
     */
    public function isPageByType(string $type): bool
    {
        foreach ($this->menuGroups as $menuGroup) {
            if ($menuGroup->key === $type) {
                return true;
            }
        }

        return false;
    }
}
