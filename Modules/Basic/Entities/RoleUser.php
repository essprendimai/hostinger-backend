<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleUser
 * @package Essprendimai\Basic\Entities
 */
class RoleUser extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'role_user';

    /**
     * @var array
     */
    protected $fillable = [
        'role_id',
        'user_id',
    ];
}
