<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;

/**
 * Class Role
 * @package Essprendimai\Basic\Entities
 *
 * @property int id
 * @property string name
 * @property array accessible_routes
 * @property string description
 * @property bool full_access
 * @property-read Collection|Model[] users
 */
class Role extends Model
{
    /** @var array */
    protected $fillable = [
        'name',
        'accessible_routes',
        'description',
        'full_access',
    ];

    /** @var array */
    protected $casts = [
        'accessible_routes' => 'array',
    ];

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'role_user', 'role_id', 'user_id');
    }
}
