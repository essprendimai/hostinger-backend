<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Enums;

/**
 * Class GroupsTypesEnum
 * @package Essprendimai\Basic\Enums
 */
class GroupsTypesEnum extends Enumerable
{
    /**
     * @return GroupsTypesEnum
     */
    final public static function pageCategory(): GroupsTypesEnum
    {
        return self::make('page_category', 'basic::group.page_category');
    }

    /**
     * @return GroupsTypesEnum
     */
    final public static function group(): GroupsTypesEnum
    {
        return self::make('group', 'basic::group.group');
    }

    /**
     * @return GroupsTypesEnum
     */
    final public static function menu(): GroupsTypesEnum
    {
        return self::make('menu', 'basic::group.menu');
    }

    /**
     * @return GroupsTypesEnum
     */
    final public static function empty(): GroupsTypesEnum
    {
        return self::make('-', 'Empty');
    }
}
