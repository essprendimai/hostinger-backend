<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Enums;

/**
 * Class PageGroupsTypesEnum
 * @package Essprendimai\Basic\Enums
 */
class PageGroupsTypesEnum extends Enumerable
{
    /**
     * @return PageGroupsTypesEnum
     */
    final public static function none(): PageGroupsTypesEnum
    {
        return self::make('-', '');
    }

    /**
     * @return PageGroupsTypesEnum
     */
    final public static function group(): PageGroupsTypesEnum
    {
        return self::make('group', 'basic::group.category');
    }

    /**
     * @return PageGroupsTypesEnum
     */
    final public static function menu(): PageGroupsTypesEnum
    {
        return self::make('menu', 'basic::group.menu_group');
    }
}
