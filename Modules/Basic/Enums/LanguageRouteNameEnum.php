<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Enums;

/**
 * Class LanguageRouteNameEnum
 * @package Essprendimai\Basic\Enums
 */
class LanguageRouteNameEnum extends Enumerable
{
    /**
     * @return LanguageRouteNameEnum
     */
    final public static function lt(): LanguageRouteNameEnum
    {
        return self::make('lt', 'puslapis');
    }

    /**
     * @return LanguageRouteNameEnum
     */
    final public static function en(): LanguageRouteNameEnum
    {
        return self::make('en', 'page');
    }

    /**
     * @return LanguageRouteNameEnum
     */
    final public static function ee(): LanguageRouteNameEnum
    {
        return self::make('ee', 'lehel');
    }
}
