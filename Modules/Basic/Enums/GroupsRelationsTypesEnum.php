<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Enums;

/**
 * Class GroupsRelationsTypesEnum
 * @package Essprendimai\Basic\Enums
 */
class GroupsRelationsTypesEnum extends Enumerable
{
    /**
     * @return GroupsRelationsTypesEnum
     * @throws \ReflectionException
     */
    final public static function page(): GroupsRelationsTypesEnum
    {
        return self::make('page', 'Page');
    }

    /**
     * @return GroupsRelationsTypesEnum
     * @throws \ReflectionException
     */
    final public static function user(): GroupsRelationsTypesEnum
    {
        return self::make('user', 'User');
    }
}
