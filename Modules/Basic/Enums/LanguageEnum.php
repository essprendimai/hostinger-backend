<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Enums;

/**
 * Class LanguageEnum
 * @package Essprendimai\Basic\Enums
 */
class LanguageEnum extends Enumerable
{
    /**
     * @return LanguageEnum
     */
    final public static function lt(): LanguageEnum
    {
        return self::make('lt', 'basic::language.lithuanian');
    }

    /**
     * @return LanguageEnum
     */
    final public static function en(): LanguageEnum
    {
        return self::make('en', 'basic::language.english');
    }

    /**
     * @return LanguageEnum
     */
    final public static function ee(): LanguageEnum
    {
        return self::make('ee', 'basic::language.estonia');
    }
}
