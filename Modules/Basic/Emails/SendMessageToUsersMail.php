<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Emails;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;

/**
 * Class SendMessageToUsersMail
 * @package Essprendimai\Basic\Emails
 */
class SendMessageToUsersMail extends Mailable implements ShouldQueue
{
    /**
     * @var string
     */
    public $message;

    /**
     * ContactFormMail constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): Mailable
    {
        return $this
            ->locale($this->locale)
            ->subject(__('basic::mail.send_message_to_users.subject'))
            ->markdown('basic-view::mail.message-to-users');
    }
}
