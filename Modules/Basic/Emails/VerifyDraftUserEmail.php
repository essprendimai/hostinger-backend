<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Emails;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;


/**
 * Class VerifyDraftUserEmail
 * @package Essprendimai\Basic\Emails
 */
class VerifyDraftUserEmail extends Mailable implements ShouldQueue
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $button_url;

    /**
     * VerifyDraftUserEmail constructor.
     * @param int $userId
     * @param string $password
     */
    public function __construct(int $userId, string $password)
    {
        $this->user_id = $userId;
        $this->password = $password;
        $this->button_url = $this->verificationUrl($userId);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): Mailable
    {
        return $this
            ->locale($this->locale)
            ->subject(__('basic::mail.verify_draft_user_email.subject'))
            ->markdown('basic-view::mail.verify-draft-user');
    }

    /**
     * @param $userId
     * @return string
     */
    protected function verificationUrl($userId)
    {
        return URL::temporarySignedRoute(
            'verification.verify', Carbon::now()->addMinutes(60), ['id' => $userId]
        );
    }
}
