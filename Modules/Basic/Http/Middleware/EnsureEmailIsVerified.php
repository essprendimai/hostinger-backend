<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Redirect;
use Essprendimai\Basic\Http\Controllers\Auth\LoginController;

/**
 * Class EnsureEmailIsVerified
 * @package Essprendimai\Basic\Http\Middleware
 */
class EnsureEmailIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::guard(LoginController::AUTH_CLIENT_TYPE)->check()) {
            $user = \Auth::guard(LoginController::AUTH_CLIENT_TYPE)->user();
            if (!$user || (
                !$user->is_admin &&
                $user instanceof MustVerifyEmail &&
                !$user->hasVerifiedEmail()
            )) {
                return $request->expectsJson()
                    ? abort(403, 'Your email address is not verified.')
                    : Redirect::route('verification.notice');
            }
        }

        return $next($request);
    }
}
