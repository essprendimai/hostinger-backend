<?php

namespace Essprendimai\Basic\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Essprendimai\Basic\Http\Controllers\Auth\LoginController;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard(LoginController::AUTH_CLIENT_TYPE)->check()) {
            return redirect(LoginController::CLIENT_REDIRECT_TO);
        }

        if (Auth::guard($guard)->check()) {
            if (!Auth::user()->is_admin) {
                return redirect(LoginController::CLIENT_REDIRECT_TO);
            }

            return redirect(LoginController::ADMIN_REDIRECT_TO);
        }

        return $next($request);
    }
}
