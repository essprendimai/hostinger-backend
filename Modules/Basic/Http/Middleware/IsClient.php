<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Essprendimai\Basic\Entities\User;
use Essprendimai\Basic\Http\Controllers\Auth\LoginController;

/**
 * Class IsClient
 * @package Essprendimai\Basic\Http\Middleware
 */
class IsClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard(LoginController::AUTH_CLIENT_TYPE)->check()) {
            /** @var User $user */
            $user = Auth::guard(LoginController::AUTH_CLIENT_TYPE)->user();
            if ($user->hasVerifiedEmail() || $user->is_admin) {
                return $next($request);
            } else {
                return redirect(route('welcome'))->with('alert', __('basic::auth.verify_account_email'));
            }
        }

        return redirect(route('welcome'));
    }
}
