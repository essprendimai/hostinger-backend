<?php

declare(strict_types=1);

namespace Essprendimai\Basic\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Essprendimai\Basic\Http\Controllers\Auth\LoginController;

/**
 * Class ForceChangePassword
 * @package Essprendimai\Basic\Http\Middleware
 */
class ForceChangePassword
{
    const CHANGE_PASSWORD_ROUTE = '/backend/change-password';
    const CHANGE_PASSWORD_ROUTE_CLIENT = [
        '/client/profile/edit',
        '/client/profile/store'
    ];

    /**
     * @param Request $request
     * @param Closure $next
     * @return RedirectResponse|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()
            && Auth::user()->is_admin
            && $request->getPathInfo() !== self::CHANGE_PASSWORD_ROUTE
            && Auth::user()->change_password
            && Auth::user()->hasVerifiedEmail()) {
            return redirect(route('backend.change-password.form'));
        }

        $auth = Auth::guard(LoginController::AUTH_CLIENT_TYPE);
        if ($auth->check()
            && !$auth->user()->is_admin
            && !in_array($request->getPathInfo(), self::CHANGE_PASSWORD_ROUTE_CLIENT)
            && $auth->user()->change_password
            && $auth->user()->hasVerifiedEmail()) {
            return redirect(route('client.profile.edit'));
        }

        return $next($request);
    }
}
