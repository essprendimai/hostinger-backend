<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Middleware;

use Closure;
use Essprendimai\Basic\Services\RouteAccessManager;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/**
 * Class LimitsBackendRouteAccess
 * @package Essprendimai\Basic\Http\Middleware
 */
class LimitsBackendRouteAccess
{
    const ALIAS = 'limit-backend-access';
    const ACCESS_NOT_ALLOWED_MESSAGE = 'You don\'t have access to this page';

    /** @var  RouteAccessManager */
    private $routeAccessManager;

    /**
     * LimitsBackendRouteAccess constructor.
     * @param RouteAccessManager $routeAccessManager
     */
    public function __construct(RouteAccessManager $routeAccessManager)
    {
        $this->routeAccessManager = $routeAccessManager;
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return RedirectResponse|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->shouldBlockAccess()) {
            return redirect(route('welcome'))->with('danger', self::ACCESS_NOT_ALLOWED_MESSAGE);
        }

        return $next($request);
    }

    /**
     * @return bool
     */
    protected function shouldBlockAccess(): bool
    {
        return Auth::check()
            && !$this->routeAccessManager->accessAllowed(Auth::user(),
                (string)array_get(Route::current()->action, 'as'));
    }
}
