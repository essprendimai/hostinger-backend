<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Middleware;

use Closure;
use Essprendimai\Basic\Services\LanguageService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class LanguageAccess
 * @package Essprendimai\Basic\Http\Middleware
 */
class LanguageAccess
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return RedirectResponse|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        LanguageService::load();

        return $next($request);
    }
}
