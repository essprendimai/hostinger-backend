<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers;

use Illuminate\View\View;
use LogicException;
use Essprendimai\Basic\Entities\Category;
use Essprendimai\Basic\Repositories\CategoryRepository;

/**
 * Class WelcomeController
 * @package Essprendimai\Basic\Http\Controllers
 */
class WelcomeController extends Controller
{
    /**
     * @throws LogicException
     */
    public function index(): View
    {
        return view('basic-view::welcome');
    }
}
