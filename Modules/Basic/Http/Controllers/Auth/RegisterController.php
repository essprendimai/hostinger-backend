<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Essprendimai\Basic\Database\Seeders\UsersTableSeeder;
use Essprendimai\Basic\Entities\Role;
use Essprendimai\Basic\Entities\User;
use Essprendimai\Basic\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Essprendimai\Basic\Services\RouteAccessManager;

/**
 * Class RegisterController
 * @package Essprendimai\Basic\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    const CLIENT_REGISTER_ROUTE_NAME = 'client-register';

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = LoginController::CLIENT_REDIRECT_TO;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // Disable auto login after registration
        //$this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect(route('welcome'))
                ->with('alert_success', __('basic::register.client_page.success'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'agree_to_privacy_policy' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return mixed
     */
    protected function create(array $data)
    {
        /** @var RouteAccessManager $routeAccessManager */
        $routeAccessManager = app(RouteAccessManager::class);

        $user = User::create([
            'name' => $data['email'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'is_admin' => 0,
        ]);

        $routeAccessManager->syncRoles($user, [Role::where('name', UsersTableSeeder::CLIENT_ROLE)->first()->id]);

        return $user;
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard(LoginController::AUTH_CLIENT_TYPE);
    }
}
