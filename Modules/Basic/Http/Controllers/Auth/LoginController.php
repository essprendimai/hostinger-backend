<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Essprendimai\Basic\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Session\Store;

/**
 * Class LoginController
 * @package Essprendimai\Basic\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    const AUTH_CLIENT_TYPE = 'client';

    const ADMIN_LOGIN_ROUTE_NAME = 'admin-panel-login';
    const CLIENT_LOGIN_ROUTE_NAME = 'login';
    const CLIENT_LOGOUT_ROUTE_NAME = 'client-logout';

    const ADMIN_REDIRECT_TO = '/backend/home';
    const CLIENT_REDIRECT_TO = '/client/home';

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = self::ADMIN_REDIRECT_TO;

    /**
     * @var string
     */
    protected $redirectClientTo = self::CLIENT_REDIRECT_TO;

    /**
     * Unauthenticated login route
     *
     * @var string
     */
    public static $unauthenticatedTo = 'client.login';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('custom-guest')->except('logout');
    }

    /**
     * @return View
     * @throws \Exception
     */
    public function showClientLoginForm(): View
    {
//        if (MainSettings::disableLoginPage()->getValueConverted()) {
//            abort(404);
//        }

        return view('basic-view::client-login');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        if (request()->get('auth_type') === self::AUTH_CLIENT_TYPE) {
            return Auth::guard('client');
        }

        return Auth::guard();
    }

    /**
     * @return string
     */
    protected function redirectTo()
    {
        return Auth::guard(self::AUTH_CLIENT_TYPE)->check() || !Auth::user()->is_admin ? $this->redirectClientTo : $this->redirectTo;
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $credentials = $this->credentials($request);
        if ('/' . self::ADMIN_LOGIN_ROUTE_NAME === $request->getRequestUri()) {
            $credentials += ['is_admin' => 1];
        }

        return $this->guard()->attempt($credentials, $request->filled('remember'));
    }


    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        /** @var Store $session */
        $session = app(Store::class);
        $session->flash('hide_header_messages', 1);
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }
}
