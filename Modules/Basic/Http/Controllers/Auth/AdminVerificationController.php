<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers\Auth;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Modules\Basic\Services\UserMailSenderService;
use Essprendimai\Basic\Entities\User;
use Throwable;

/**
 * Class AdminVerificationController
 * @package Essprendimai\Basic\Http\Controllers\Auth
 */
class AdminVerificationController
{
    /**
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    public function sendVerification(Request $request, User $user): RedirectResponse
    {
        if ($user->hasVerifiedEmail()) {
            return redirect(route('backend.users.index'))
                ->with('danger', __('basic::users.failed_verification_sent'));
        }

        try {
            $user->sendEmailVerificationNotification();
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.users.index'))
                ->with('danger', $throwable->getMessage());
        }

        return redirect(route('backend.users.index'))
            ->with('success', __('basic::users.verification_sent'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @param UserMailSenderService $mailer
     * @return RedirectResponse
     */
    public function sendDraftUserVerification(Request $request, User $user, UserMailSenderService $mailer): RedirectResponse
    {
        if ($user->hasVerifiedEmail() || !$user->is_draft) {
            return redirect(route('backend.users.index'))
                ->with('danger', __('basic::users.failed_verification_sent'));
        }

        try {
            $mailer->sendVerifyDraftUser($user);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.users.index'))
                ->with('danger', $throwable->getMessage());
        }

        return redirect(route('backend.users.index'))
            ->with('success', __('basic::users.verification_sent'));
    }
}
