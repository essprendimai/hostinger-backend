<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers;

use Essprendimai\Basic\Entities\Page;
use Essprendimai\Basic\Enums\GroupsTypesEnum;
use Essprendimai\Basic\Http\Requests\PagesFormRequest;
use Essprendimai\Basic\Repositories\CategoryRepository;
use Essprendimai\Basic\Repositories\GroupRepository;
use Essprendimai\Basic\Repositories\PageRepository;
use Essprendimai\Basic\Services\LanguageService;
use Essprendimai\Basic\Services\PageService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use LogicException;
use Essprendimai\Basic\Services\PageTransactionService;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

/**
 * Class PagesController
 * @package Essprendimai\Basic\Http\Controllers
 */
class PagesController extends Controller
{
    const FAIL_CREATE = 'Failed to create page';
    const FAIL_UPDATE = 'Failed to update page';
    const FAIL_DELETE = 'Failed to delete page';

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * @var PageTransactionService
     */
    private $pageTransactionService;

    /**
     * PagesController constructor.
     * @param CategoryRepository $categoryRepository
     * @param PageRepository $pageRepository
     * @param GroupRepository $groupRepository
     * @param PageTransactionService $pageTransactionService
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        PageRepository $pageRepository,
        GroupRepository $groupRepository,
        PageTransactionService $pageTransactionService
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->pageRepository = $pageRepository;
        $this->groupRepository = $groupRepository;
        $this->pageTransactionService = $pageTransactionService;
    }

    /**
     * @return View
     * @throws LogicException
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function index(): View
    {
        $pages = $this->pageRepository->getPagesByCurrentLanguage();

        return view('basic-view::pages.index', compact('pages'));
    }

    /**
     * @return View
     * @throws LogicException
     * @throws \ReflectionException
     * @throws \RuntimeException
     */
    public function create(): View
    {
        $categoryByLanguages = $this->categoryRepository->getAllFilteredByLanguage();
        $menuGroups = $this->groupRepository->getGroupsByType(GroupsTypesEnum::menu()->id());

        return view('basic-view::pages.create', compact('categoryByLanguages', 'menuGroups'));
    }

    /**
     * @param PagesFormRequest $request
     * @return RedirectResponse
     */
    public function store(PagesFormRequest $request): RedirectResponse
    {
        try {
            $this->pageTransactionService->storePages($request);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.pages.index'))
                ->with('danger', $throwable->getMessage());
        }

        return redirect(route('backend.pages.index'))
            ->with('success', __('basic::validation.success_create'));
    }

    /**
     * @param Request $request
     * @param string $key
     * @return RedirectResponse
     */
    public function destroy(Request $request, string $key): RedirectResponse
    {
        try {
            $this->pageTransactionService->destroyPagesByKey($key);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.pages.index'))
                ->with('danger', $throwable->getMessage());
        }

        return redirect(route('backend.pages.index'))
            ->with('success', __('basic::validation.success_delete'));
    }

    /**
     * @param string $key
     * @return View
     * @throws \Exception
     */
    public function edit(string $key): View
    {
        $filteredPages = $this->getFilteredPages($key);
        $categoryByLanguages = $this->categoryRepository->getAllFilteredByLanguage();
        $menuGroups = $this->groupRepository->getGroupsByType(GroupsTypesEnum::menu()->id());

        return view('basic-view::pages.edit', compact('filteredPages', 'key', 'categoryByLanguages', 'menuGroups'));
    }

    /**
     * @param PagesFormRequest $request
     * @param string $key
     * @return RedirectResponse
     */
    public function update(PagesFormRequest $request, string $key)
    {
        try {
            $this->pageTransactionService->updatePages($request, $key);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.pages.index'))
                ->with('danger', $throwable->getMessage());
        }

        return redirect(route('backend.pages.index'))
            ->with('success', __('basic::validation.success_update'));
    }

    /**
     * @param Request $request
     * @param string $pageUrl
     * @param PageService $pageService
     * @return View
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function showPageByPageUrl(Request $request, string $pageUrl, PageService $pageService): View
    {
        try {
            $pageService->setLayout('basic-view::layouts.page.default');

            return $pageService->buildPage($pageUrl);
        } catch (Throwable $throwable) {
            return abort(404);
        }
    }

    /**
     * @param Request $request
     * @param string $categoryUrl
     * @param string $pageUrl
     * @param PageService $pageService
     * @return View
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function showPageByCategoryAndPageUrl(
        Request $request,
        string $categoryUrl,
        string $pageUrl,
        PageService $pageService
    ): View {
        try {
            $pageService->setLayout('basic-view::layouts.page.default');

            return $pageService->buildPage($pageUrl, $categoryUrl);
        } catch (Throwable $throwable) {
            return abort(404);
        }
    }

    /**
     * @param Request $request
     * @param Page $page
     * @param bool $status
     * @return RedirectResponse
     */
    public function changeStatus(Request $request, Page $page, bool $status): RedirectResponse
    {
        try {
            $page->active = $status;
            $page->save();
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect()
                ->back()
                ->with('danger', $throwable->getMessage());
        }

        return redirect()
            ->back()
            ->with('success', __('basic::validation.success_update'));
    }

    /**
     * @return View
     */
    public function groupIndex(): View
    {
        $pages = $this->pageRepository->getPagesByCurrentLanguage();

        return view('basic-view::pages.menu-index', compact('pages'));
    }

    /**
     * @param string $key
     * @return array
     * @throws \Exception
     */
    private function getFilteredPages(string $key): array
    {
        $pages = $this->pageRepository->findAllByKey($key);

        $filteredPages = [];
        foreach (LanguageService::getAllActiveLanguages() as $language) {
            $filteredPages[$language->id()] = $pages->filter(function ($page) use ($language) {
                return $page->language == $language->id() ? $page : null;
            })->first();
        }

        return $filteredPages;
    }
}
