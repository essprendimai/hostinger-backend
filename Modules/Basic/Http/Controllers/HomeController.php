<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers;

use Illuminate\Http\RedirectResponse;

/**
 * Class HomeController
 * @package Essprendimai\Basic\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * @return RedirectResponse
     */
    public function index(): RedirectResponse
    {
        return redirect(route('backend.users.index'));
    }
}
