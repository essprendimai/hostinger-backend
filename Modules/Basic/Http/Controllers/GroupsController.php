<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers;

use Essprendimai\Basic\Entities\Group;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Essprendimai\Basic\Http\Requests\GroupsFormRequest;
use Essprendimai\Basic\Repositories\GroupRepository;
use Throwable;

/**
 * Class GroupsController
 * @package Essprendimai\Basic\Http\Controllers
 */
class GroupsController extends Controller
{
    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * GroupsController constructor.
     * @param GroupRepository $groupRepository
     */
    public function __construct(GroupRepository $groupRepository)
    {
        $this->groupRepository = $groupRepository;
    }

    /**
     * @return View
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \RuntimeException
     */
    public function index(): View
    {
        $groups = $this->groupRepository->all();

        return view('basic-view::groups.index', compact('groups'));
    }

    /**
     * @return View
     * @throws \LogicException
     */
    public function create(): View
    {
        return view('basic-view::groups.create');
    }

    /**
     * @param GroupsFormRequest $request
     * @return RedirectResponse
     */
    public function store(GroupsFormRequest $request): RedirectResponse
    {
        try {
            $this->groupRepository->create([
                'key' => $request->getKey(),
                'title' => $request->getTitle(),
                'type' => $request->getType(),
            ]);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.groups.index'))
                ->with('danger', $throwable->getMessage());
        }

        return redirect(route('backend.groups.index'))
            ->with('success', __('basic::validation.success_create'));
    }

    /**
     * @param Request $request
     * @param Group $group
     * @return RedirectResponse
     */
    public function destroy(Request $request, Group $group): RedirectResponse
    {
        try {
            $group->delete();
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.groups.index'))
                ->with('danger', $throwable->getMessage());
        }

        return redirect(route('backend.groups.index'))
            ->with('success', __('basic::validation.success_delete'));
    }

    /**
     * @param Group $group
     * @return View
     */
    public function edit(Group $group): View
    {
        return view('basic-view::groups.edit', compact('group'));
    }

    /**
     * @param GroupsFormRequest $request
     * @param string $id
     * @return RedirectResponse
     */
    public function update(GroupsFormRequest $request, string $id): RedirectResponse
    {
        try {
            $this->groupRepository->update([
                'key' => $request->getKey(),
                'title' => $request->getTitle(),
                'type' => $request->getType(),
            ], $id);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.groups.index'))
                ->with('danger',$throwable->getMessage());
        }

        return redirect(route('backend.groups.index'))
            ->with('success', __('basic::validation.success_update'));
    }
}
