<?php

declare(strict_types=1);

namespace Essprendimai\Basic\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Essprendimai\Basic\Http\Controllers\Auth\LoginController;
use Essprendimai\Basic\Repositories\UserRepository;
use Essprendimai\Basic\Http\Requests\ProfileEditRequest;
use Throwable;

/**
 * Class ClientProfileController
 * @package Essprendimai\Basic\Http\Controllers
 */
class ClientProfileController extends Controller
{
    /**
     * @return View
     */
    public function edit(): View
    {
        return view('basic-view::client.edit');
    }

    /**
     * @param ProfileEditRequest $request
     * @param UserRepository $userRepository
     * @return RedirectResponse
     */
    public function store(ProfileEditRequest $request, UserRepository $userRepository): RedirectResponse
    {
        try {
            $userRepository->update([
                'password' => bcrypt($request->getPasswordNew()),
                'change_password' => 0,
                'is_draft' => 0,
            ], Auth::guard(LoginController::AUTH_CLIENT_TYPE)->user()->id);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('client.home'))
                ->with('danger', __('basic::validation.failed_update'));
        }

        return redirect(route('client.home'))
            ->with('success', __('basic::validation.success_update'));
    }
}
