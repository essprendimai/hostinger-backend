<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers;

use Essprendimai\Basic\Entities\Role;
use Essprendimai\Basic\Http\Requests\CreateRoleRequest;
use Essprendimai\Basic\Http\Requests\DeleteRoleRequest;
use Essprendimai\Basic\Http\Requests\UpdateRoleRequest;
use Essprendimai\Basic\Services\RouteAccessManager;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use LogicException;

/**
 * Class RolesController
 * @package Essprendimai\Basic\Http\Controllers
 */
class RolesController extends Controller
{
    /**
     * @return View
     * @throws LogicException
     * @throws MassAssignmentException
     */
    public function index(): View
    {
        return view('basic-view::role.index', [
            'roles' => Role::all(),
        ]);
    }

    /**
     * @param Role $role
     * @param RouteAccessManager $routeAccessManager
     * @return View
     * @throws LogicException
     */
    public function show(Role $role, RouteAccessManager $routeAccessManager): View
    {
        return view('basic-view::role.edit', [
            'role' => $role,
            'routes' => $routeAccessManager->getBackendRoutes(),
        ]);
    }

    /**
     * @param RouteAccessManager $routeAccessManager
     * @return View
     * @throws LogicException
     * @throws MassAssignmentException
     */
    public function create(RouteAccessManager $routeAccessManager): View
    {
        return view('basic-view::role.create', [
            'role' => new Role(),
            'routes' => $routeAccessManager->getBackendRoutes(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param CreateRoleRequest $request
     * @param RouteAccessManager $routeAccessManager
     * @return RedirectResponse
     */
    public function store(CreateRoleRequest $request, RouteAccessManager $routeAccessManager): RedirectResponse
    {
        $role = Role::create($request->input());
        $routeAccessManager->flushCache();

        return redirect(route('backend.role.edit', $role))->withSuccess(__('basic::validation.success_create'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param Role $role
     * @param RouteAccessManager $routeAccessManager
     * @return View
     * @throws LogicException
     */
    public function edit(Role $role, RouteAccessManager $routeAccessManager): View
    {
        return view('basic-view::role.edit', [
            'role' => $role,
            'routes' => $routeAccessManager->getBackendRoutes(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateRoleRequest $request
     * @param Role $role
     * @param RouteAccessManager $routeAccessManager
     * @return RedirectResponse
     * @throws MassAssignmentException
     */
    public function update(
        UpdateRoleRequest $request,
        Role $role,
        RouteAccessManager $routeAccessManager
    ): RedirectResponse
    {
        $data = $request->input();
        if (!isset($data['accessible_routes'])) {
            $data['accessible_routes'] = [];
        }

        $role->update($data);
        $routeAccessManager->flushCache();

        return back()->withSuccess(__('basic::validation.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     * @param DeleteRoleRequest $request
     * @param Role $role
     * @param RouteAccessManager $routeAccessManager
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(
        DeleteRoleRequest $request,
        Role $role,
        RouteAccessManager $routeAccessManager
    ): RedirectResponse
    {
        $role->delete();
        $routeAccessManager->flushCache();

        return redirect(route('backend.role.index'))->withSuccess(__('basic::validation.success_delete'));
    }
}
