<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers;

use Essprendimai\Basic\Http\Requests\CategoriesFormRequest;
use Essprendimai\Basic\Repositories\CategoryRepository;
use Essprendimai\Basic\Repositories\PageRepository;
use Essprendimai\Basic\Services\LanguageService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Throwable;

/**
 * Class CategoriesController
 * @package Essprendimai\Basic\Http\Controllers
 */
class CategoriesController extends Controller
{
    const FAIL_CREATE = 'Failed to create category';
    const FAIL_UPDATE = 'Failed to update category';
    const FAIL_DELETE = 'Failed to delete category';

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * CategoriesController constructor.
     * @param CategoryRepository $categoryRepository
     * @param PageRepository $pageRepository
     */
    public function __construct(CategoryRepository $categoryRepository, PageRepository $pageRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->pageRepository = $pageRepository;
    }

    /**
     * @return View
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \RuntimeException
     */
    public function index(): View
    {
        $categories = $this->categoryRepository->paginateDistinctDesc(['key']);

        return view('basic-view::categories.index', compact('categories'));
    }

    /**
     * @return View
     * @throws \LogicException
     */
    public function create(): View
    {
        return view('basic-view::categories.create');
    }

    /**
     * @param CategoriesFormRequest $request
     * @return RedirectResponse
     */
    public function store(CategoriesFormRequest $request): RedirectResponse
    {
        try {
            foreach ($request->get('title') as $languageKey => $title) {
                $this->categoryRepository->create([
                    'key' => str_replace(' ', '-', trim($request->get('key'))),
                    'language' => $languageKey,
                    'title' => $title,
                    'url' => str_replace(' ', '-', trim($request->get('url')[$languageKey])),
                ]);
            }
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.categories.index'))
                ->with('danger', $throwable->getMessage());
        }

        return redirect(route('backend.categories.index'))
            ->with('success', __('basic::validation.success_create'));
    }

    /**
     * @param Request $request
     * @param string $key
     * @return RedirectResponse
     */
    public function destroy(Request $request, string $key): RedirectResponse
    {
        try {
            $categories = $this->categoryRepository->findAllBy([
                'key' => $key,
            ]);

            $categoriesIds = $categories->pluck('id');
            if ($categoriesIds->isNotEmpty()) {
                $categoryPages = $this->pageRepository->findWhereIn($categoriesIds->toArray(), 'category_id');
                if ($categoryPages->isNotEmpty()) {
                    return redirect(route('backend.categories.index'))
                        ->with('danger', self::FAIL_DELETE);
                }
            }

            $this->categoryRepository->deleteWhere([
                'key' => $key,
            ]);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.categories.index'))
                ->with('danger', $throwable->getMessage());
        }

        return redirect(route('backend.categories.index'))
            ->with('success', __('basic::validation.success_delete'));
    }

    /**
     * @param string $key
     * @return View
     * @throws \Exception
     */
    public function edit(string $key): View
    {
        $categories = $this->categoryRepository->findAllBy(['key' => $key]);

        $filteredCategories = [];
        foreach (LanguageService::getAllActiveLanguages() as $language) {
            $filteredCategories[$language->id()] = $categories->filter(function($category) use ($language) {
                return $category->language == $language->id() ? $category : null;
            })->first();
        }

        return view('basic-view::categories.edit', compact('filteredCategories', 'key'));
    }

    /**
     * @param CategoriesFormRequest $request
     * @param string $key
     * @return RedirectResponse
     */
    public function update(CategoriesFormRequest $request, string $key)
    {
        try {
            foreach ($request->get('title') as $languageKey => $title) {
                $keyUpdate = str_replace(' ', '-', trim($request->get('key')));
                $urlUpdate = str_replace(' ', '-', trim($request->get('url')[$languageKey]));

                $this->categoryRepository->updateOrCreate([
                    'key' => $key,
                    'language' => $languageKey,
                ], [
                    'key' => $keyUpdate,
                    'language' => $languageKey,
                    'title' => $title,
                    'url' => $urlUpdate,
                ]);
            }
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect(route('backend.categories.index'))
                ->with('danger', 'Category has assigned pages. First of all remove pages or change category');
        }

        return redirect(route('backend.categories.index'))
            ->with('success', __('basic::validation.success_update'));
    }
}
