<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers;

use Essprendimai\Basic\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use InvalidArgumentException;
use LogicException;
use Throwable;

/**
 * Class ChangePasswordController
 * @package Essprendimai\Basic\Http\Controllers
 */
class ChangePasswordController extends Controller
{
    /**
     * @return View
     * @throws LogicException
     */
    public function changePassword(): View
    {
        return view('basic-view::user.password.change');
    }

    /**
     * @param ChangePasswordRequest $request
     * @return RedirectResponse
     * @throws InvalidArgumentException
     * @throws LogicException
     */
    public function postChangePassword(ChangePasswordRequest $request): RedirectResponse
    {
        try {
            $user = $request->user();
            $user->change_password = 0;
            $user->password = bcrypt($request->new_password);
            $user->save();

            return redirect()->route('backend.users.index')->with('success', __('basic::validation.success_update'));
        } catch (Throwable $exception) {
            return redirect()->back()->withErrors([$exception->getMessage()]);
        }
    }
}
