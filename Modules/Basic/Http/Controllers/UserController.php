<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers;

use InvalidArgumentException;
use Modules\Basic\Services\UserMailSenderService;
use Essprendimai\Basic\Entities\Group;
use Essprendimai\Basic\Entities\Role;
use Essprendimai\Basic\Entities\User;
use Essprendimai\Basic\Enums\GroupsTypesEnum;
use Essprendimai\Basic\Http\Requests\AddUsersToGroupRequest;
use Essprendimai\Basic\Http\Requests\CreateUserRequest;
use Essprendimai\Basic\Http\Requests\SendEmailForUsersFromGroupRequest;
use Essprendimai\Basic\Http\Requests\SendEmailForUsersRequest;
use Essprendimai\Basic\Http\Requests\UpdateUserRequest;
use Essprendimai\Basic\Repositories\GroupRelationRepository;
use Essprendimai\Basic\Repositories\GroupRepository;
use Essprendimai\Basic\Repositories\UserRepository;
use Essprendimai\Basic\Services\RouteAccessManager;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use LogicException;
use Throwable;

/**
 * Class UserController
 * @package Essprendimai\Basic\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var GroupRelationRepository
     */
    private $groupRelationRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     * @param GroupRelationRepository $groupRelationRepository
     */
    public function __construct(UserRepository $userRepository, GroupRelationRepository $groupRelationRepository)
    {
        $this->userRepository = $userRepository;
        $this->groupRelationRepository = $groupRelationRepository;
    }

    /**
     * @param Request $request
     * @param GroupRepository $groupRepository
     * @return View
     * @throws LogicException
     */
    public function index(Request $request, GroupRepository $groupRepository): View
    {
        try {
            $users = $this->userRepository->getAllUsers();
            $menuGroups = $groupRepository->getGroupsByType(GroupsTypesEnum::group()->id());

            return view('basic-view::user.index', compact('users', 'menuGroups'));
        } catch (Throwable $exception) {
            return view('basic-view::user.index', [
                'users' => collect(),
            ])->withErrors(['Failed to retrieve users.']);
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return View
     * @throws LogicException
     */
    public function create(Request $request): View
    {
        $roles = Role::all();

        return view('basic-view::user.create', compact('roles'));
    }

    /**
     * @param CreateUserRequest $request
     * @param RouteAccessManager $routeAccessManager
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function store(CreateUserRequest $request, RouteAccessManager $routeAccessManager): RedirectResponse
    {
        try {
            $user = new User();

            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->change_password = $request->input('change_password', 0);
            $user->is_admin = $request->isAdmin();
            $user->save();

            $routeAccessManager->syncRoles($user, $request->getRoles());

            return redirect()->back()->with([
                'success' => __('basic::validation.success_create'),
            ]);
        } catch (Throwable $exception) {
            return redirect()->back()->withErrors([$exception->getMessage()]);
        }

    }

    /**
     * Show the specified resource.
     * @param Request $request
     * @param int $id
     * @return View
     * @throws LogicException
     */
    public function show(Request $request, string $id): View
    {
        try {
            $user = $this->userRepository->find($id);
            $roles = Role::all();

            return view('basic-view::user.show', compact('user', 'secretKey', 'roles'));
        } catch (Throwable $exception) {
            return view('basic-view::user.show', [
                'user' => collect(),
                'roles' => [],
                'secretKey' => '',
            ])->withErrors([$exception->getMessage()]);
        }

    }

    /**
     * @param UpdateUserRequest $request
     * @param int $id
     * @param RouteAccessManager $routeAccessManager
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function update(
        UpdateUserRequest $request,
        int $id,
        RouteAccessManager $routeAccessManager
    ): RedirectResponse {
        try {
            $update = [
                'name' => $request->name,
                'change_password' => $request->input('change_password', 0),
                'is_admin' => $request->isAdmin(),
            ];

            if ($request->password != null) {
                $update['password'] = bcrypt($request->password);
            }

            $this->userRepository->update($update, $id);

            /** @var Role $user */
            $user = $this->userRepository->findOrFail($id);
            $routeAccessManager->syncRoles($user, $request->getRoles());

            return redirect()->back()->with('success', __('basic::validation.success_update'));
        } catch (Throwable $exception) {
            return redirect()->back()->withErrors([$exception->getMessage()]);
        }

    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        try {
            $this->userRepository->delete($id);

            return redirect()->back()->with('success', __('basic::validation.success_delete'));
        } catch (Throwable $exception) {
            return redirect()->back()->withErrors([$exception->getMessage()]);
        }
    }

    /**
     * @param SendEmailForUsersRequest $request
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function sendMessageToUsers(SendEmailForUsersRequest $request): RedirectResponse
    {
        try {
            /** @var UserMailSenderService $mailer */
            $mailer = app(UserMailSenderService::class);
            $mailer->sendMessageToUsers($request->getUsers(), $request->getMessage());
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect()
                ->back()
                ->with('danger', __('basic::mail.failed_to_send'));
        }

        return redirect()
            ->back()
            ->with('success', __('basic::mail.successfully_sent'));
    }

    /**
     * @param SendEmailForUsersFromGroupRequest $request
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function sendMessageToUsersFromGroup(SendEmailForUsersFromGroupRequest $request): RedirectResponse
    {
        try {
            /** @var GroupRepository $groupRepository */
            $groupRepository = app(GroupRepository::class);
            $group = $groupRepository->find($request->getGroupId());
            if ($group && $group->exists) {
                $relations = $this->groupRelationRepository->getAllByGroupIdAndType(
                    $request->getGroupId(),
                    Group::ALL_USERS
                );

                /** @var UserMailSenderService $mailer */
                $mailer = app(UserMailSenderService::class);
                $mailer->sendMessageToUsers($relations->pluck('user.email')->toArray(), $request->getMessage());
            }
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect()
                ->back()
                ->with('danger', __('basic::mail.failed_to_send'));
        }

        return redirect()
            ->back()
            ->with('success', __('basic::mail.successfully_sent'));
    }

    /**
     * @param AddUsersToGroupRequest $request
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function addUsersToGroup(AddUsersToGroupRequest $request): RedirectResponse
    {
        try {
            foreach ($request->getUsers() as $userId) {
                $this->groupRelationRepository->createRelations(
                    $request->getGroupIds(),
                    (int)$userId,
                    Group::ALL_USERS
                );
            }
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect()
                ->back()
                ->with('danger', __('basic::validation.failed_add_to_group'));
        }

        return redirect()
            ->back()
            ->with('success', __('basic::validation.success_add_to_group'));
    }


    /**
     * @param Request $request
     * @param int $userId
     * @param int $groupId
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function removeFromGroup(Request $request, int $userId, int $groupId): RedirectResponse
    {
        try {
            $this->groupRelationRepository->removeRelationByGroupIdAndRelationId($groupId, $userId);
        } catch (Throwable $throwable) {
            logger()->error($throwable, $request->toArray());

            return redirect()
                ->back()
                ->with('danger', __('basic::validation.failed_delete'));
        }

        return redirect()
            ->back()
            ->with('success', __('basic::validation.success_delete'));
    }
}
