<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers;

use Illuminate\View\View;
use Essprendimai\Settings\Settings\MainSettings;

/**
 * Class ClientHomeController
 * @package Modules\Basic\Http\Controllers
 */
class ClientHomeController extends Controller
{
    /**
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
//        if (MainSettings::disableLoginPage()->getValueConverted()) {
//            abort(404);
//        }

        return view('basic-view::client.index');
    }
}
