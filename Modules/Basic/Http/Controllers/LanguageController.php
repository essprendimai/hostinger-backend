<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Controllers;

use Essprendimai\Basic\Services\LanguageService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class LanguageController
 * @package Essprendimai\Basic\Http\Controllers
 */
class LanguageController extends Controller
{
    /**
     * @param Request $request
     * @param string $language
     * @return RedirectResponse
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function change(Request $request, string $language): RedirectResponse
    {
        return LanguageService::change($request, $language);
    }

    /**
     * @param Request $request
     * @param string $language
     * @return RedirectResponse
     * @throws \InvalidArgumentException
     */
    public function changeBackend(Request $request, string $language)
    {
        LanguageService::changeBackend($request, $language);

        return redirect()->back();
    }
}
