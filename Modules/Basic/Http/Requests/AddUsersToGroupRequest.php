<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;
use Essprendimai\Basic\Entities\GroupRelation;
use Essprendimai\Basic\Repositories\GroupRelationRepository;

/**
 * Class AddUsersToGroupRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class AddUsersToGroupRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'users' => 'required|array|distinct',
            'users.*' => 'required|numeric|exists:users,id',
            'group_ids' => 'required|array|distinct',
            'group_ids.*' => 'required|numeric|exists:groups,id',
        ];
    }

    /**
     * Custom validate
     *
     * @param Validator $validator
     */
    public function withValidator(Validator $validator)
    {
        $method = $this->getMethod();
        if (strtolower($method) === 'post') {
            $userIds = $this->getUsers();
            $groupIds = $this->getGroupIds();

            $validator->after(function (Validator $validator) use ($userIds, $groupIds): void {
                /** @var GroupRelationRepository $groupRelationRepository */
                $groupRelationRepository = app(GroupRelationRepository::class);

                foreach ($groupIds as $groupId) {
                    $existingUsers = $groupRelationRepository->getAllByGroupAndUsersId((int)$groupId, $userIds);
                    if ($existingUsers->isNotEmpty()) {
                        /** @var GroupRelation $groupRelations */
                        foreach ($existingUsers as $groupRelations) {
                            $errorKey = 'user - ' . $groupRelations->user->email . ' ';
                            $validator->errors()->add($errorKey, ' '. __('basic::validation.exists_group', [
                                'user' => $groupRelations->user->email,
                                'group' => $groupRelations->group->title,
                            ]));
                        }
                    }
                }
            });
        }
    }

    /**
     * @return array|null
     */
    public function getUsers(): ?array
    {
        return (array)$this->input('users') ?? null;
    }

    /**
     * @return array
     */
    public function getGroupIds(): array
    {
        return (array)$this->input('group_ids');
    }
}
