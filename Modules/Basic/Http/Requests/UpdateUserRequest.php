<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateUserRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:191',
            'password' => 'nullable|min:8',
            'roles' => 'array',
        ];
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return (array)$this->input('roles');
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return (bool)$this->input('is_admin', 0);
    }
}
