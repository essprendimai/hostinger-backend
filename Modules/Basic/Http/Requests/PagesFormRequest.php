<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Http\UploadedFile;
use Essprendimai\Basic\Repositories\PageRepository;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class PagesFormRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class PagesFormRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'key' => 'required|string' . ($this->isMethod('post') ? '|unique:pages,key' : ''),
            'title' => 'array',
            'title.*' => '',
            'url' => 'array',
            'url.*' => ($this->isMethod('post') ? 'unique:pages,url|distinct' : '') . ($this->isMethod('put') ? 'distinct' : ''),
            'text' => '',
            'category' => 'array',
            'category.*' => '',
            'old_url' => 'array',
            'old_url.*' => '',
            'active' => 'array',
            'active.*' => '',
            'external_url' => 'array',
            'external_url.*' => '',
            'is_template' => 'array',
            'is_template.*' => '',
            'menu' => 'array',
            'menu.*' => 'array',
            'featured_image' => 'array',
            'featured_image.*' => 'image:png,jpg,jpeg',
        ];
    }

    /**
     * Validate on PUT request if url is used in another pages
     *
     * @param Validator $validator
     */
    public function withValidator(Validator $validator)
    {
        $method = $this->getMethod();
        if (strtolower($method) === 'put') {
            $oldUrls = $this->get('old_url');
            $newUrls = $this->get('url');

            $urls = array_diff($newUrls, $oldUrls);

            $validator->after(function (Validator $validator) use ($urls): void {
                /** @var PageRepository $pageCategory */
                $pageCategory = app(PageRepository::class);

                $existingUrls = $pageCategory->findWhereIn($urls, 'url');
                if ($existingUrls->isNotEmpty()) {
                    foreach ($existingUrls as $url) {
                        $errorKey = 'url - ' . $url->url . ' ';
                        $validator->errors()->add($errorKey, __('basic::validation.unique', ['attribute' => $errorKey]));
                    }
                }
            });
        }
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return (string)str_replace(' ', '-', trim($this->get('key')));
    }

    /**
     * @param string $languageKey
     * @return array|null
     */
    public function getTitle(string $languageKey): ?string
    {
        return isset($this->get('title')[$languageKey]) ? (string)$this->get('title')[$languageKey] : null;
    }

    /**
     * @param string $languageKey
     * @return bool
     */
    public function getActive(string $languageKey): ?bool
    {
        return isset($this->get('active')[$languageKey]) ? true : false;
    }

    /**
     * @param string $languageKey
     * @return bool
     */
    public function getExternalUrl(string $languageKey): ?bool
    {
        return isset($this->get('external_url')[$languageKey]) ? true : false;
    }

    /**
     * @param string $languageKey
     * @return null|string
     */
    public function getUrl(string $languageKey): ?string
    {
        return isset($this->get('url')[$languageKey]) ?
            (string)str_replace(' ', '-', trim($this->get('url')[$languageKey])) : null;
    }

    /**
     * @param string $languageKey
     * @return int|null
     */
    public function getCategory(string $languageKey): ?int
    {
        return isset($this->get('category')[$languageKey]) && $this->get('category')[$languageKey] != 0 ?
            (int)$this->get('category')[$languageKey] : null;
    }

    /**
     * @param string $languageKey
     * @return string|null
     */
    public function getText(string $languageKey): ?string
    {
        return isset($this->get('text')[$languageKey]) ? $this->get('text')[$languageKey] : null;
    }

    /**
     * @param string $languageKey
     * @return array|null
     */
    public function getGroupIds(string $languageKey): ?array
    {
        return isset($this->get('menu')[$languageKey]) && $this->get('menu')[$languageKey] != 0 ? (array)$this->get('menu')[$languageKey] : null;
    }

    /**
     * @param string $languageKey
     * @return UploadedFile|null
     */
    public function getFeaturedImageFile(string $languageKey): ?UploadedFile
    {
        return isset($this->file('featured_image')[$languageKey]) && $this->file('featured_image')[$languageKey] != 0 ? $this->file('featured_image')[$languageKey] : null;
    }

    /**
     * @param string $languageKey
     * @return bool
     */
    public function isTemplate(string $languageKey): bool
    {
        return isset($this->get('is_template')[$languageKey]) ? true : false;
    }
}
