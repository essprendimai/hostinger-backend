<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

/**
 * Class UpdateRoleRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class UpdateRoleRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'max:191',
                Rule::unique('roles')->ignore($this->route('role')->id),
            ],
            'accessible_routes' => 'array',
            'full_access' => 'required|bool',
        ];
    }
}
