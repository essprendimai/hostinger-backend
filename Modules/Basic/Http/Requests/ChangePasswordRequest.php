<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * Class ChangePasswordRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required',
            'new_password' => 'required|min:8|confirmed|different:old_password',
        ];
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        $validator->after(function(Validator $validator) {
            /** @var \Illuminate\Validation\Validator $validator */
            if ($validator->errors()->any()) {
                // exit early without other validations if failed
                return;
            }
            $user = Auth::user();
            if (!Hash::check($this->input('old_password'), $user->getAuthPassword())) {
                $validator->errors()->add('old_password', 'Wrong old password');

                return;
            }
        });

        return $validator;
    }
}
