<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class SendEmailForUsersFromGroupRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class SendEmailForUsersFromGroupRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'group' => 'required|numeric|exists:groups,id',
            'message' => 'required|string',
        ];
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return (int)$this->input('group');
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return (string)$this->input('message');
    }
}
