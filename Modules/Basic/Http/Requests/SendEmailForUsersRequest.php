<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class SendEmailForUsersRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class SendEmailForUsersRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'users' => 'required|array|distinct',
            'message' => 'required|string',
        ];
    }

    /**
     * @return array|null
     */
    public function getUsers(): ?array
    {
        return (array)$this->input('users') ?? null;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return (string)$this->input('message');
    }
}
