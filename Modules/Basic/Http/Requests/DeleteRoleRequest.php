<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class DeleteRoleRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class DeleteRoleRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
