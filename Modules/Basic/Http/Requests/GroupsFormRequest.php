<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use Essprendimai\Basic\Enums\Enumerable;
use Essprendimai\Basic\Enums\GroupsTypesEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Essprendimai\Basic\Repositories\GroupRepository;

/**
 * Class GroupsFormRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class GroupsFormRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'type' => [
                'required',
                Rule::in(GroupsTypesEnum::enum()),
            ],
        ];
    }

    /**
     * @param Validator $validator
     */
    public function withValidator(Validator $validator)
    {
        $isRequestPut = strtolower($this->getMethod()) === 'put';
        $key = $this->getKey();

        $validator->after(function (Validator $validator) use ($key, $isRequestPut): void {
            /** @var GroupRepository $groupRepository */
            $groupRepository = app(GroupRepository::class);

            $group = $groupRepository->findOneBy(['key' => $key]);
            if ($isRequestPut && $group && $group->id !== $this->getId()) {
                $validator->errors()->add('title', __('basic::group.errors.already_exists'));
            } elseif (!$isRequestPut && $group) {
                $validator->errors()->add('title', __('basic::group.errors.already_exists'));
            }
        });
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return (string)$this->input('title');
    }

    /**
     * @return GroupsTypesEnum
     * @throws \Exception
     */
    public function getType(): Enumerable
    {
        return GroupsTypesEnum::from($this->input('type'));
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return strtolower(str_replace(' ', '-', $this->getTitle()));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->input('id');
    }
}
