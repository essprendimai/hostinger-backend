<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateUserRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:191',
            'email' => 'required|email|max:191|unique:users',
            'password' => 'required|min:8',
            'roles' => 'array',
        ];
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return (array)$this->input('roles');
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return (bool)$this->input('is_admin', 0);
    }
}
