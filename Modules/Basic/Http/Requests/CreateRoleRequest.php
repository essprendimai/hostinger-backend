<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class CreateRoleRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class CreateRoleRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:191|unique:roles',
            'accessible_routes' => 'array',
            'full_access' => 'required|bool',
        ];
    }
}
