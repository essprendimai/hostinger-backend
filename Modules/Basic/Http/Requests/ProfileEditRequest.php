<?php

declare(strict_types=1);

namespace Essprendimai\Basic\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Essprendimai\Basic\Http\Controllers\Auth\LoginController;

/**
 * Class ProfileEditRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class ProfileEditRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'password' => 'required|string',
            'password_new' => 'required|string|confirmed',
            'password_new_confirmation' => 'required|string',
        ];
    }

    /**
     * Validate on PUT request if url is used in another pages
     *
     * @param Validator $validator
     */
    public function withValidator(Validator $validator)
    {
        $method = $this->getMethod();
        if (strtolower($method) === 'post') {
            $validator->after(function (Validator $validator): void {
                if(!Hash::check(
                    $this->getPassword(), Auth::guard(LoginController::AUTH_CLIENT_TYPE)->user()->password)
                ) {
                    $validator->errors()
                        ->add('password', __('basic::profile.error_messages.password_do_not_match', ['attribute' => 'password']));
                }
            });
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return (string)$this->get('password');
    }

    /**
     * @return string
     */
    public function getPasswordNew(): string
    {
        return (string)$this->get('password_new');
    }

    /**
     * @return string
     */
    public function getPasswordNewConfirm(): string
    {
        return (string)$this->get('password_new_confirmation');
    }
}
