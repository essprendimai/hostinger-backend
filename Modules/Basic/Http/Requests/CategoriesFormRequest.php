<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Http\Requests;

use Essprendimai\Basic\Repositories\CategoryRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;

/**
 * Class CategoriesFormRequest
 * @package Essprendimai\Basic\Http\Requests
 */
class CategoriesFormRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'key' => 'required|string' . ($this->isMethod('post') ? '|unique:categories,key' : ''),
            'title' => 'required|array',
            'title.*' => 'string',
            'url' => 'required|array',
            'url.*' => 'string|' . ($this->isMethod('post') ? '|unique:categories,url' : '') . '|distinct',
            'old_url' => 'array',
            'old_url.*' => '',
        ];
    }

    /**
     * @param Validator $validator
     */
    public function withValidator(Validator $validator)
    {
        $method = $this->getMethod();
        if (strtolower($method) === 'put') {

            $oldUrls = $this->get('old_url');
            $newUrls = $this->get('url');

            $urls = array_diff($newUrls, $oldUrls);

            $validator->after(function (Validator $validator) use ($urls): void {
                /** @var CategoryRepository $categoryRepository */
                $categoryRepository = app(CategoryRepository::class);

                $existingUrls = $categoryRepository->findWhereIn($urls, 'url');
                if ($existingUrls->isNotEmpty()) {
                    foreach ($existingUrls as $url) {
                        $errorKey = 'url.' . $url->language;
                        $validator->errors()->add($errorKey, __('basic::validation.unique', ['attribute' => $errorKey]));
                    }
                }
            });
        }
    }
}
