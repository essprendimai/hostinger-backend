<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

/**
 * Class ResetPasswordQueued
 * @package Essprendimai\Basic\Notifications
 */
class ResetPasswordQueued extends ResetPassword implements ShouldQueue
{
    use Queueable;

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
//        if (static::$toMailCallback) {
//            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
//        }

        return (new MailMessage)
            ->subject(__('basic::mail.send_reset_password.subject'))
            ->markdown('basic-view::mail.reset-password', [
                'url' => url(config('app.url').route('password.reset', $this->token, false)),
                'title' => __('basic::mail.send_reset_password.title'),
                'footer' => __('basic::mail.send_reset_password.footer'),
                'reset_button' => __('basic::mail.send_reset_password.reset_button'),
            ]);
    }
}
