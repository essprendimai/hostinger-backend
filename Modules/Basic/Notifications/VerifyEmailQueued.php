<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Notifications;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class VerifyEmailQueued
 * @package Essprendimai\Basic\Notifications
 */
class VerifyEmailQueued extends VerifyEmail implements ShouldQueue
{
    use Queueable;

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
//        if (static::$toMailCallback) {
//            return call_user_func(static::$toMailCallback, $notifiable);
//        }

        return (new MailMessage)
            ->subject(__('basic::mail.verify_mail.subject'))
            ->markdown('basic-view::mail.verify-user', [
                'url' => $this->verificationUrl($notifiable),
                'title' => __('basic::mail.verify_mail.title'),
                'footer' => __('basic::mail.verify_mail.footer'),
                'activate' => __('basic::mail.verify_mail.activate'),
            ]);
    }
}
