<?php

declare(strict_types=1);

use Essprendimai\Basic\Entities\RoleUser;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(RoleUser::class, function (Faker\Generator $faker) use ($factory)  {
    return [
        'user_id' => $faker->unique()->randomNumber(5),
        'role_id' => $faker->unique()->randomNumber(5),
    ];
});

