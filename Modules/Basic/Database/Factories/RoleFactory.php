<?php

declare(strict_types=1);

use Essprendimai\Basic\Entities\Role;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Role::class, function (Faker\Generator $faker) use ($factory)  {
    return [
        'name' => $faker->unique()->firstName,
        'full_access' => 0,
        'accessible_routes' => [],
        'description' => '',
    ];
});
