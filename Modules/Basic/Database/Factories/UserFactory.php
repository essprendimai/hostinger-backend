<?php

declare(strict_types=1);

use Essprendimai\Basic\Entities\User;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(User::class, function (Faker\Generator $faker) use ($factory) {
    return [
        'name' => $faker->address,
        'email' => $faker->email,
        'password' => $faker->md5,
        'change_password' => 0,
        'remember_token' => $faker->md5,
        'is_admin' => $faker->boolean,
    ];
});

