<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Database\Seeders;

use Essprendimai\Basic\Entities\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use LogicException;
use Essprendimai\Basic\Entities\User;

/**
 * Class UsersTableSeeder
 * @package Essprendimai\Basic\Database\Seeders
 */
class UsersTableSeeder extends Seeder
{
    const ADMIN_ROLE = 'Admin';
    const CLIENT_ROLE = 'Client';

    /**
     * @throws LogicException
     */
    public function run(): void
    {
        $userName = env('ADMIN_USER_NAME', 'administratorius-1');
        DB::table('users')->insert([
            'name' => $userName,
            'email' => env('ADMIN_USER_EMAIL', 'info@admin.com'),
            'password' => bcrypt(env('ADMIN_USER_PASSWORD', 'secret')),
            'is_admin' => 1,
        ]);

        $this->createRole(self::ADMIN_ROLE, [], 'Has access to all admin pages', true, $userName);

        $userName = 'client' . env('ADMIN_USER_NAME', 'administratorius-1');
        DB::table('users')->insert([
            'name' => $userName,
            'email' => 'client' . env('ADMIN_USER_EMAIL', 'info@admin.com'),
            'password' => bcrypt(env('ADMIN_USER_PASSWORD', 'secret')),
            'is_admin' => 0,
        ]);

        $this->createRole(self::CLIENT_ROLE, [], 'Has access to all client pages', false, $userName);

        DB::table('users')->insert([
            'name' => env('API_EMAIL', 'info@admin.com'),
            'email' => env('API_EMAIL', 'info@admin.com'),
            'password' => env('API_PASSWORD', 'secret'),
            'is_admin' => 1,
        ]);
    }

    /**
     * @param string $name
     * @param array $accessibleRoutes
     * @param string $description
     * @param bool $fullAccess
     * @param string $userName
     */
    private function createRole(
        string $name,
        array $accessibleRoutes,
        string $description,
        bool $fullAccess = false,
        string $userName = ''
    ): void {
        if (!Role::where('name', $name)->exists()) {
            (new Role([
                'name' => $name,
                'full_access' => $fullAccess,
                'accessible_routes' => $accessibleRoutes,
                'description' => $description,
            ]))->save();
        }

        DB::table('role_user')->insert([
            'user_id' => User::where('name', $userName)->first()->id,
            'role_id' => Role::where('name', self::ADMIN_ROLE)->first()->id,
        ]);
    }
}
