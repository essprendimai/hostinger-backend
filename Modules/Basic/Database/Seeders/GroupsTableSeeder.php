<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use LogicException;
use Essprendimai\Basic\Entities\Group;
use Essprendimai\Basic\Enums\GroupsTypesEnum;
use Essprendimai\Basic\Repositories\GroupRepository;

/**
 * Class GroupsTableSeeder
 * @package Essprendimai\Basic\Database\Seeders
 */
class GroupsTableSeeder extends Seeder
{
    /**
     * @throws LogicException
     */
    public function run(): void
    {
        $today = Carbon::now();

        /** @var GroupRepository $groupRepository */
        $groupRepository = app(GroupRepository::class);

        $groupRepository->insert([
            [
                'key' => Group::ALL_USERS,
                'title' => 'All users',
                'type' => GroupsTypesEnum::group()->id(),
                'created_at' => $today,
            ],
            [
                'key' => Group::NEWSLETTER_SIGN_UPS,
                'title' => 'Newsletter sign up',
                'type' => GroupsTypesEnum::group()->id(),
                'created_at' => $today,
            ],
            [
                'key' => Group::HEADER,
                'title' => 'Header',
                'type' => GroupsTypesEnum::menu()->id(),
                'created_at' => $today,
            ],
            [
                'key' => Group::FOOTER,
                'title' => 'Footer',
                'type' => GroupsTypesEnum::menu()->id(),
                'created_at' => $today,
            ],
            [
                'key' => Group::HOME,
                'title' => 'Home',
                'type' => GroupsTypesEnum::menu()->id(),
                'created_at' => $today,
            ],
            [
                'key' => Group::FEATURED_CMS,
                'title' => 'Featured CMS',
                'type' => GroupsTypesEnum::menu()->id(),
                'created_at' => $today,
            ],
        ]);
    }
}
