<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

/**
 * Class BasicDatabaseSeeder
 * @package Essprendimai\Basic\Database\Seeders
 */
class BasicDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     * @throws \InvalidArgumentException
     */
    public function run()
    {
        Model::unguard();

        $this->call(UsersTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
    }
}
