<?php

declare(strict_types = 1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreatePagesTable
 */
class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up(): void
    {
        Schema::create('pages', function(Blueprint $table) {
            $table->increments('id');

            $table->string('key');
            $table->string('language');
            $table->string('title')->nullable();
            $table->longText('text')->nullable();
            $table->string('url')->nullable();

            $table->unsignedInteger('category_id')->nullable();
            $table->foreign('category_id')
                ->references('id')
                ->on('categories');

            $table->index(['key', 'language']);
            $table->index('language');
            $table->index('url');
            $table->index('category_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down(): void
    {
        Schema::dropIfExists('pages');
    }
}
