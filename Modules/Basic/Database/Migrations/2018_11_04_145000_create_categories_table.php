<?php

declare(strict_types = 1);

use Essprendimai\Basic\Entities\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateTablesForRoles
 */
class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up(): void
    {
        Schema::create('categories', function(Blueprint $table) {
            $table->increments('id');

            $table->string('key');
            $table->string('language');
            $table->string('title');
            $table->string('url')->unique();

            $table->index(['key', 'language']);
            $table->index('language');
            $table->index('url');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
}
