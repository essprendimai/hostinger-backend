<?php

declare(strict_types = 1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddTypeFieldAtPagesTable
 */
class AddTypeFieldAtPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up(): void
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->string('type')->nullable();

            $table->index(['key', 'language', 'type']);
            $table->index(['type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down(): void
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn('type');

            $table->dropIndex('pages_key_language_type_index');
            $table->dropIndex('pages_type_index');
        });
    }
}
