<?php

declare(strict_types = 1);

use Essprendimai\Basic\Entities\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateTablesForRoles
 */
class CreateTablesForRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up(): void
    {
        Schema::create('roles', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->boolean('full_access')->default(false);
            $table->longText('accessible_routes')->nullable();
            $table->text('description');
            $table->timestamps();
        });

        Schema::create('role_user', function(Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('role_id');

            $table->foreign('user_id')
                ->references('id')->on(app(User::class)->getTable())
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary(['user_id', 'role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down(): void
    {
        Schema::dropIfExists('role_user');
        Schema::dropIfExists('roles');
    }
}
