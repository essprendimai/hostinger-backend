<?php

declare(strict_types = 1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateGroupsTable
 */
class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up(): void
    {
        Schema::create('groups', function(Blueprint $table): void {
            $table->increments('id');

            $table->string('key')->unique();
            $table->string('title');
            $table->string('type');

            $table->index(['key', 'type', 'title']);
            $table->index(['type', 'title']);
            $table->index('title');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down(): void
    {
        Schema::dropIfExists('groups');
    }
}
