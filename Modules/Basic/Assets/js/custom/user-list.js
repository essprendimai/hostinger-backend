/**
 * Companies user groups javascript
 */
module.exports = function() {
    var self = this;

    var globals = {
        inputs: {
            selectAllCheckboxes: '#check-all',
            userGroupsRowCheckboxes: '.user-groups-row-checkbox',
        },
        buttons: {
            sendMessage: '#send-message',
            addToGroup: '#add-to-group',
            sendMessageGroup: '#send-message-group',
            messageModalUsersFormSubmit: '#message-modal-users-form-submit',
            addToGroupModalSubmit: '#add-to-group-modal-submit',
            messageGroupModalFormSubmit: '#message-group-modal-form-submit',
        },
        modals: {
            sendMessageModal: '#send-message-modal',
            addToGroupModal: '#add-to-group-modal',
            sendMessageGroupModal: '#send-message-group-modal',
        },
        nodes: {
            sendMessageModalUsersInfo: '#send-message-modal-users-info',
            addToGroupModalUsersInfo: '#add-to-group-modal-users-info',
        },
        forms: {
            messageModalUsersForm: '#message-modal-users-form',
            addToGroupModalForm: '#add-to-group-modal-form',
            messageModalGroupForm: '#message-modal-group-form',
        },
    };

    /**
     * @type {{translations: {
     *  error_title: String,
     *  error_close_button: String,
     *  error_select_user: String,
     * }}}
     */
    var options = {};

    /**
     * Main app initialization.
     *
     * @type {{translations: {
     *  error_title: String,
     *  error_close_button: String,
     *  error_select_user: String,
     * }}}
     */
    self.init = function(initOptions) {
        options = initOptions;

        $(document).ready(function() {
            // Buttons
            $(globals.buttons.sendMessage).on('click', onClickSendMessageEvent);
            $(globals.buttons.addToGroup).on('click', onClickAddToGroupEvent);
            $(globals.buttons.sendMessageGroup).on('click', onClickSendMessageGroupEvent);

            // Inputs
            $(globals.inputs.selectAllCheckboxes).on('change', onSelectedAllCheckboxEvent);

            // Forms
            $(globals.buttons.messageModalUsersFormSubmit).on('click', onSubmitUsersMessageModalFormEvent);
            $(globals.buttons.addToGroupModalSubmit).on('click', onSubmitAddToGroupModalFormEvent);
            $(globals.buttons.messageGroupModalFormSubmit).on('click', onSubmitMessageGroupModalFormEvent);
        });
    };

    /**
     * @type Object event
     */
    function onSelectedAllCheckboxEvent(event) {
        var target = getTargetFromEvent(event);

        $(globals.inputs.userGroupsRowCheckboxes).prop('checked', target.checked);
    }

    /**
     * @type Object event
     */
    function onClickSendMessageEvent(event) {
        var target = getTargetFromEvent(event);

        if (isCheckboxSelected()) {
            loadUsersToModal(globals.nodes.sendMessageModalUsersInfo, globals.forms.messageModalUsersForm);
            $(globals.modals.sendMessageModal).modal('show');
        } else {
            swal({
                title: options.translations.error_title,
                text: options.translations.error_select_user,
                type: 'error',
                confirmButtonText: options.translations.error_close_button,
            });
        }
    }

    /**
     * @type Object event
     */
    function onClickSendMessageGroupEvent(event) {
        var target = getTargetFromEvent(event);

        $(globals.modals.sendMessageGroupModal).modal('show');
    }

    /**
     * @type Object event
     */
    function onClickAddToGroupEvent(event) {
        var target = getTargetFromEvent(event);

        if (isCheckboxSelected()) {
            loadUsersIdsToModal(globals.nodes.addToGroupModalUsersInfo, globals.forms.addToGroupModalForm);
            $(globals.modals.addToGroupModal).modal('show');
        } else {
            swal({
                title: options.translations.error_title,
                text: options.translations.error_select_user,
                type: 'error',
                confirmButtonText: options.translations.error_close_button,
            });
        }
    }

    function loadUsersToModal(node, form) {
        var users = getSelectedUsers();
        var userHiddenInputClassName = 'user-hidden-input-class';
        var userSpanClassName = 'user-span-class';

        $('.' + userHiddenInputClassName).remove();
        $('.' + userSpanClassName).remove();

        $(users).each(function (index, email) {
            $(node).append('<span class="' + userSpanClassName + '"><span class="badge badge-primary">' + email + '</span>&nbsp;</span>');

            $('<input>').attr({
                type: 'hidden',
                name: 'users[]',
                value: email,
                class: userHiddenInputClassName,
            }).appendTo(form);
        });
    }

    function loadUsersIdsToModal(node, form) {
        var usersIds = getSelectedUserIds();
        var users = getSelectedUsers();
        var userHiddenInputClassName = 'user-hidden-input-class';
        var userSpanClassName = 'user-span-class';

        $('.' + userHiddenInputClassName).remove();
        $('.' + userSpanClassName).remove();

        $(users).each(function (index, user) {
            $(node).append('<span class="' + userSpanClassName + '"><span class="badge badge-primary">' + user + '</span>&nbsp;</span>');
        });

        $(usersIds).each(function (index, userId) {
            $('<input>').attr({
                type: 'hidden',
                name: 'users[]',
                value: userId,
                class: userHiddenInputClassName,
            }).appendTo(form);
        });
    }

    /**
     * @returns {boolean}
     */
    function isCheckboxSelected() {
        var checked = false;
        $(globals.inputs.userGroupsRowCheckboxes).each(function(index, checkbox) {
            if (!checked && checkbox.checked) {
                checked = checkbox.checked;
            }
        });

        return checked;
    }

    function getSelectedUsers() {
        var users = [];
        $(globals.inputs.userGroupsRowCheckboxes).each(function(index, checkbox) {
            if (checkbox.checked) {
                var email = $(checkbox).closest('tr').find('.email')[0];
                if (email.innerHTML) {
                    users.push(email.innerHTML);
                }
            }
        });

        return users;
    }

    function getSelectedUserIds() {
        var users = [];
        $(globals.inputs.userGroupsRowCheckboxes).each(function(index, checkbox) {
            if (checkbox.checked) {
                var userId = $(checkbox).closest('tr').find('.user-id')[0];
                if (userId.innerHTML) {
                    users.push(userId.innerHTML);
                }
            }
        });

        return users;
    }

    function onSubmitUsersMessageModalFormEvent(event) {
        $(globals.forms.messageModalUsersForm).submit();
    }

    function onSubmitMessageGroupModalFormEvent(event) {
        $(globals.forms.messageModalGroupForm).submit();
    }

    function onSubmitAddToGroupModalFormEvent(event) {
        $(globals.forms.addToGroupModalForm).submit();
    }

    /**
     * @type Object event
     */
    function getTargetFromEvent(event) {
        var target;

        if (event.target) { // W3C
            target = event.target;
        } else if (event.srcElement) { // IE6-8
            target = event.srcElement;
        }

        if (target.nodeType == 3) { // Safari
            target = target.parentNode;
        }

        return target;
    }

    return self;
};
