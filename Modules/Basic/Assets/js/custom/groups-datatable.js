export function initGroupsIndexDatatable(translations) {
    $('#groups-data-table').DataTable({
        "language": datatableLanguages["{{ App::getLocale() }}"],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;

                if (!column.footer().innerHTML) {
                    return;
                }

                if ($(column.footer()).data().type === 'type') {
                    dataTableTypeSelection(column);
                } else {
                    dataTableBasicSearch(column);
                }
            });

            // Move tfooter filters to thead
            $('#groups-data-table tfoot tr').appendTo('#groups-data-table-thead');
        }
    });

    // Is admin filter
    function dataTableTypeSelection(column) {
        var select = $('<select class="form-control"><option value=""></option></select>')
            .appendTo($(column.footer()).empty())
            .on('change', function () {
                dataTableBasicFilter(this, column);
            });

        select.append('<option value="' + translations.page_category + '">' + translations.page_category + '</option>');
        select.append('<option value="' + translations.group + '">' + translations.group + '</option>');
        select.append('<option value="' + translations.menu + '">' + translations.menu + '</option>');
    }

    function dataTableBasicFilter(event, column) {
        var val = $.fn.dataTable.util.escapeRegex(
            $(event).val()
        );

        column.search(val)
            .draw();
    }

    // Basic Search
    function dataTableBasicSearch(column) {
        $('<input type="text" class="form-control" placeholder="' + translations.search + '"/>')
            .appendTo($(column.footer()).empty())
            .on('keyup change', function () {
                if (column.search() !== this.value) {
                    column.search( this.value )
                        .draw();
                }
            });
    }
}
