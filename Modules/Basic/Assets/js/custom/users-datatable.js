export function initUserIndexDatatable(translations) {
    $('#users-data-table').DataTable({
        "language": datatableLanguages["{{ App::getLocale() }}"],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;

                if (!column.footer().innerHTML) {
                    return;
                }

                if ($(column.footer()).data().type === 'is_admin') {
                    dataTableIsAdminSelection(column);
                } else if ($(column.footer()).data().type === 'email') {
                    dataTableBasicSearch(column);
                } else {
                    dataTableBasicSelection(column);
                }
            });

            // Move tfooter filters to thead
            $('#users-data-table tfoot tr').appendTo('#users-data-table-thead');
        }
    });

    // Is admin filter
    function dataTableIsAdminSelection(column) {
        var select = $('<select class="form-control"><option value=""></option></select>')
            .appendTo($(column.footer()).empty())
            .on('change', function () {
                dataTableBasicFilter(this, column);
            });

        select.append('<option value="' + translations.admin_type + '">' + translations.admin_type + '</option>');
        select.append('<option value="' + translations.client_type + '">' + translations.client_type + '</option>');
        select.append('<option value="' + translations.verified + '">' + translations.verified + '</option>');
        select.append('<option value="' + translations.draft + '">' + translations.draft + '</option>');
        select.append('<option value="' + translations.snx_ee + '">' + translations.snx_ee + '</option>');
    }

    // Basic filter
    function dataTableBasicSelection(column) {
        var select = $('<select class="form-control"><option value=""></option></select>')
            .appendTo($(column.footer()).empty())
            .on('change', function () {
                dataTableBasicFilter(this, column);
            });

        column.data().unique().sort().each(function (d, j) {
            select.append('<option value="'+d+'">'+d+'</option>')
        });
    }

    function dataTableBasicFilter(event, column) {
        var val = $.fn.dataTable.util.escapeRegex(
            $(event).val()
        );

        column.search(val)
            .draw();
    }

    // Basic Search
    function dataTableBasicSearch(column) {
        $('<input type="text" class="form-control" placeholder="' + translations.search + '"/>')
            .appendTo($(column.footer()).empty())
            .on('keyup change', function () {
                if (column.search() !== this.value) {
                    column.search( this.value )
                        .draw();
                }
            });
    }
}
