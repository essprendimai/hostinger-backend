export function confirmBeforeSubmit(formId, message) {
    var response = confirm(message);
    if (response) {
        document.getElementById(formId).submit();
    }

    return false;
}
