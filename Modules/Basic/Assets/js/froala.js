try {
    window.$ = window.jQuery = require('jquery');
} catch (e) {}

require("froala-editor/js/froala_editor.pkgd.min.js");

$(function() {
    $('div#froala-editor').froalaEditor({
        iconsTemplate: 'font_awesome_5'

        // If you want to use the regular/light icons, change the template to the following.
        // iconsTemplate: 'font_awesome_5r'
        // iconsTemplate: 'font_awesome_5l'
    })
});