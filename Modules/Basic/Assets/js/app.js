/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';
window.Vue = Vue;

require('select2');

import swal from 'sweetalert2';
window.swal = swal;

// Custom

import Layout from'./layouts/default';
window.Layout = Layout;

import {confirmBeforeSubmit} from './custom/actions';
window.confirmBeforeSubmit = confirmBeforeSubmit;

import {initUserIndexDatatable} from './custom/users-datatable';
window.initUserIndexDatatable = initUserIndexDatatable;

import {initGroupsIndexDatatable} from './custom/groups-datatable';
window.initGroupsIndexDatatable = initGroupsIndexDatatable;

import UserList from'./custom/user-list';
window.UserList = UserList;
