<?php

declare(strict_types = 1);

use Essprendimai\Basic\Services\RouteAccessManager;
use Illuminate\Support\Facades\Auth;

/**
 * Check whether or not currently logged-in user can access given route
 *
 * @param string $route
 * @return bool
 */
function can_access(string $route): bool
{
    return app(RouteAccessManager::class)->accessAllowed(Auth::user(), $route);
}

/**
 * Check whether or not currently logged-in user can access at least on of given routes
 *
 * @param string[] $routes
 * @return bool
 */
function can_access_any(array $routes): bool
{
    /** @var RouteAccessManager $manager */
    $manager = app(RouteAccessManager::class);

    $user = Auth::user();

    foreach ($routes as $route) {
        if ($manager->accessAllowed($user, $route)) {
            return true;
        }
    }

    return false;
}
