<?php

declare(strict_types = 1);

use Essprendimai\Basic\DTO\MenuDTO;
use Essprendimai\Basic\Entities\Page;

/**
 * @param mixed$object
 * @return string|null
 */
function get_page_link($object): ?string
{
    if ($object instanceof Page) {
        return get_page_from_page($object);
    } elseif ($object instanceof MenuDTO) {
        return get_page_from_menu($object);
    }

    return null;
}

/**
 * @param Page $page
 * @return string
 */
function get_page_from_page(Page $page): string
{
    if ($page->external_url) {
        return $page->url;
    }

    return $page->category ?
        route('page.' . $page->language . '.page-by-category-url', ['pageUrl' => $page->url, 'categoryUrl' => $page->category->url]) :
        route('page.' . $page->language  . '.page-by-url', ['pageUrl' => $page->url]);
}

/**
 * @param MenuDTO $menu
 * @return string
 */
function get_page_from_menu(MenuDTO $menu): string
{
    if ($menu->isExternalLink()) {
        return $menu->getPageUrl();
    }

    return $menu->getCategoryUrl() ?
        route('page.' . $menu->getLanguage() . '.page-by-category-url', ['pageUrl' => $menu->getPageUrl(), 'categoryUrl' => $menu->getCategoryUrl()]) :
        route('page.' . $menu->getLanguage() . '.page-by-url', ['pageUrl' => $menu->getPageUrl()]);
}
