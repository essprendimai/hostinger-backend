<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Exceptions;

/**
 * Class SettingsException
 * @package Essprendimai\Basic\Exceptions
 */
class SettingsException extends \Exception
{
    const FAIL_EMAIL_SEND_DISABLED = 'Email send disabled from settings';

    /**
     * @return SettingsException
     */
    public static function emailSendDisabledFromSettings(): self
    {
        return new self(self::FAIL_EMAIL_SEND_DISABLED);
    }
}
