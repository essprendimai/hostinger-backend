<?php

declare(strict_types = 1);

namespace Essprendimai\Basic\Exceptions;

/**
 * Class PageTransactionException
 */
class PageTransactionException extends \Exception
{
    const FAIL_STORE_PAGE = 'Failed to store page with transaction';
    const FAIL_DESTROY_PAGE = 'Failed to store page with transaction';
    const FAIL_STORE_PAGE_WHEN_GROUP_NOT_FOUND = 'Failed to store page with transaction when group id not found by group type';

    /**
     * @return PageTransactionException
     */
    public static function failStorePage(): self
    {
        return new self(self::FAIL_STORE_PAGE);
    }

    /**
     * @return PageTransactionException
     */
    public static function failDestroyPage(): self
    {
        return new self(self::FAIL_STORE_PAGE);
    }

    /**
     * @return PageTransactionException
     */
    public static function failStorePageWhenGroupNotFound(): self
    {
        return new self(self::FAIL_STORE_PAGE_WHEN_GROUP_NOT_FOUND);
    }
}
