FROM php:7.2.2-fpm

ARG APP_ENV=local

# Set timezone
ENV TZ 'Europe/Vilnius'
    RUN echo $TZ > /etc/timezone && \
    apt-get update && apt-get install -y tzdata && \
    rm /etc/localtime && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    apt-get clean

ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64.deb /root/
RUN dpkg -i /root/dumb-init_*.deb

RUN apt-get update \
  && apt-get install -y \
  unzip \
  bzip2 \
  cron \
  curl \
  libcurl4-gnutls-dev \
  wget \
  apt-transport-https \
  lsb-release \
  ca-certificates \
  git \
  nano \
  sqlite \
  ca-certificates \
  libmagickwand-dev

RUN apt-get update -qq && apt-get install -y mysql-client --no-install-recommends \
 && docker-php-ext-install pdo pdo_mysql pcntl shmop curl sockets gd zip bcmath

# Install grpc extension for PHP
RUN set -xe; \
	apt-get update; \
	apt-get -y --no-install-recommends install g++ zlib1g-dev; \
	pecl install grpc; \
	docker-php-ext-enable grpc

ENV APP_HOME /home/docker/src

COPY docker/php.ini /usr/local/etc/php/
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /home/docker/src/hostinger-backend/

EXPOSE 8000

ENTRYPOINT [""]
CMD ["dumb-init", "php", "artisan", "serve", "--host", "0.0.0.0", "--port", "8000"]